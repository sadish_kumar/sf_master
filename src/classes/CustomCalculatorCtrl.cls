global class CustomCalculatorCtrl{
    
    private ApexPages.StandardController ctrl; // Controller to fetch loanApplication record details from webpage
    public genesis__Applications__c loanApp {get; set; }             // In memory loanApplication object
    private String loanApplicationID;
    public Boolean isPreview = false;
    public boolean termsPresent{get;set;}
    public Decimal totalAmount {get;set;}
    public Decimal totalPrincipal {get;set;}
    public Decimal totalInterest {get;set;}
    public Decimal paymentAmount{get;set;}
    public Boolean isSF1Request{get;set;}
    public Decimal apr;
    
    public String renderAsValue {get;set;}
    // Constructor (invokes generation of schedule based on the params specified in loanApplication)
    public CustomCalculatorCtrl(ApexPages.StandardController controller) {
        
        ctrl = controller;
        
        loanApplicationID = ((genesis__Applications__c)controller.getRecord()).Id;   
        termsPresent = true;
        initEntries();
        if(termsPresent==true)
        generateSchedule();
        isPreview = false;
        
        String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
        if (userAgent!=null && userAgent.contains('Salesforce1')){
        isSF1Request = true;
        }else{
            isSF1Request = false;
        }
        apr = 0.0;
        
    }
    public PageReference saveAsPdf(){
        renderAsValue = 'pdf';
        
        PageReference ref = new PageReference('/apex/customCalculator?Id=' + loanApp.Id +'&renderAs=pdf');
        ref.setRedirect(false);
        return ref; 
    }
    
    public Component.Apex.pageBlockSection getDynamicPageSection(){
        Map<String,genesis__Loan_Application_Loan_Mapping__c> mapping=genesis__Loan_Application_Loan_Mapping__c.getAll();
        Component.Apex.pageBlockSection dynPageSec= new Component.Apex.pageBlockSection();
        Component.Apex.InputField loanAmount = new Component.Apex.InputField();
        loanAmount.expressions.value = '{!'+'loanApp.genesis__Loan_Amount__c'+'}';
        System.debug('Loan Amount expressions value ::' + loanAmount.expressions.value);
        Component.Apex.OutputField interestRate = new Component.Apex.OutputField();
        interestRate.expressions.value = '{!'+'loanApp.genesis__Interest_Rate__c'+'}';
        Component.Apex.OutputField term = new Component.Apex.OutputField();
        term.expressions.value = '{!'+'loanApp.Number_of_Payments__c'+'}';
        
        dynPageSec.childComponents.add(loanAmount);
        dynPageSec.childComponents.add(interestRate);
        dynPageSec.childComponents.add(term);
        
        return dynPageSec;
    }
    public Component.Apex.PageBlockSection getDynamicPgSectionTerms(){
        Map<String,genesis__Loan_Application_Loan_Mapping__c> mapping = genesis__Loan_Application_Loan_Mapping__c.getAll();
        Component.Apex.PageBlockSection dynPageSec = new Component.Apex.PageBlockSection();
        Component.Apex.InputField firstPaymentDate = new Component.Apex.InputField();
        firstPaymentDate.expressions.value = '{!' +'loanApp.genesis__Expected_First_Payment_Date__c'+'}';
        dynPageSec.childComponents.add(firstPaymentDate);
        return dynPageSec;
    }
    
    public void initEntries() {
        String loanAppFieldlist='';
        List<genesis__Loan_Application_Loan_Mapping__c> loanMappingList=genesis__Loan_Application_Loan_Mapping__c.getall().values();
        Set<String> loanAppFields=new Set<String>();
        Map<String,genesis__Loan_Application_Loan_Mapping__c> mapping=genesis__Loan_Application_Loan_Mapping__c.getAll();
        System.debug('***loanMappingList' + loanMappingList);
        if(loanMappingList == null || loanMappingList.size()==0){
                //debugStr='No Field Mapping is Defined.';
                termsPresent=false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No field mapping is defined.'));
                return;
        }
        
         
        try{
            for(genesis__Loan_Application_Loan_Mapping__c lMap : loanMappingList ) {
                  if(lMap.genesis__Application_Field_Name__c != null && (lMap.genesis__Application_Field_Name__c).trim() != '')
                       if(!loanAppFields.contains(lMap.genesis__Application_Field_Name__c)){
                           loanAppFields.add(lMap.genesis__Application_Field_Name__c);
                       }
            }  
            System.debug('***loanAppFields' + loanAppFields);
            for(String field:loanAppFields){
                loanAppFieldList += 'genesis__' + field+',';
            } 
            System.debug('***loanAppFieldList' + loanAppFieldList);
            if(loanAppFieldList.length() > 0) {
                  loanAppFieldList  = loanAppFieldList.subString(0,loanAppFieldList.length() - 1);                    
                  String dynamicQuery = 'Select Name,genesis__Expected_Start_Date__c,Number_of_Payments__c,genesis__Total_Estimated_Interest__c,genesis__Interest_Only_Period__c,genesis__Balloon_Payment__c,genesis__Interest_Calculation_Method__c,genesis__Days_Convention__c,' + loanAppFieldList + 
                                          ' from  genesis__Applications__c where Id=:loanApplicationID limit 1';
                  System.debug('dynamicQuery: ' + dynamicQuery);
                  loanApp=Database.query(dynamicQuery) ;
                  System.debug('loanApp: ' + loanApp);
                  
                  if(loanApp.genesis__Expected_Start_Date__c==null){
                        termsPresent=false;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'Expected Start Date cannot be null.'));
                        return;                     
                  }                  
                  
            }
        }catch(Exception e){
            System.debug('Cause:'+e.getCause()+',Message:'+e.getMessage()+',Line Number:'+e.getLineNumber());
        }
                              
        return;
    }
    
    public PageReference reset(){
         initEntries();
        return null;
    }
    
    // This method uses the loanApplication object and the rateSchedule object to generate emi's based on the method.
    public PageReference generateSchedule() {
        Map<String,genesis__Loan_Application_Loan_Mapping__c> mapping=genesis__Loan_Application_Loan_Mapping__c.getAll();
        System.debug('Loan Mapping :: ' + mapping);
        Date systemDate = Date.today();
        Date today = Date.today();
        
        isPreview = false;
        Decimal loanAmount=0;
        Date firstPaymentDate;
        String paymentFrequencyCode;
        String billingMethodCode;
        Decimal interestRate = 0;
        Decimal term =0;
         System.debug('loanApp: ' + loanApp);
        system.debug('Mapping value ::' + mapping.get(genesis.LendingConstants.AMORTIZABLE_AMOUNT));
        if(mapping.get(genesis.LendingConstants.AMORTIZABLE_AMOUNT)!=null){
            //system.debug('Mapping value ::' + (mapping.get(genesis.LendingConstants.AMORTIZABLE_AMOUNT).genesis__Application_Field_Name__c));
            //System.debug('Loan app ::' + loanApp.get(Amortizable Amount));
            system.debug(loanApp.genesis__Loan_Amount__c);
            loanAmount=(Decimal)loanApp.genesis__Loan_Amount__c;
        }else{
            loanAmount = loanApp.genesis__Loan_Amount__c;
        }
        system.debug('Loan Amount ::' + loanAmount);
        if(mapping.get(genesis.LendingConstants.EXPECTED_FIRST_PAYMENT_DATE)!=null){
            //firstPaymentDate =(Date)loanApp.get(mapping.get(genesis.LendingConstants.EXPECTED_FIRST_PAYMENT_DATE).genesis__Application_Field_Name__c);
            firstPaymentDate =(Date)loanApp.genesis__Expected_First_Payment_Date__c;
        }else{
            firstPaymentDate = loanApp.genesis__Expected_First_Payment_Date__c;
        }
        if(mapping.get(genesis.LendingConstants.PAYMENT_FREQUENCY)!=null){
           // paymentFrequencyCode=(String)loanApp.get(mapping.get(genesis.LendingConstants.PAYMENT_FREQUENCY).genesis__Application_Field_Name__c);
            paymentFrequencyCode=(String)loanApp.genesis__Payment_Frequency__c;
        }else{
            paymentFrequencyCode = loanApp.genesis__Payment_Frequency__c;
        }
        if(mapping.get(genesis.LendingConstants.INTEREST_RATE)!=null){
            //interestRate=(Decimal)loanApp.get(mapping.get(genesis.LendingConstants.INTEREST_RATE).genesis__Application_Field_Name__c);
            interestRate=(Decimal)loanApp.genesis__Interest_Rate__c;
        }else{
            interestRate = loanApp.genesis__Interest_Rate__c;
        }
        if(mapping.get(genesis.LendingConstants.TERM)!=null){
            //term=(Decimal)loanApp.get(mapping.get(genesis.LendingConstants.TERM).genesis__Application_Field_Name__c);
            term=(Decimal)loanApp.Number_of_Payments__c ;
        }else{
            term = loanApp.Number_of_Payments__c ;
        }
        
        System.debug('Terms: ' + loanAmount + ' - ' + firstPaymentDate + ' - ' +paymentFrequencyCode + ' - ' +interestRate + ' - ' +term);
        if( loanAmount == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Loan Amount cannot be null.'));
            return null;
        }
        system.debug('****firstPaymentDate***'+firstPaymentDate);
        
        if( firstPaymentDate == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'First payment date cannot be null.'));
            return null;
        }
        
        system.debug('****genesis__Expected_Start_Date__c***'+loanApp.genesis__Expected_Start_Date__c);
        
        if( loanApp.genesis__Expected_Start_Date__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Expected Start date cannot be null.'));
            return null;
        }
        
        system.debug('****paymentFrequencyCode***'+paymentFrequencyCode);
        
        if( paymentFrequencyCode == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Payment frequency cannot be null.'));
            return null;
        }
        
        system.debug('****interestRate***'+interestRate);
        
        if( interestRate == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Interest Rate cannot be null.'));
            return null;
        }
        
        system.debug('****term***'+term);
        
        if( term == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Term cannot be null.'));
            return null;
        }   
       
        billingMethodCode = loanApp.genesis__Interest_Calculation_Method__c; 
        system.debug('****billingMethodCode***'+billingMethodCode);
        if(billingMethodCode == null)
            billingMethodCode = 'Declining Balance';
        
        Date ExpectedStartDate = loanApp.genesis__Expected_Start_Date__c;
        Decimal totalLoanAmount = loanAmount ;
             
        String dayConvention = (loanApp.genesis__Days_Convention__c!=null)?loanApp.genesis__Days_Convention__c:'30/360';
        
        Decimal interestOnlyPeriod = 0;
        if(loanApp.genesis__Interest_Only_Period__c != 0){
            interestOnlyPeriod = loanApp.genesis__Interest_Only_Period__c;
        }
        
        Decimal balloonPayment = 0;
        if(loanApp.genesis__Balloon_Payment__c != 0){
            balloonPayment = loanApp.genesis__Balloon_Payment__c;
        } 
        
        System.debug(loggingleveL.error,'interestRate: ' + interestRate);
        genesis__Lending_Calculator__c calc = new genesis__Lending_Calculator__c(
                                            genesis__Accrual_Base_Method_Code__c = dayConvention,
                                            genesis__Action__c = 'CALCULATION_ALL',
                                            genesis__Additional_Interest_Amount__c = 0,
                                            genesis__Amortization_Calculation_Method_Code__c = 'NONE', 
                                            genesis__APR__c = 0,
                                            genesis__Balance_Amount__c = 0,
                                            genesis__Balloon_Method_Code__c = 'DUMMY',
                                            genesis__Balloon_Payment_Amount__c = balloonPayment,
                                            genesis__Billing_Method_Code__c = billingMethodCode,
                                            genesis__Contract_Date__c = expectedStartDate,
                                            genesis__Final_Payment_Amount__c = 0,
                                            genesis__Financed_Amount__c = 0,
                                            genesis__Financed_Fees__c = 0,
                                            genesis__First_Payment_Date__c = firstPaymentDate, 
                                            genesis__First_Period_Calender_Days__c = 0,
                                            genesis__First_Period_Interest__c = 0,
                                            genesis__Flexible_Repayment_Flag__c = false,
                                            genesis__Installment_Method_Code__c = 'UNDEFINED',
                                            genesis__Interest_Amount__c = 0,
                                            genesis__Interest_Only_Period__c = interestOnlyPeriod,
                                            genesis__Loan_Amount__c = totalLoanAmount,
                                            genesis__Payment_Amount__c = 0,
                                            genesis__Payment_Frequency_Code__c = paymentFrequencyCode,
                                            genesis__Prepaid_Fees__c = 0,
                                            genesis__Principal_Payment_Amount__c = 0,
                                            genesis__Rate__c = interestRate,
                                            genesis__Repayment_Type_Code__c = 'UNDEFINED',
                                            genesis__Term__c = term,
                                            genesis__Total_Finance_Charge__c = 0,
                                            genesis__Total_Financed_Amount__c = 0,
                                            genesis__Total_Of_Payments__c = 0

        );  
        totalAmount = 0;
        totalPrincipal = 0;
        totalInterest = 0;
        system.debug('***Calc'+calc);
        try {
            emi = genesis.LendingCalculator.calculate(calc, null);
            
            system.debug('***Emi'+emi);
           
            for(genesis__Amortization_Schedule__c e:emi){
                totalAmount += e.genesis__Total_Due_Amount__c;
                totalPrincipal += e.genesis__Due_Principal__c;
                totalInterest += e.genesis__Due_interest__c;
                
            }
            paymentAmount = calc.genesis__Payment_Amount__c;
            totalAmount = totalAmount.setScale(2);
            totalPrincipal = totalPrincipal.setScale(2);
            totalInterest = totalinterest.setScale(2);
        } catch (Exception e) {
            emi = new List<genesis__Amortization_Schedule__c>();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }
        
        isPreview = true;
        return null;
    }
    
    // Helper to populate modified loanApplication fields from webpage
    public void refreshCtrlRecord() {
        loanApp = (genesis__Applications__c)ctrl.getRecord();
        /*loanApp = [Select Id,name,
                    genesis__Loan_Amount__c,
                    genesis__Term__c,
                    genesis__Interest_rate__c,
                    genesis__]
            */
            
    }
    
    global PageReference save() {
        loanApp.genesis__Total_Estimated_Interest__c = 0;
        
        Map<String,genesis__Loan_Application_Loan_Mapping__c> mapping=genesis__Loan_Application_Loan_Mapping__c.getAll();
        if (!isPreview) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please review your changes and generate a valid offer before saving the information'));
            return null;
        }
        
        if(emi == null || emi.size() < 1) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'The generated schedule should have atleast one EMI. Please check the loan parameters!'));
            return null;
        }

        try {
            //Clear if there is any repayment schedule
            List<genesis__Amortization_Schedule__c> schedule = [select Id from genesis__Amortization_Schedule__c
                                                            where genesis__Application__c = :loanApp.Id];
            if(schedule.size()>0){
                delete schedule;                                                            
            }
            //Regenerate
            List<genesis__Amortization_Schedule__c> newSchedule = new List<genesis__Amortization_Schedule__c>();
            for(genesis__Amortization_Schedule__c e : emi){
                genesis__Amortization_Schedule__c rec = new genesis__Amortization_Schedule__c();
                rec.genesis__Payment_Number__c = e.genesis__Payment_Number__c;
                rec.genesis__Due_Principal__c = e.genesis__Due_Principal__c;
                rec.genesis__Due_Interest__c = e.genesis__Due_Interest__c;
                rec.genesis__Due_Date__c = e.genesis__Due_Date__c;
                rec.genesis__Closing_Principal_Balance__c = e.genesis__Closing_Principal_Balance__c;
                rec.genesis__Opening_Principal_Balance__c = e.genesis__Opening_Principal_Balance__c;
                rec.genesis__Total_Due_Amount__c = e.genesis__Total_Due_Amount__c;
                rec.genesis__Application__c = loanApp.Id;
                newSchedule.add(rec);
                loanApp.genesis__Total_Estimated_Interest__c += e.genesis__Due_Interest__c;
            }
            
            insert newSchedule;
            
            Object obj=emi[0].genesis__Total_Due_Amount__c ;
            //loanApp.put(mapping.get(genesis.LendingConstants.PAYMENT_AMOUNT).Application_Field_Name__c,obj); 
            //loanApp.put(mapping.get(genesis.LendingConstants.PAYMENT_AMOUNT).genesis__Application_Field_Name__c,paymentAmount);
            loanApp.put('genesis__Payment_Amount__c',paymentAmount);
            System.debug('loanApplication: ' + loanApp);
            //ctrl.save();//invoke standard Save method
           
            if(loanApp.Number_of_Payments__c > 0)
                loanApp.genesis__Expected_Close_Date__c = loan.DateUtil.addCycle(loanApp.genesis__Expected_First_Payment_Date__c,
                                                        (Integer)loanApp.genesis__Expected_First_Payment_Date__c.day(),
                                                        loanApp.genesis__Payment_Frequency__c,
                                                        (Integer)(loanApp.Number_of_Payments__c -1) );
           
            update loanApp;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'Successfully saved information on the loan application'));
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
        }
        isPreview = false;
        update loanApp;
        PageReference ref = new PageReference('/apex/tabbedScoring?id=' + loanApp.Id);
        ref.setRedirect(true);
        return ref;
    }
    
    // Get the modified fields from the visual force page, reassign values to loanApplication in memory(no db save) and recompute
    global PageReference regenerateSchedule() {
       
        if(emi != null) {
            emi.clear();
        } 
        //refreshCtrlRecord();
        generateSchedule();
        return null;
    }
    
    public PageReference setBillingParams(){
        return null;
    }
    
    public PageReference valueChanged() {
        isPreview = false;
        return null;
    }
    
    // Returns the generated schedule
    public List<genesis__Amortization_Schedule__c> getGeneratedSchedule() {
        return emi;
    }

    private List<genesis__Amortization_Schedule__c> emi;
}