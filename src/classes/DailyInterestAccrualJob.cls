/*
Schedule the job manually:
DailyInterestAccrualJob job = new DailyInterestAccrualJob();
Database.executeBatch(job,100);
*/
global class DailyInterestAccrualJob implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'DailyInterestAccrualJob';
    private boolean submitNextJob = false;
    private static final String query = 'select Id,Name,loan__Contractual_Interest_Rate__c,loan__Expected_Disbursal_Date__c, loan__Loan_Amount__c, loan__Charged_Off_Date__c, loan__Closed_Date__c  from loan__Loan_Account__c where loan__Loan_Status__c = \'Active - Good Standing\' or loan__Loan_Status__c = \'Active - Bad Standing\' or loan__Loan_Status__c = \'Closed - Obligations met\' or loan__Loan_Status__c = \'Closed- Written Off\'';
        
    public DailyInterestAccrualJob(){
    }
    public DailyInterestAccrualJob(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        DailyInterestAccrualJob job = new DailyInterestAccrualJob(true);
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            List<loan__loan_Account__c > loanAccountList= (List<loan__loan_Account__c > ) scope;
            DailyInterestAccrualHandler handler = new DailyInterestAccrualHandler(loanAccountList);
            handler.processLoanAccounts();
        }catch(Exception e){
              Database.rollback(sp);     
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'DailyInterestAccrualJob failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedDisbursalJournalEntries job = new AcctSeedDisbursalJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}