public with sharing class NewDocumentCtrl extends genesis.ApplicationWizardBaseClass{
    
    public boolean showAttachFileBtn {set; get;}
    public boolean showUploadPanelGrid {set; get;}
    public Attachment attach {get;set;}
    public String apiSessionId {get;set;} 
    public genesis__Applications__c application {get; set;}
    public boolean showpanel{get;set;}
    public boolean hideattach{get;set;}
    public NewDocumentCtrl(ApexPages.StandardController controller) {
        super(controller);
        showpanel=false;
        hideattach=true;
        this.application = (genesis__Applications__c)controller.getRecord();
        
        
        this.application = [Select Id,Name,
                                    genesis__Interest_Rate__c,
                                    genesis__Loan_Amount__c,
                                    genesis__Term__c,
                                    genesis__status__c,
                                    genesis__Interest_Only_Period__c ,
                                    genesis__Expected_First_Payment_Date__c,
                                    genesis__Expected_Start_Date__c ,
                                    genesis__Payment_Frequency__c,
                                    genesis__Total_Estimated_Interest__c,
                                    genesis__Days_Convention__c,
                                    genesis__Interest_Calculation_Method__c,
                                    Applicant_s_Email__c,
                                    Applicant_Name__c
                                    
                                 from genesis__Applications__c 
                                where id =: this.application.Id];
                               
        showAttachFileBtn = true;
        showUploadPanelGrid = false;
        
        attach = new Attachment();
        apiSessionId = UserInfo.getSessionId();
        
        //List<attachment> empAttachments = [Select Id,Name from attachment where parentId in :empId];
    }
    
    
    public PageReference cancelFileUpload(){
        showAttachFileBtn = true;
        showUploadPanelGrid = false;
        return null;
    }
    
    public PageReference uploadFile(){
        showAttachFileBtn = false;
        showUploadPanelGrid = true;
        return null;
    }
    
    
    @TestVisible private void insertDocumentStatus(String title){
        genesis__Document_Status__c obj = new genesis__Document_Status__c();
        if(title.contains('.')){
            title = title.split('\\.')[0];
        }
        System.debug('title ' + title);
        obj.genesis__Application__c = this.application.Id;
        List<genesis__Document_Master__c> dmList = new List<genesis__Document_Master__c>();
        dmList = [SELECT Name,Id,genesis__Doc_Name__c from genesis__Document_Master__c where genesis__Doc_Name__c =:title ];
        System.debug('dmList ::'+dmList);
        if(dmList!=null && dmList.size() == 1){
            obj.genesis__Doc_Name__c = dmList[0].Id;
            obj.genesis__Attachment_Name__c = title;
            obj.genesis__Status__c = 'CREATED';
        }else{
            obj.genesis__Attachment_Name__c = title;
            obj.genesis__Doc_Name__c = null;
            obj.genesis__Status__c = null;
        }
        obj.genesis__Document_Url__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' + attach.id;
        System.debug('obj ::'+obj);
        insert obj;
    }

    @TestVisible private Attachment insertAttachment(Id parentId){       
        
        attach.ParentId = parentId;
        insert attach;
        System.debug('Success');
        attach.Body = null; 
        return attach;
    }


    public PageReference processUpload(){
        try{
            insertAttachment(this.application.Id);
            String title = attach.Name;
            insertDocumentStatus(title);
            System.debug('Record Inserted');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'File uploaded successsfully'));
        }catch (Exception e){
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Some exception has occured'));
        }
    
        attach = new Attachment();
        return null;
    }
    
     public PageReference genDocument(){
        //String contactId = [Select genesis__Account__r.Id from genesis__Applications__c where id =: application.id].genesis__Account__r.Id;
        //Account contactInfo = [Select Id,Email__c,Name from Account where Id =: contactId];
        String email = this.application.Applicant_s_Email__c;
        String name = this.application.Applicant_Name__c;
        apiSessionId = UserInfo.getSessionID();
        System.debug('apiSessionId: ' + apiSessionId);
        //apex/loop__looplus?sessionId={!$Api.Session_ID}&eid={!genesis__Applications__c.Id}&parameter1_name={!genesis__Applications__c.genesis__Account__c}&parameter1_email={!genesis__Applications__c.Email__c}
        //String documentUrl =  '/apex/loop__looplus?sessionId=' + apiSessionId+ '&eid='+ this.application.Id + '&param_email=' + email + '&param_name=' + Name;
        String documentUrl =  '/apex/loop__looplus?sessionId=' + apiSessionId+ '&eid='+ this.application.Id + '&param_email=' + email + '&param_name=' + Name;
        system.debug(documentUrl);
        Pagereference pageRef = new Pagereference(documentUrl);
        
        return pageRef.setRedirect(true);
        
    }

    /*override public PageReference nextActionLogic(){
        System.debug('overridden method..');
        
        //Decimal maxSequence = CommonUtil.getMaxLandingSequence(application.genesis__Product_Type__c);
        Decimal maxSequence;
        //if(application.genesis__Landing_Sequence__c < maxSequence){
            //application.genesis__Landing_Sequence__c = manager.currentSequence;
        //}
        //upsert application;
        String retMsg = '';
        genesis.ConvertApplicationCtrl ctrl = new genesis.ConvertApplicationCtrl();
        if(!Test.isRunningTest())
            retMsg = ctrl.convertApplication(application.Id);
        
        if(!String.isBlank(retMsg)){
            createMessage(ApexPages.severity.INFO,retMsg);
        }
        //ApexPages.currentPage().getParameters().put('landingSeq',application.Landing_Sequence__c.intValue()+'');
        return null;//manager.navigateNext(ApexPages.currentPage().getParameters());
    }
    */
    
    Public void showPanel() {
        showpanel=true;
        hideattach=false;
    }
    
    public void hidePanel(){
        showPanel = false;
        hideattach = true;
    }
    
    Public Note mynote;
    Public Note getmynote() {
        mynote = new Note();
        return mynote;
    }
    
    Public Pagereference Savedoc(){
        String accid = this.application.id;
        if(String.isEmpty(mynote.title)){
            createMessage(ApexPages.severity.ERROR,'Note\'s Title cannot be null.');
            return null;
        }
        else if(String.isEmpty(mynote.Body)){
            createMessage(ApexPages.severity.ERROR,'Note\'s Body cannot be null.');
            return null;
        }
        
        Note a = new Note(parentId = accid, title = mynote.title, body = mynote.body);        
         /* insert the attachment */
         insert a;
        return NULL;
    }  
    
    public void sendDocument(){
        
        DDP_Template__c ddpTemplate = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
        System.debug(LoggingLevel.ERROR, 'Drawloop iD :'+ ddpTemplate);
        try{
            if(ddpTemplate != null){
                System.debug(LoggingLevel.ERROR, 'Tried Calling Drawloop');
                Drawloop.generateDDP(ddpTemplate.Deploy_Id__c,ddpTemplate.Template_Id__c,this.application.Id,UserInfo.getSessionId());
                System.debug(LoggingLevel.ERROR, 'End of Drawloop call');
            }    
            createMessage(ApexPages.severity.Confirm,'Request submitted for Document generation.');
        }catch(Exception e){
            createMessage(ApexPages.severity.ERROR,e.getMessage());
        }
    }
    
    public void sendPADDoc(){        
       
        try{            
            loan_vp.VersapayImpl par = new loan_vp.VersapayImpl();
            loan_vp.PADResponse pr = par.createPADAgreement(this.application.Applicant_s_Email__c, '', this.application.id);
            this.application.Versapay_Token__c=pr.token;
            update this.application;
            createMessage(ApexPages.severity.Confirm,'PAD Agreement has been sent');
        }catch(Exception e){
            createMessage(ApexPages.severity.ERROR,e.getMessage());
        }
    }
    
    @TestVisible private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.getMessages().clear();
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }
    
    /*public void createDoc(){
        String sessionId = UserInfo.getSessionId();
        DrawloopTest.generateDDP('a6cg00000008gZI','a6dg0000000ChQ0',this.application.Id,sessionId);
    }*/
    
}