global class ACHReturnFileParser extends loan.ACHReturnPaymentProcessor {
    
    global override List < loan.ACHReturn > parseFile(String fileContent, String objectType) {
        List < loan.ACHReturn > achReturns = new List < loan.ACHReturn > ();
        List < String > bodyLines = fileContent.split('\r\n');
        
        if (bodyLines.size() == 1) {
            bodyLines = fileContent.split('\n');
        }
        //Map<String,String> objectNamingMap = new Map<String,String>();
        //objectNamingMap.put('LAI','loan__Loan_Account__c');
        // Map<String,Date> lNameDateMap = new Map<String,Date>();
        Map < String, String > lNameRetCodeMap = new Map < String, String > ();
        system.debug('Lines: ' + bodyLines);
        system.debug('Number of lines: '+ bodyLines.size());
        List < String > pmts = new List < String > ();
        String returnCode = '';
        List < loan__Loan_Payment_Transaction__c > payments = new List < loan__Loan_Payment_Transaction__c > ();
        for (Integer i = 0; i < bodyLines.size(); i++) {
            Date pDate;
            System.debug(bodyLines[i]);
            String pmtName = '';
            //String returnCode = '';
            if (bodyLines[i].startsWith('5')) {              
                returnCode = bodylines[i].subString(47, 50);
            }
            if (bodyLines[i].startsWith('6')) {
                pmtName = bodyLines[i].subString(39, 52).trim();
            }                  
            system.debug('pmtName:' + pmtName);
            system.debug('counter:' + i);
            system.debug('return code:' + returnCode);            
            lNameRetCodeMap.put(pmtName, returnCode);
            pmts.add(pmtName);
            system.debug('return code is:' + lNameRetCodeMap.get(pmtName));
            }
        system.debug('return code is:' + lNameRetCodeMap);
        payments = [select id, name from loan__Loan_Payment_Transaction__c where name IN :pmts];
        system.debug('Payments are: ' + payments);
        for (loan__Loan_Payment_Transaction__c pmt :payments) {
            loan.ACHReturn achRet = new loan.ACHReturn();
            achRet.payment = pmt;
            System.debug('loan transaction name:' + pmt.Name);
            achRet.returnCode = lNameRetCodeMap.get(pmt.Name);
            achReturns.add(achRet);
        }
        system.debug('ACHReturns: ' + achReturns);
        return achReturns;
    }
}