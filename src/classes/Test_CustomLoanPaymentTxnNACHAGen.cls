@isTest
public class Test_CustomLoanPaymentTxnNACHAGen{
    /* Commenting since we are no longer using CustomLoanPaymentTxnNACHAGen...
    
    
  public static void init(boolean Achon){
    loan__ACH_Parameters__c customAchParameter                      = new loan__ACH_Parameters__c();
    customAchParameter.loan__ACH_Return_Filegen__c                  = 'ACHReturnFileParser';
    customAchParameter.loan__Days_In_Advance_To_Create_File__c      = 0;
    customAchParameter.loan__Loan_Payment_Transaction_Filegen__c    = 'CustomLoanPaymentTxnNACHAGen';
    customAchParameter.loan__Lock_Period_for_Loan_Payments__c       = 3;
    customAchParameter.loan__Merge_One_Time_and_Recurring_ACH__c    = true;
    customAchParameter.Originator_Number__c                         = '15256';
    customAchParameter.CIBC_settlement_account_number__c            = '817217';
    customAchParameter.loan__Organization_Name__c                   = 'Vault Circle';
    customAchParameter.loan__Folder_Name__c                         = 'ACH Folder';
    customAchParameter.loan__Loan_Disbursal_Transaction_Filegen__c  = 'CustomLoanDisbursalTxnNACHAGen';
    customAchParameter.loan__Use_Lock_Based_ACH__c =true;
    insert customAchParameter;  
    
     Account acc = new Account();
        acc.Name                   = 'Ext';
        acc.peer__First_Name__c    ='TestData';
        acc.peer__Last_Name__c     ='TestData2';
        insert acc;
        
    loan__Bank_Account__c objBankAcct                  = new loan__Bank_Account__c();
    objBankAcct.loan__Account__c                       = acc.id;
         objBankAcct.peer__Account_Age__c              = 24;
         objBankAcct.loan__Account_Type__c             = 'Saving';  
         objBankAcct.loan__Account_Usage__c            = 'Investor Trust Account';
         objBankAcct.loan__ACH_Code__c                 = '101';
         objBankAcct.loan__Active__c                   = true;
         objBankAcct.peer__Allow_for_Direct_Credit__c  = true;
         objBankAcct.loan__Bank_Account_Number__c      = '11389192834';
         objBankAcct.loan__Bank_Name__c                = 'SBI';
         objBankAcct.peer__Branch_Code__c              = '01';
         objBankAcct.Branch_Transit_Number__c          = '12345';
         objBankAcct.loan__Routing_Number__c           = '1234567';
         objBankAcct.peer__Working_Place_Type__c       = 'Big company';
         insert objBankAcct;
         
         loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c gnAp= genesis.TestHelper.createLoanApplication(); 
        gnAp.Contract_Document_Status__c='Completed';
        update gnAp;
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;       
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;
        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        
         loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
                                                    
        loanAccount.loan__OT_Borrower_ACH__c   = objBankAcct.id;
        loanAccount.loan__Account__c           = acc.id;
        loanAccount.loan__Borrower_ACH__c      = objBankAcct.id;
        loanAccount.loan__ACH_On__c =Achon;
        loanAccount.loan__ACH_Routing_Number__c = '123345678';
        update loanAccount;
        
        list<Sobject> lstLoanPaymentTxn = new list<Sobject>();
     
          loan__Loan_Payment_Transaction__c objLoanPayment = new loan__Loan_Payment_Transaction__c();
          objLoanPayment.loan__Loan_Account__c     = loanAccount.id;
          objLoanPayment.loan__Transaction_Amount__c = 2300;
          Sobject sObj = (Sobject)objLoanPayment;
          lstLoanPaymentTxn.add(sObj);
        
        
        insert lstLoanPaymentTxn;
        
         loan.TransactionSweepToACHState state = new loan.TransactionSweepToACHState();
         state.counter = 2; 
        CustomLoanPaymentTxnNACHAGen  objCustom = new CustomLoanPaymentTxnNACHAGen();
        objCustom.requeryScope(lstLoanPaymentTxn);
        objCustom.getSimpleFileName();
        objCustom.getEntries(state ,lstLoanPaymentTxn);
        try{
        objCustom.getHeader(state ,lstLoanPaymentTxn);
        }catch(Exception e){
        }
         try{
          objCustom.getTrailer(state ,lstLoanPaymentTxn);
        }catch(Exception e){
        }
        
       
        CustomLoanPaymentTxnNACHAGen.getJulianDate(1,2,3);
         
         
  }
  public static testmethod void testme1(){
  init(true);
  }
   public static testmethod void testme2(){
   init(false);
  }
  
    */
      
}