/*
Schedule the job manually:
AcctSeedWaivedFeeChargeJournalEntries j = new AcctSeedWaivedFeeChargeJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedWaivedFeeChargeJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedWaivedFeeChargeJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id,loan__Loan_Account__c from loan__Charge__c where loan__Waive__c = true and Journal_Entry__c != null and Waived_Fee_Journal_Entry__c = null';
        
    public AcctSeedWaivedFeeChargeJournalEntries(){
    }
    public AcctSeedWaivedFeeChargeJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedWaivedFeeChargeJournalEntries job = new AcctSeedWaivedFeeChargeJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            List<loan__loan_Account__c > loanAccountList = new List<loan__loan_Account__c >();
            for (loan__Charge__c record : (List<loan__Charge__c> ) scope) {
            	loanAccountList.add(new loan__loan_Account__c(Id=record.loan__Loan_Account__c));
            }
            AcctSeedUtilityClass.createWaivedFeeChargeJournalEntries(loanAccountList);
            
        }catch(Exception e){
              Database.rollback(sp); 
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedWaivedFeeChargeJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedWriteOffJournalEntries job = new AcctSeedWriteOffJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}