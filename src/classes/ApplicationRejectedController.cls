public with sharing class ApplicationRejectedController {
 
    public String applicationTrackId {get;set;}
    
    public ApplicationRejectedController(){
    
      applicationTrackId= ApexPages.currentPage().getParameters().get('id');
    
    }
    
    public PageReference RedirectToFAQ() {
    
        return new PageReference('https://www.lendified.com/faq/');
    }
}