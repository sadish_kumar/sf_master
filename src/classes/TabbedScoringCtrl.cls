public with sharing class TabbedScoringCtrl extends genesis.ApplicationWizardBaseClass{
    
    public List<genesis__Amortization_Schedule__c> amortizationList {get; set;}
    public TabbedScoringCtrl(ApexPages.StandardController controller) {
        super(controller);
       this.application = (genesis__Applications__c)controller.getRecord();
       amortizationList = (List<genesis__Amortization_Schedule__c>)[SELECT Id, Name, genesis__Payment_Number__c, genesis__Due_Date__c, genesis__Due_Interest__c, genesis__Due_Principal__c, genesis__Total_Due_Amount__c, genesis__Opening_Principal_Balance__c, genesis__Closing_Principal_Balance__c from genesis__Amortization_Schedule__c where genesis__Application__c =: this.application.Id];
    }

}