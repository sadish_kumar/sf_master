public with sharing class NewAppCtrl extends genesis.ApplicationWizardBaseClass{
    public genesis__Applications__c application{get; set;}
    public note notes {get; set;}
    public boolean showpanel{get;set;}
    public boolean hideattach{get;set;}
    
    public NewAppCtrl(ApexPages.StandardController controller) {
        super(controller);
        showpanel=false;
        hideattach=true;
       this.application = (genesis__Applications__c)controller.getRecord();
    }
    
}