public class VersapayBatchClassHandler{
    
    List<loan__Loan_Account__c> scope;
    DDP_Template__c ddpTemplate;
    
    
    public VersapayBatchClassHandler(List<loan__loan_Account__c> scope){
        this.scope = scope;
        ddpTemplate           = new DDP_Template__c();
       // ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
        //sessionId             = UserInfo.getSessionId(); 
        //system.debug('****ddpTemplate'+sessionId);    
    }
    
    public void checkVersapayTokenStatus(String sessionId){
        
         system.debug('***sessionId'+sessionId);
        for(loan__Loan_Account__c contractObj : scope){
            
            loan_vp.VersapayImpl impl = new loan_vp.VersapayImpl();
            loan_vp.PADResponse res;
            if(!Test.isRunningTest()){
                res =  impl.getPADAgreement(contractObj.loan_vp__Versapay_Token__c);
                system.debug('***INLive'+res);
            }
            else{
                res = new loan_vp.PADResponse();
                res.state = 'APPROVED';
                system.debug('***INTest'+res);
            }
            
            if(res != null)
                contractObj.Versapay_Token_Status__c = res.state;
            
            if(contractObj.Versapay_Token_Status__c!=null && (contractObj.Versapay_Token_Status__c).equalsIgnoreCase('APPROVED')){    
                contractObj.Contract_Document_Send__c = true;
                 
                if(contractObj.loan__Contact__c!=null && contractObj.loan__Contact__r.MailingState!=null){
                     if(contractObj.loan__Contact__r.MailingState.equalsIgnoreCase('AB'))
                     ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE_AB);
                     else if(contractObj.loan__Contact__r.MailingState.equalsIgnoreCase('QC'))
                     ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE_QC);
                     else
                     ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
                }
                else{
                ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
                }               
                system.debug('***ddpTemplate'+ddpTemplate.name);
                if(ddpTemplate != null){                
                    // Drawloop.generateDDP(ddpTemplate.Deploy_ID__c , ddpTemplate.Template_Id__c, contractObj.Application__c, sessionId);
                    Map<string, string> variables = new Map<string,string>();
                    variables = new Map<string, string> {
                    'deploy' => ddpTemplate.Deploy_ID__c,
                    'SFAccount' => contractObj.loan__Account__c,
                    'SFContact' => contractObj.loan__Contact__c,
                    'param_name'=> contractObj.loan__Contact__r.name,
                    'param_email'=> contractObj.loan__Contact__r.email
                    };
                    Loop.loopMessage lm = new Loop.loopMessage();
                    lm.sessionId=sessionId;
                    lm.batchNotification = Loop.loopMessage.Notification.BEGIN_AND_COMPLETE;                    
                    lm.requests.add(new Loop.loopMessage.loopMessageRequest(contractObj.Application__c,ddpTemplate.Template_Id__c,variables));
                   String resp=lm.sendAllRequests();
                  
                }
            }
        }
        
        List<Database.SaveResult> updateCLContract =  Database.update(scope);
        
    }
    
}