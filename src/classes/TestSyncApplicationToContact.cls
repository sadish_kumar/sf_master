@isTest
public class TestSyncApplicationToContact {
	
    static testmethod void testSyncApplicationToContact() {
        
        
        genesis__Applications__c app = genesis.TestHelper.createApplication();
        app.genesis__Status__c = 'Email Verified';
        update app;
        
        Contact contact = [select Id,Application_Status__c from Contact where Id =: app.genesis__Contact__c];
        System.assertEquals(contact.Application_Status__c,app.genesis__Status__c);
        
    } 
}