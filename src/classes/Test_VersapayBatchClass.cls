@isTest
Public class Test_VersapayBatchClass{
    public static genesis__Applications__c gnAp;
   
    public static testmethod void test_VersapayBatchClass(){
    
        DDP_Template__c temp = new DDP_Template__c();
        temp.name='Loan Agreement';
        temp.Deploy_ID__c='test';
        temp.Template_Id__c='test';
        insert temp;
        gnAp = genesis.TestHelper.createLoanApplication(); 
        loan.TestHelper.createSeedDataForTesting();        
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);        
         loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);        
        loanAccount.loan_vp__Versapay_Token__c = '8TNJ5BFZBAZW';
        loanAccount.loan__Loan_Status__c = 'Approved';
        loanAccount.Application__c=gnAp.id;
        update loanAccount;       
        SchedulableContext sc;
        scheduledBatchableForVersapayBatch versapay = new scheduledBatchableForVersapayBatch();
        versapay.execute(sc);    
       
         
         
 
    }
    
     public static testmethod void test_Login(){
    
    Test.setMock(HttpCalloutMock.class, new Test_MockHttpResponseGenerator());
    Login l = new Login();
    String res =Login.login('','');
    
    
   }


}