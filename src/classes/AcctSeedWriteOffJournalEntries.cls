/*
Schedule the job manually:
AcctSeedWriteOffJournalEntries j = new AcctSeedWriteOffJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedWriteOffJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedWriteOffJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id from loan__Loan_Account__c where loan__Charged_Off_Date__c != null and loan_seed__Write_Off_Journal_Entry__c = null';
        
    public AcctSeedWriteOffJournalEntries(){
    }
    public AcctSeedWriteOffJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedWriteOffJournalEntries job = new AcctSeedWriteOffJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            AcctSeedUtilityClass.createWriteOffJournalEntries(scope);
            
        }catch(Exception e){
              Database.rollback(sp); 
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedWriteOffJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedDailyInterestJournalEntries job = new AcctSeedDailyInterestJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}