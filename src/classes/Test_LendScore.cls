@isTest
private class Test_LendScore {
    
    @isTest static void test_method_one() {
        // Implement test code ..
        loan.TestHelper.createSeedDataForTesting();
        loan.TestHelper.createOffice('TestOffice');
        
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        system.debug('.......'+beacon9RTID+'.........'+bni3RTID);
        
        genesis.TestHelper.createAccount();
        genesis__applications__c application = genesis.TestHelper.createLoanApplication();
        Account acc = genesis.TestHelper.createAccount();
        Contact con = genesis.TestHelper.createContact();

        application.genesis__Account__c = acc.Id;
        application.Beacon_9_Score__c   = 600 ;
        application.BNI__c              = 600 ;
        application.genesis__Term__c    =12;
        application.Beacon_9_Decile__c= 4;
        application.Beacon_9_Loss_Rate__c =4.70;
        application.BNI_Decile__c =3;
        application.BNI_Loss_Rate__c =8.70;
        application.Lend_Score_Decile__c =3;
        application.Lend_Score__c =8;
        
        application.genesis__Days_Convention__c  = '30/360';
        
        update application;

        List<Loss_Rate_Decile_Comparator__c> lrdComp =  new List<Loss_Rate_Decile_Comparator__c>();
        Loss_Rate_Decile_Comparator__c beaconLRD     =  new Loss_Rate_Decile_Comparator__c(RecordTypeId = beacon9RTID, Max_Range__c = 700, Min_Range__c = 500, Decile__c = 10, Loss_Rate__c = 1);
        Loss_Rate_Decile_Comparator__c bniLRD        =  new Loss_Rate_Decile_Comparator__c(RecordTypeId = bni3RTID   , Max_Range__c = 700, Min_Range__c = 500, Decile__c = 10, Loss_Rate__c = 1);

        lrdComp.add(beaconLRD);
        lrdComp.add(bniLRD);
        insert lrdComp;

        Lend_Score__c lcScoreObj = new Lend_Score__c();
        lcScoreObj.Lend_Score_Min_Value__c  = 0 ;
        lcScoreObj.Lend_Score_Max_Value__c  = 1000;
        lcScoreObj.Decile__c                = 5;
        lcScoreObj.Applied_Interest_Rate__c = 1.2 ;
        insert lcScoreObj;
        
        application = [Select Id,Name,genesis__Contact__r.Id,Lend_Score__c,Lend_Score_Decile__c,BNI_Loss_Rate__c,BNI_Decile__c,Beacon_9_Loss_Rate__c,Beacon_9_Decile__c,genesis__Term__c,Beacon_9_Score__c ,BNI__c,
                                        genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                        Number_of_Payments__c ,genesis__Expected_Close_Date__c 
                                    from genesis__Applications__c
                                    where Id = :application.Id]; 
                                    
        
        Test.startTest();
        LendScore lsObj = new LendScore(application);
        lsObj.lendScoreCalculate();
        Test.stopTest();
    }
    
    @isTest static void test_method_two() {
        // Implement test code ..
        loan.TestHelper.createSeedDataForTesting();
        loan.TestHelper.createOffice('TestOffice');
      
        
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        system.debug('.......'+beacon9RTID+'.........'+bni3RTID);
        genesis.TestHelper.createAccount();
        genesis__applications__c application = genesis.TestHelper.createLoanApplication();
        Account acc = genesis.TestHelper.createAccount();
        Contact con = genesis.TestHelper.createContact();

        application.genesis__Account__c = acc.Id;
        application.Beacon_9_Score__c   = 600 ;
        application.BNI__c              = 600 ;
        application.genesis__Term__c    =12;
        application.Beacon_9_Decile__c= 4;
        application.Beacon_9_Loss_Rate__c =4.70;
        application.BNI_Decile__c =3;
        application.BNI_Loss_Rate__c =8.70;
        application.Lend_Score_Decile__c =3;
        application.Lend_Score__c =8;
        application.genesis__Loan_amount__c=1000;
        
        application.genesis__Days_Convention__c  = '30/360';
        
        update application;

        List<Loss_Rate_Decile_Comparator__c> lrdComp =  new List<Loss_Rate_Decile_Comparator__c>();
        Loss_Rate_Decile_Comparator__c beaconLRD     =  new Loss_Rate_Decile_Comparator__c(RecordTypeId = beacon9RTID, Max_Range__c = 700, Min_Range__c = 500, Decile__c = 10, Loss_Rate__c = 1);
        Loss_Rate_Decile_Comparator__c bniLRD        =  new Loss_Rate_Decile_Comparator__c(RecordTypeId = bni3RTID   , Max_Range__c = 700, Min_Range__c = 500, Decile__c = 10, Loss_Rate__c = 1);

        lrdComp.add(beaconLRD);
        lrdComp.add(bniLRD);
        insert lrdComp;

        Lend_Score__c lcScoreObj = new Lend_Score__c();
        lcScoreObj.Lend_Score_Min_Value__c  = 0 ;
        lcScoreObj.Lend_Score_Max_Value__c  = 1000;
        lcScoreObj.Decile__c                = 5;
        lcScoreObj.Applied_Interest_Rate__c = 1.2 ;
        insert lcScoreObj;
        
        application = [Select Id,Name,genesis__Loan_amount__c,genesis__Contact__r.Id,Lend_Score__c,Lend_Score_Decile__c,BNI_Loss_Rate__c,BNI_Decile__c,Beacon_9_Loss_Rate__c,Beacon_9_Decile__c,genesis__Term__c,Beacon_9_Score__c ,BNI__c,
                                        genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                        Number_of_Payments__c ,genesis__Expected_Close_Date__c 
                                    from genesis__Applications__c
                                    where Id = :application.Id]; 
                                    
        
        Test.startTest();
        LendScore lsObj = new LendScore(application);
        lsObj.lendScoreCalculate();
        Test.stopTest();
    }
    
}