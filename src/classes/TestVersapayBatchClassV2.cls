@isTest
Public class TestVersapayBatchClassV2{
    
    public static testmethod void testVersapayBatchClass(){
    
        DDP_Template__c temp = new DDP_Template__c();
        temp.name='Loan Agreement';
        temp.Deploy_ID__c='test';
        temp.Template_Id__c='test';
        insert temp;
        genesis__Applications__c app = genesis.TestHelper.createLoanApplication(); 
        app.Versapay_Token__c = '8TNJ5BFZBAZW';
        app.genesis__Status__c = 'Offer Accepted';
        app.Contract_Document_Sent__c = false;
        update app;
        
           
        SchedulableContext sc;
        scheduledBatchableForVersapayBatchV2 versapay = new scheduledBatchableForVersapayBatchV2();
        versapay.execute(sc);    
    }
    
    public static testmethod void test_Login(){
        Test.setMock(HttpCalloutMock.class, new Test_MockHttpResponseGenerator());
        Login l = new Login();
        String res =Login.login('','');
    }


}