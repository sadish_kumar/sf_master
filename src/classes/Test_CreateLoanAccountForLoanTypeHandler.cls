@isTest
public class Test_CreateLoanAccountForLoanTypeHandler {
    public static genesis__Applications__c gnAp;
    public static  loan__Loan_Product__c Lprd;
    public static   loan__Office_Name__c LoneOfName;
    public static   loan__MF_Account__c LoanMFacc;
    public static  loan__Currency__c lcurr;
    public static  loan__Fee_Set__c  Lfset;
    public static loan__Loan_Account__c LLA;
    public static loan__Loan_Purpose__c LLP;
    
    public static void init(){
       /*
        gnAp = genesis.TestHelper.createLoanApplication();
        LoneOfName =loan.TestHelper.createOffice();
        LoanMFacc  = loan.TestHelper.createMFAccount('TestAccName','DummyType');
        lcurr      = loan.TestHelper.createCurrency();
        Lfset      =loan.TestHelper.createFeeSet();
         loan__Client__c Loanclient = loan.TestHelper.createClient();
         loan__Loan_Purpose__c LLP = loan.TestHelper.createLoanPurpose();
        
        // Lprd =loan.TestHelper.createLoanProductwithProductType('testProduct',LoneOfName,
                                                                LoanMFacc,
                                                                lcurr,Lfset,'dummyMethod',5,0.9,'dummysetup','loanproduct');
                                                                  */
        loan.TestHelper.createSeedDataForTesting();
        LoneOfName = loan.TestHelper.createOffice(); 
        loan__Client__c Loanclient = loan.TestHelper.createClient();
        LLP = loan.TestHelper.createLoanPurpose();                
        gnAp=[select id,RecordTypeId,genesis__Contact__c,genesis__Product_Type__c,genesis__Loan_Amount__c,genesis__Status__c,
        genesis__Term__c,genesis__Payment_Frequency__c,genesis__Interest_Rate__c,genesis__Days_Convention__c,
        genesis__Expected_First_Payment_Date__c,genesis__Interest_Calculation_Method__c,genesis__Interest_Only_Period__c,
        genesis__Total_Estimated_Interest__c,genesis__Valid_Pricing_Flag__c,genesis__Probability__c,genesis__Payment_Amount__c,
        genesis__Lending_Product__c,Company__r.name,Company__r.id,com_loan__Loan_Purpose__c,Number_of_Payments__c,genesis__Draw_Term__c,
        genesis__Expected_Start_Date__c,genesis__Balloon_Payment__c,Versapay_Token__c,Origination_Fee__c,CreatedDate,
        genesis__Funding_in_Tranches__c,Lending_Account__c from genesis__Applications__c where id=:gnAp.id];
       // loan.TestHelper.createSeedDataForTesting();        
        lcurr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        LoanMFacc = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS'); 
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');             
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(lcurr,dummyIncAccount ,LoanMFacc );                                    
        Lfset = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,Lfset );       
               
        //Create a dummy Loan Product                                                       
        Lprd=loan.TestHelper.createLoanProduct(LoneOfName,
                            LoanMFacc, 
                            lcurr, 
                            Lfset);
        
          LLA = loan.TestHelper.createLoanAccount(Lprd,Loanclient,Lfset,LLP,LoneOfName);
        
        
        
    }
    public TestMethod static void test1(){
        gnAp = genesis.TestHelper.createLoanApplication(); 
        init();     
        gnAp.Company__c=LoneOfName.id;
        gnAp.com_loan__Loan_Purpose__c=LLP.name;
        update gnAp;
        //loan__Bank__c testbank = new loan__Bank__c(name='Test');
        //insert testbank;
        //loan__Bank_Account__c testacc =new loan__Bank_Account__c(name='Test');
        //insert testacc;
        
        CreateLoanAccountForLoanTypeHandler CAFL = new CreateLoanAccountForLoanTypeHandler(gnAp,LLA,Lprd);
        test.startTest();
         CAFL.createLoanAccount();
       // CAFL.validateForTranches();
      //  CAFL.createFirstDisbursal('payment');
        CAFL.createBorrowerACH();
        CAFL.getLoanProductDetails(Lprd.name);
        test.stopTest();
        
        
    }  
     public TestMethod static void test2(){
         
        gnAp = genesis.TestHelper.createLoanApplication(); 
        init();     
        gnAp.Company__c=LoneOfName.id;
        gnAp.com_loan__Loan_Purpose__c='Testing';
        update gnAp;
        CreateLoanAccountForLoanTypeHandler CAFL = new CreateLoanAccountForLoanTypeHandler(gnAp,LLA,Lprd);
        test.startTest();
         CAFL.createLoanAccount();
       // CAFL.validateForTranches();
      //  CAFL.createFirstDisbursal('payment');
        test.stopTest();
        
        
    }  
    
}