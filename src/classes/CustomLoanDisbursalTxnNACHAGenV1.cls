global class CustomLoanDisbursalTxnNACHAGenV1 extends loan.FileGenerator{
//Custom class which will result in blank disbursal file creation...    
    public override String getSimpleFileName(){
        return 'Loan_Disbursals';
    }
    
    public override List<String> getEntries(Loan.TransactionSweepToACHState state, List<SObject> scope){
        return new List<String>();
    }
    
    public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
        return '';   
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
        return '';   
    }
}