@isTest
public class TestDailyInterestAccrualJob{
    
    public static void createDummyData(){
        loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c gnAp= genesis.TestHelper.createLoanApplication(); 
        gnAp.Contract_Document_Status__c='Completed';
        update gnAp;
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         loan__Fee__c dummyFee1 = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         dummyFee1.loan__Time_of_charge__c ='Pre-Paid Fees';
         update dummyFee1;                                 
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;       
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;
        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        loan__Product_Pre_Paid_Fees__c PreFee = new loan__Product_Pre_Paid_Fees__c(loan__Fee_Type__c=dummyFee1.id,
                                                                                   loan__Enabled__c =true,
                                                                                   loan__Lending_Product__c=dummyLP.id,
                                                                                   loan__Amortize_Balance__c='Pre-Paid Fees'
                                                                                    );
        insert PreFee ;
        
         loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        
        loanAccount.loan_vp__Versapay_Token__c = '8TNJ5BFZBAZW';
        loanAccount.loan__Loan_Status__c = 'Active - Good Standing';
        loanAccount.Application__c=gnAp.id;
        //loanAccount.Origination_Fee__c=loanAccount.loan__Loan_Amount__c*0.02;
        loanAccount.Origination_Fee__c = 10000.0;
        update loanAccount;
        
        loan_vp__Versapay_Parameters__c params = new loan_vp__Versapay_Parameters__c();
        params.loan_vp__Versapay_Username__c = 'dummyUserName';
        params.loan_vp__Versapay_Password__c = 'dummyPassword';
    }
    
    public testMethod static void testJob(){
        createDummyData();
        DailyInterestAccrualJob job = new DailyInterestAccrualJob();
        Database.executeBatch(job,8);
        SchedulableContext sc ;
        job.execute(sc);
    }
    
    public testMethod static void testJobException(){
         DailyInterestAccrualJob job = new DailyInterestAccrualJob();
         Database.executeBatch(job,8);
    }
    
    
 
}