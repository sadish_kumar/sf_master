<apex:page id="Offer" controller="LoanOfferController" showHeader="false" 
           standardStylesheets="false" cache="false" title="Confirm Details" action="{!validate}">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link href='//fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,300' rel='stylesheet' type='text/css' />
        <apex:stylesheet value="{!URLFOR($Resource.com_loan__lendified, 'css/bootstrap.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.com_loan__lendified, 'css/style.css')}" />
    </head>
    <body>  
<!-- Data Layer for GTM -->
<script>
dataLayer = [{'userId':'{!applicationTrackId}'}];
</script>
<!-- Data Layer for GTM -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KDK7DR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KDK7DR');</script>
<!-- End Google Tag Manager -->
        <header class="header">
            <div class="header__logo">
                <img src="{!URLFOR($Resource.com_loan__lendified, 'img/logo-small.png')}" alt="" />
            </div>
            <div class="header__message">
                <div class="title">CONGRATS ON YOUR LOAN APPROVAL</div>
                <em>You are just about there, time to make your decision</em>
            </div>
            <div class="progress-bar">
                <div class="pb-section pb-section--summary">
                    <div class="pb-section__summary">
                        <p class="title u-mt-5">Loan Summary</p>
                        <table>
                            <tr>
                                <td><span class="title">Amount:&nbsp;</span></td>
                                <td>
                                    <apex:outputText value="${0, number, ###,###}">
                                        <apex:param value="{!ldm.amount}"/>
                                    </apex:outputText>
                                </td>
                            </tr>
                            <tr>
                                <td><span class="title">Term:&nbsp;</span></td>
                                <td>{!ldm.term} months</td>
                            </tr>
                            <tr>
                                <td><span class="title">Purpose:&nbsp;</span></td>
                                <td>{!ldm.purpose}</td>
                            </tr>
                        </table>
                        
                        <div class="summary__corner summary__corner--top"></div>
                        <div class="summary__corner summary__corner--bottom"></div>
                    </div>
                    <div class="pb-section__ball"></div>
                </div>
                <div class="pb-section pb-section--small pb-section--small">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball pb-section__ball--number">1</div>
                    <div class="pb-section__description">Applicant Details</div>
                </div>
                <div class="pb-section pb-section">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball pb-section__ball--number">2</div>
                    <div class="pb-section__description">ID Verification</div>
                </div>
                <div class="pb-section pb-section">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball pb-section__ball--number">3</div>
                    <div class="pb-section__description">Confirm Details</div>
                </div>
                <div class="pb-section pb-section">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball pb-section__ball--number">4</div>
                    <div class="pb-section__description">Loan Offer</div>
                </div>
                <div class="pb-section pb-section--future">
                <div class="pb-section__bar"></div>
                <div class="pb-section__ball pb-section__ball--number">5</div>
                <div class="pb-section__description" style="margin-bottom: -19px;">Bank Deposit<br/>/Loan Document</div>
                </div>
              <!--  <div class="pb-section pb-section--future">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball pb-section__ball--number">5</div>
                    <div class="pb-section__description">Bank Deposit</div>
                </div>-->
                <div class="pb-section pb-section--small pb-section--future">
                    <div class="pb-section__bar"></div>
                    <div class="pb-section__ball"></div>
                </div>
            </div>
        </header>
        <main>
            <apex:form id="form">
                <c:LoadingUI />
                <apex:actionFunction name="continueAction" action="{!continueAction}" status="loadStatus" reRender="form"/>
                <apex:actionFunction name="abortAction" action="{!abortAction}" />
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="title title--large">LOAN OFFER</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row u-mb-20">
                                
                                <div class="col-xs-12">
                                    <div class="callout">LOAN OFFER DETAILS</div>
                                </div>
                                
                            </div>
                            <div class="row u-mb-20">

                                <div class="col-xs-10 col-sm-8">
                                    <p><small>Attached in the table are the highlights of your loan offer, including the results of our “LendScore” which has been prepared by our credit review system.​ The offer has been calculated from your provided information, and represents the best offer within your chosen parameters.</small></p>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row u-mb-20">
            
                                <div class="col-xs-12">
                                    <table class="offer-table">
                                        <tr>
                                          <td><span class="title">Amount:&nbsp;</span></td>
                                            <td>  <apex:outputText value="${0, number, ###,###}">
                                            <apex:param value="{!ldm.amount}"/>
                                            </apex:outputText></td> 
                                          <!--    <td class="title">Amount</td>
                                            <td>{!ldm.amount}</td> -->
                                        </tr>
                                        <tr>
                                            <td class="title">Term </td>
                                            <td>{!ldm.term} &nbsp; <apex:outputText value="{!If(ldm.term!=null,'Months','')}"></apex:outputText></td>
                                        </tr>
                                        <tr>
                                            <td class="title">Rate</td>
                                            <td>{!rateOfInterest} <apex:outputText value="%" rendered="{!IF(rateOfInterest!=null,true,false)}"/></td>
                                        </tr>
                                        <tr>
                                            <td class="title">Arrangement Fee</td>
                                            <td>{!arrangementFees} <apex:outputText value="%" rendered="{!IF(arrangementFees!=null,true,false)}"/></td>
                                        </tr>
                                        <tr> 
                                               <td class="title">Lend Score</td> 
                                          <!--  <td>{!lendScore}</td> -->
                                         
                                            <td>{!lendScoreGrade}</td>
                                            
                                            
                                        </tr>
                                        <tr>
                                            <td class="title">Bi-weekly Payment</td>
                                            <td>{!paymentAmount}</td>
                                        </tr>
                                        <tr>
                                            <td class="title">Purpose</td>
                                            <td>{!ldm.purpose}</td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                            <div class="row">
                                
                                <div class="col-xs-12">
                                    <br />
                                    <button id="notacceptbtnApply" name="btnApply" class="form-styled btn btn--link u-mb-40"
                                            onclick="showFeedbackBox();return false;">Not Right Now</button>
                                    <button id="btnApply" name="btnApply" class="form-styled btn btn--green"
                                            onclick="continueAction();return false;">Accept</button>
                                </div>
                                <div id="feedbackField" style="display:none;">
                                <div id="feedbackFieldMsg" style="display:none;"> <span style="color:red">please fill the required field</span></div>
                                 Please tell us why you have chosen not to accept:<span style="color:red">*</span><br/>
                                
                                <apex:selectList id="rejectedReasonOption" multiselect="false" size="1" styleClass="form-styled form-styled--select" style="width: 56%;-webkit-appearance: none;" value="{!rejectedReason}">
                                            <apex:selectOptions value="{!rejectedReasonList}"/>
                                        </apex:selectList><p/>  <br/> 
                                 Please tell us about your experience and how we can improve<span style="color:red">*</span><br/>                           
                                 <apex:inputTextarea id="feedback" rows="3" cols="40" value="{!rejectedFeedback}"/><br/><br/>
                                <button id="submitbtnApply" name="btnApply" class="form-styled btn btn--green"
                                            style= "margin-left: 16%;" onclick="checkFeedbackField();return false;">Submit</button> 
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <c:GeoTrustSeal />
                </div>
            </apex:form>
        </main>
    </body>
    <c:CommunityGoogleAnalyticsTracking />
    <script>
    function showFeedbackBox(){
    document.getElementById('feedbackField').style.display = "block";
    document.getElementById('btnApply').style.display = "none";
    }
    function checkFeedbackField(){
      
    if(document.getElementById("{!$Component.form.feedback}").value!='' && document.getElementById("{!$Component.form.rejectedReasonOption}").value!='---Select---'){ 
    document.getElementById('feedbackFieldMsg').style.display = "none";     
    abortAction();
    }
    else
    document.getElementById('feedbackFieldMsg').style.display = "block";
    }
    </script>
</apex:page>