/*
Schedule the job manually:
AcctSeedDailyInterestJournalEntries j = new AcctSeedDailyInterestJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedDailyInterestJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedDailyInterestJournalEntries';
    private boolean submitNextJob = false;
        
    public AcctSeedDailyInterestJournalEntries(){
    }
    public AcctSeedDailyInterestJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedDailyInterestJournalEntries job = new AcctSeedDailyInterestJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
     	
     	Date startDate;
        List<loan_seed__Lending_Product_Accounting_Seed__c> glAccountSettingsRecords
				= [select Id,
						Last_Daily_Interest_Journal_Entry__c
					from loan_seed__Lending_Product_Accounting_Seed__c
					limit 1];
    	
    	if (glAccountSettingsRecords.size() == 0) {
    		startDate = Date.today();
    	} else {
	    	loan_seed__Lending_Product_Accounting_Seed__c glAccountSettings = glAccountSettingsRecords.get(0);
	    	startDate = glAccountSettings.Last_Daily_Interest_Journal_Entry__c;
    	}
    	
    	if (startDate != null) {
    		// set journal entry date to next day (from last date recorded)
    		Date journalEntryDate = startDate + 1;
    		
    		// get all daily interest records for that year
    		String query = 'select Id from Daily_Interest_Accrual__c where Year__c = \''+String.valueOf(journalEntryDate.year())+'\'';
    		
        	return Database.getQueryLocator(query);
    	}
    	return null;
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try {
        	AcctSeedUtilityClass.createDailyInterestJournalEntriesHandler(scope);
	    
	    }catch(Exception e){
              Database.rollback(sp);
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedDailyInterestJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedPostJournalEntries job = new AcctSeedPostJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}