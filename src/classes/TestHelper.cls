Public Class TestHelper{
    
    public static void createEquifaxCRRecords(Map<String,String> fieldsMap){
        
        ints__EfxReport__c er = new ints__EfxReport__c();
        er.ints__FileSinceDate__c = String.valueOf(Date.Today());
        //er.ints__Hit_Code__c,
        //er.ints__Hit_Description__c,
        
        if(fieldsMap != null && fieldsMap.size() > 0){
            for(String fieldName : fieldsMap.Keyset()){
                er.put(fieldName,fieldsMap.get(fieldName));
            }
        }
        
        insert er;
        
        List<ints__EfxReport_Score__c> erScoreList = new List<ints__EfxReport_Score__c>();
        
        ints__EfxReport_Score__c erBeacon = new ints__EfxReport_Score__c();
        erBeacon.ints__efxReport__c = er.Id;
        erBeacon.ints__Description__c = 'BEACON';
        erBeacon.ints__Value__c = '750';
        
        erScoreList.add(erBeacon);
        
        ints__EfxReport_Score__c erBNV = new ints__EfxReport_Score__c();
        erBNV.ints__efxReport__c = er.Id;
        erBNV.ints__Description__c = 'BANK. NAV. INDEX 3';
        erBNV.ints__Value__c = '750';
        
        erScoreList.add(erBNV);
        
        insert erScoreList;
        
    }

}