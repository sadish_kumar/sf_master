@isTest
public class TestApplicationUtil{
    
    public testMethod static void testDeclineApplication(){
        Contact objContact = new Contact();
        objContact.LastName = 'TestContact';
        objContact.FirstName = 'TestData';
        objContact.MailingStreet='sonysignal,koramangala';
        objContact.Birthdate = date.today();
        insert objContact;
        
        genesis__Applications__c objApplication = new genesis__Applications__c();
        objApplication.genesis__Contact__c = objContact.id;
        objApplication.Beacon_9_Score__c=100;
        objApplication.BNI__c=100;
        objApplication.Beacon_9_Loss_Rate__c=100;
        ObjApplication.genesis__Status__c = 'Offer Accepted';
        insert objApplication;
        
        String resp1 = ApplicationUtil.declineApplication(objApplication.Id);
        
        String resp2 = ApplicationUtil.declineApplication(null);
        
        objApplication.genesis__Status__c = 'APPLICATION - CONVERTED';
        update objApplication;
        
        String resp3 = ApplicationUtil.declineApplication(ObjApplication.Id);
        
    
    }
    
    public testMethod static void testConfirmApplication(){
        loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c application = genesis.TestHelper.createLoanApplication();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //create Currency
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        //create a Loan Product
        String Name='loan product';
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProductwithProductType(Name,                                                  
                                                                                         dummyOffice,
                                                                                         dummyAccount,
                                                                                         curr,
                                                                                         dummyFeeSet,
                                                                                         'Interest Only',
                                                                                         40,
                                                                                         12,
                                                                                         null,
                                                                                         'Loan');
         
      
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        Account acc = new Account(Name = name);
        acc.loan__Borrower__c = true;
        insert acc;
        application.Lending_Product__c = dummyLP.id;
        application.Company__c                          = dummyOffice.Id;
        application.com_loan__Loan_Purpose__c =dummyLoanPurpose.id;
        
        application.genesis__Product_Type__c            = 'Loan';
        application.genesis__Expected_Start_Date__c = date.today();
        application.genesis__Status__c = 'Offer Accepted';
        
        update application;
        
        Lendified_Parameters__c lenp = new Lendified_Parameters__c();
        lenp.Default_Company_Id__c=dummyOffice.id;
        lenp.Default_Lending_Product_Id__c=dummyLP.id;
        lenp.Pre_Paid_Fee__c='Origination Fee';
        insert lenp;
    
        ApplicationUtil.convertApplication(application.Id);
    }
    
    
}