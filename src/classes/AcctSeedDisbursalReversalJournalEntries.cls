/*
Schedule the job manually:
AcctSeedDisbursalReversalJournalEntries j = new AcctSeedDisbursalReversalJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedDisbursalReversalJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedDisbursalReversalJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id, loan__Loan_Account__c from loan__Loan_Disbursal_Transaction__c where loan__Cleared__c = true and loan__Reversed__c = true and loan_seed__Journal_Entry__c != null and Reverse_Transaction_Journal_Entry__c = null and loan_vp__Sent_To_Versapay__c = true';
        
    public AcctSeedDisbursalReversalJournalEntries(){
    }
    public AcctSeedDisbursalReversalJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedDisbursalReversalJournalEntries job = new AcctSeedDisbursalReversalJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            List<loan__loan_Account__c > loanAccountList = new List<loan__loan_Account__c >();
            for (loan__Loan_Disbursal_Transaction__c record : (List<loan__Loan_Disbursal_Transaction__c> ) scope) {
            	loanAccountList.add(new loan__loan_Account__c(Id=record.loan__Loan_Account__c));
            }
            AcctSeedUtilityClass.createDisbursalReversalJournalEntries(loanAccountList);
        }catch(Exception e){
              Database.rollback(sp);      

              System.debug('Daily Interest Accrual failed: '+e.getCause()+' '+e.getMessage());
              System.debug(e.getStackTraceString());

              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedDisbursalReversalJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedPaymentJournalEntries job = new AcctSeedPaymentJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}