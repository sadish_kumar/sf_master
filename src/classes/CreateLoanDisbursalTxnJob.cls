global class CreateLoanDisbursalTxnJob implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'Loan Disbursal Creation Job';
    private boolean submitNextJob = false;
    private static final String query = 'Select id,name,loan__Disbursal_Amount__c,loan__Loan_Amount__c,loan_vp__Versapay_Token__c from loan__loan_Account__c where loan__Loan_Status__c = \'Approved\' and loan__Invalid_Data__c=false and Application__r.Contract_Document_Status__c=\'Completed\'';
        
    public CreateLoanDisbursalTxnJob(){
    }
    
    public void execute(SchedulableContext sc){
        CreateLoanDisbursalTxnJob job = new CreateLoanDisbursalTxnJob();
        Database.executeBatch(job,8);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
            
        try{
            List<loan__loan_Account__c > loanAccountList= (List<loan__loan_Account__c > ) scope;
            CreateLoanDisbursalTxnHandler handler = new CreateLoanDisbursalTxnHandler(loanAccountList);
            handler.processLoanAccounts();
        }catch(Exception e){
              //Database.rollback(sp);      

              System.debug('Disbursal creation failed: '+e.getCause()+' '+e.getMessage());
              System.debug(e.getStackTraceString());

              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'Disbursal creation failed: '+e.getCause()+' '+e.getMessage();
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){}
    
    global void submitNextJob() {
        
    }
}