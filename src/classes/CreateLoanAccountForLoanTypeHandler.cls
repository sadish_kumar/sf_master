public class CreateLoanAccountForLoanTypeHandler{
    loan__Loan_Product__c loanProduct = new loan__Loan_Product__c();
    loan__Loan_Account__c loanAccount = new loan__Loan_Account__c();
    genesis__Applications__c application = new genesis__Applications__c();
    List<loan__Product_Pre_Paid_Fees__c> feeList = new List<loan__Product_Pre_Paid_Fees__c>();
    
    private loan.GlobalLoanUtilFacade loanUtil;
    private Date systemDate;
    public CreateLoanAccountForLoanTypeHandler(genesis__Applications__c application ,loan__Loan_Account__c loanAccount,loan__Loan_Product__c loanProduct){
        this.application = application;
        this.loanAccount = loanAccount;
        this.loanProduct = loanProduct;
        loanUtil = new loan.GlobalLoanUtilFacade();
        systemDate = loanUtil.getCurrentSystemDate();
    }
    private loan__Payment_Mode__c pmtMode = new loan__Payment_Mode__c();
    public String createLoanAccount(){
        Savepoint sp = Database.setSavepoint();
        genesis__Applications__c loanApplication = this.application;      
        
        if(String.isNotEmpty(loanApplication.com_loan__Loan_Purpose__c)){
            String purpose = loanApplication.com_loan__Loan_Purpose__c.length() < 50 ? loanApplication.com_loan__Loan_Purpose__c : loanApplication.com_loan__Loan_Purpose__c.substring(0,50);
            List<loan__Loan_Purpose__c> lp = [Select id,name from loan__Loan_Purpose__c where name = : loanApplication.com_loan__Loan_Purpose__c];
            if(lp != null && lp.size() > 0){
                loanAccount.loan__loan_Purpose__c = lp[0].Id;
            }
            else{
                loan__Loan_Purpose__c l = new loan__Loan_Purpose__c();
                l.name = loanApplication.com_loan__Loan_Purpose__c;
                l.loan__Loan_Purpose_Code__c = purpose;
                l.loan__Description__c = loanApplication.com_loan__loan_Purpose__c;
                try{
                    insert l;
                    loanAccount.loan__Loan_Purpose__c = l.Id;
                }catch(Exception e){
                }
            }
        }                                          
        loanAccount.loan__Frequency_of_Loan_Payment__c = loanApplication.genesis__Payment_Frequency__c;
        loanAccount.loan__Loan_Amount__c = loanApplication.genesis__Loan_Amount__c; //loanApplication.Total_Loan_Amount__c; //
        loanAccount.loan__Number_of_Installments__c = loanApplication.Number_of_Payments__c;
        loanAccount.loan__Frequency_Of_Loan_Payment__c = loanApplication.genesis__Payment_Frequency__c;
        loanAccount.loan__draw_term__c = loanApplication.genesis__Draw_Term__c;
        loanAccount.loan__contractual_Interest_Rate__c = loanApplication.genesis__Interest_Rate__c;
        loanAccount.loan__Product_Type__c = loanApplication.genesis__Product_Type__c;
        loanAccount.loan__Expected_Disbursal_Date__c = loanApplication.genesis__Expected_Start_Date__c;
        
        loanAccount.loan__Balloon_Payment__c = loanApplication.genesis__Balloon_Payment__c;
        
        system.debug(';;' + loanApplication.genesis__Interest_Only_Period__c);
       
        loanAccount.loan__Interest_Only_Period__c = loanApplication.genesis__Interest_Only_Period__c;
       
        
        loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();
        
        loanAccount.loan__Approval_Date__c = info.getCurrentSystemDate();
        loanAccount.loan__Disbursal_Date__c = loanApplication.genesis__Expected_Start_Date__c; 
        loanAccount.loan__Contractual_Due_Day__c = loanApplication.genesis__Expected_First_Payment_Date__c.day();
        loanAccount.loan__Expected_Repayment_Start_Date__c = loanApplication.genesis__Expected_First_Payment_Date__c;
        loanAccount.loan__Next_Installment_Date__c = loanApplication.genesis__Expected_First_Payment_Date__c;
        loanAccount.loan__First_Installment_Date__c = loanApplication.genesis__Expected_First_Payment_Date__c;
        loanAccount.loan__Due_Day__c = loanApplication.genesis__Expected_First_Payment_Date__c.day();
        loanAccount.loan__Term_Cur__c = loanApplication.Number_of_Payments__c;
        loanAccount.loan__Application_Date__c = Date.valueOf(loanApplication.CreatedDate);
       
        RecordType rT = [select Id,
                                Name,
                                SobjectType
                           from RecordType
                           where Name =: loanApplication.genesis__Product_Type__c 
                           and SObjectType =: 'loan__Loan_Account__c' LIMIT 1];
        
        
        /*genesis__Loan_Product__c appLoanProd = [Select Name
                                                from genesis__Loan_Product__c
                                                where Id =: loanApplication.genesis__Lending_Product__c];
        */
        
        loanAccount.RecordTypeId = rT.Id;                                        
        
        
        //getLoanProductDetails(appLoanProd.Name);
        
        //get Loan Product info to Loan Account...
        loanAccount.loan__Loan_Product_Name__c = loanProduct.id;
        loanAccount.loan__Interest_Calculation_Method__c = loanProduct.loan__Interest_Calculation_Method__c;            
        //loanAccount.loan__Frequency_Of_Loan_Payment__c = loanProduct.loan__Frequency_Of_Loan_Payment__c;
        loanAccount.loan__Time_Counting_Method__c = loanProduct.loan__Time_Counting_Method__c;
        loanAccount.loan__Delinquency_Grace_Days__c = loanProduct.loan__Delinquency_Grace_Days__c;
        loanAccount.loan__Write_off_Tolerance_Amount__c = loanProduct.loan__Write_off_Tolerance_Amount__c;
        loanAccount.loan__Grace_Period_for_Repayments__c = loanProduct.loan__Late_Charge_Grace_Days__c;
        loanAccount.loan__LA_Amortized_Balance_Type__c = loanProduct.loan__Amortize_Balance_type__c;
        loanAccount.loan__LA_Amortization_Frequency__c = loanProduct.loan__Amortization_Frequency__c;
        loanAccount.loan__LA_Amortization_Enabled__c = loanProduct.loan__Amortization_Enabled__c;
        loanAccount.loan__LA_Amortization_Calculation_Method__c = loanProduct.loan__Amortization_Calculation_Method__c;
        //loanAccount.loan__Term_Cur__c=loanAccount.loan__Number_Of_Installments__c;

        loanAccount.loan__Fee_Set__c = loanProduct.loan__Fee_Set__c;

        loanAccount.loan__Pre_Bill_Days__c = loanProduct.loan__Pre_Bill_Days__c;
        loanAccount.loan__Pmt_Amt_Cur__c = loanAccount.loan__Payment_Amount__c;
        loanAccount.loan__Write_off_Tolerance_Amount__c = loanProduct.loan__Write_off_Tolerance_Amount__c;
        //loanAccount.loan__Interest_Only_Period__c = loanProduct.loan__Interest_Only_Period__c==null?0:loanProduct.loan__Interest_Only_Period__c;
        
        String companyName = loanApplication.Company__r.Name;
        
        if(!Test.isRunningTest()){
            //loanAccount.loan__Branch__c = [select Id from loan__Office_Name__c where Name=:companyName limit 1].Id;
            loanAccount.loan__Branch__c = loanApplication.Company__r.Id;
        }
        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_PARTIAL_APPLICATION;
        loanAccount.Application__c = application.Id;
        loanAccount.loan__Interest_Type__c = 'Fixed';
        
        if(loanApplication.genesis__Days_Convention__c.equals('30/360')){
            loanAccount.loan__Time_Counting_Method__c = loan.LoanConstants.TIME_COUNTING_MONTH_AND_DAYS;
        }else if(loanApplication.genesis__Days_Convention__c.equals('365/365')){
            loanAccount.loan__Time_Counting_Method__c = loan.LoanConstants.TIME_COUNTING_ACTUAL_DAYS;
        }
        /*if(loanApplication.genesis__Funding_in_Tranches__c == true){
            loanAccount.loan__Funding_in_Tranches__c = loanApplication.genesis__Funding_in_Tranches__c;
            loanAccount.loan__Draw_Period_End_Date__c = loanApplication.genesis__Draw_Period_End_Date__c;
            string msg = validateForTranches();
            if(!String.isEmpty(msg))
                return msg; 
            //loanAccount.loan__Disbursal_Amount__c= loanApplication.genesis__Loan_Amount__c ;
            //loanAccount.loan__Disbursed_Amount__c= 0;
            loanAccount.loan__Draw_Billing_Method__c = loanProduct.loan__Draw_Billing_Method__c;
            
            
        }*/
        
        //set Financed Amount as loan application's Loan Amount..
        //on creating pre paid fee this amount will be reduced...
        loanAccount.loan__Disbursal_Amount__c = loanApplication.genesis__Loan_Amount__c; //loanApplication.Total_Loan_Amount__c;
        
        //push versapay token is pushed from application to CL Contract...
        loanAccount.loan_vp__Versapay_Token__c = loanApplication.Versapay_Token__c;
        
        //add Origination Fee...
        loanAccount.Origination_Fee__c = loanApplication.Origination_Fee__c;
        
        try{
            if(!Test.isRunningTest()){
                insert loanAccount;
            }
        }catch(Exception e){
            Database.Rollback(sp);
            return e.getMessage();
        }
        if(loanApplication.genesis__Funding_in_Tranches__c == false){
            if(!Test.isRunningTest())
                loan.RegenerateAmortizationScheduleCtrl.regenerateAmortizationSchedule(loanAccount.id);
        }
        
        loanAccount.loan__Loan_Status__c = loan.LoanConstants.LOAN_STATUS_APPROVED;
        //loanAccount.loan__Interest_estimated__c = totalInterest;
        loanAccount.loan__Last_Installment_Date__c =  loan.DateUtil.addCycle(loanAccount.loan__First_Installment_Date__c ,
                                                        (Integer)loanAccount.loan__First_Installment_Date__c.day(),
                                                        loanAccount.loan__Frequency_of_Loan_Payment__c,
                                                        (Integer)(loanAccount.loan__Term_Cur__c-1) );
        loanAccount.loan__Maturity_Date_Current__c = loan.DateUtil.addCycle(loanAccount.loan__First_Installment_Date__c ,
                                                        (Integer)loanAccount.loan__First_Installment_Date__c.day(),
                                                        loanAccount.loan__Frequency_of_Loan_Payment__c,
                                                        (Integer)(loanAccount.loan__Term_Cur__c-1) ); 
                                                                                               
        
        loanApplication.genesis__Status__c = 'APPLICATION - CONVERTED';
         //genesis.LendingConstants.APPL_STATUS_NEW_APPROVED_CONVERTED;
        loanAccount.Versapay_Token_Status__c = loanApplication.Versapay_Token_Status__c;
        
        feeList = [Select id,loan__Lending_Product__c,loan__Fee_Type__r.Name,loan__Fee_Type__r.loan__Amount__c,loan__Fee_Type__c 
                                                        from loan__Product_Pre_Paid_Fees__c 
                                                        where loan__Lending_Product__c = :loanProduct.Id and loan__Enabled__c=true and loan__Amortize_Balance__c='Pre-Paid Fees'];
        
        if(feeList == null || feeList.size() == 0){
            Database.Rollback(sp);
            return 'Error : Pre-Paid Fee not available on Lending Product.';
        }
        
        
        try{
            if(!Test.isRunningTest()){
                createBorrowerACH();
                update loanAccount;
                createFirstDisbursal('ACH');
                loanApplication.Lending_Account__c = loanAccount.Id;
                update loanApplication;
                //update application;
            }
        }catch(Exception e){
            Database.Rollback(sp);
            return e.getMessage()+ e.getLineNumber();
        }
        //reference application's attachments,collaterals and notes to loan account...
        //getCollateralAndDocuments();
        /*loan__Loan_Account__c loanAcc;
        
        if(!Test.isRunningTest())
            loanAcc = [select Id,Name from loan__Loan_Account__c where Id=:loanAccount.id];
        else
            loanAcc =  new loan__loan_Account__c();
        */
            
        //String msg = '<h3><a href="' + URL.getSalesforceBaseUrl().toExternalForm()+'/'+loanAcc.Id +'">'+loanAcc.Name+'</a></h3>.';
        
        return 'Application converted to Loan.'; //+ msg;        
        //return loanAccount.Id;
    }
    
    /*private String validateForTranches(){
        
        if(loanProduct.loan__Funding_in_Tranches__c != true)
            return 'Error: Funding in Tranches is not checked in Lending Product.';
        if(loanAccount.loan__Draw_Period_End_Date__c == null)
            return 'Error: Draw Period End Date cannot be empty for Funding in Tranches';
        if(loanAccount.loan__Expected_Repayment_Start_Date__c <= loanAccount.loan__Draw_Period_End_Date__c.addDays(Integer.valueOf(loanAccount.loan__Pre_Bill_Days__c)))
            return 'Error: Repayment start Date should be greater than Draw Period End Date and Pre Bill days.';
        
        return null;
        
    }*/
    
    
    private void createFirstDisbursal(String mode){
        Map<Id,loan__Product_Pre_Paid_Fees__c> originationFeeMap = new Map<Id,loan__Product_Pre_Paid_Fees__c>();
        Lendified_Parameters__c lpParams = Lendified_Parameters__c.getOrgDefaults();
        
        //create Pre-Paid Fee first...
        /*for(loan__Product_Pre_Paid_Fees__c fee : feeList){
            if(lpparams != null && fee.loan__Fee_Type__r.Name.equals(lpParams.Pre_Paid_Fee__c) && !originationFeeMap.containsKey(fee.loan__Lending_Product__c))
                originationFeeMap.put(fee.loan__Lending_Product__c,fee);
        }*/
        
        
        loan__Contract_Pre_Paid_Fee__c f = new loan__Contract_Pre_Paid_Fee__c();
        f.loan__Contract__c = loanAccount.Id;
        f.loan__Amount__c = loanAccount.Origination_Fee__c;
        f.loan__Amortize_Balance__c = 'Pre-Paid Fees';
        f.loan__Add_Fee_to_Loan_Amount__c = false;
        f.loan__Fee_Type__c = feeList[0].loan__Fee_Type__c;
        try{
            if(!Test.isRunningTest())
                insert f;
        }catch(Exception e){
            system.debug(e.getMessage() + e.getLineNumber());
        }
        
                        
        loan__Payment_Mode__c paymentMode = [Select Id from loan__Payment_Mode__c where Name=: 'Cash'];
        if(!String.isEmpty(mode) && [Select Id from loan__Payment_Mode__c where Name=: mode].size() > 0)
            paymentMode = [Select Id from loan__Payment_Mode__c where Name=: mode];
        
        loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();
        
        loan__Loan_Disbursal_Transaction__c disbursal =  new loan__Loan_Disbursal_Transaction__c();
        disbursal.loan__Disbursal_Date__c = loanAccount.loan__Expected_Disbursal_Date__c;
        disbursal.loan__Disbursed_Amt__c =  loanAccount.loan__Loan_Amount__c;
        disbursal.loan__Loan_Account__c = loanAccount.id;
        disbursal.loan__Mode_of_Payment__c = paymentMode.id;
        disbursal.loan__Financed_Amount__c = loanAccount.loan__Disbursal_Amount__c - loanAccount.Origination_Fee__c;
        disbursal.loan__Cleared__c = true;
        
        try{
            if(!Test.isRunningTest())
                insert disbursal;
        }catch(Exception e){
            system.debug(e.getMessage() + e.getLineNumber());
        }
        
    }
    
    
 /*   private void getCollateralAndDocuments(){
        List<ints__Collateral__c> collateralList = [Select Id from ints__Collateral__c where application__c =: application.id];
        List<loan__Loan_Collateral__c> loanCollateralList = new List<loan__Loan_Collateral__c>();
        for(ints__Collateral__c collateral:collateralList){
            loan__Loan_Collateral__c loanCollateral = new loan__Loan_Collateral__c();
            loanCollateral.loan__Loan__c = loanAccount.Id;
            loanCollateral.Collaterals__c = collateral.Id;
            loanCollateralList.add(loanCollateral);
        }
        List<Attachment> applicationAttachmentList = [Select parentid,name,body from Attachment where parentid =: application.id];
        List<Attachment> loanAttachmentList = new List<Attachment>();
        for(Attachment applicationAttachment : applicationAttachmentList ){
            Attachment loanAttachment =  new Attachment();
            loanAttachment.parentid = loanAccount.id;
            loanAttachment.name = applicationAttachment.name;
            loanAttachment.body = applicationAttachment.body;
            
            loanAttachmentList.add(loanAttachment);
        }
        
        try{
            if(!Test.isRunningTest()){
                insert loanAttachmentList;
                insert loanCollateralList;
            }
        }catch(Exception e){}
    
    
    } */
    
    @TestVisible private void getLoanProductDetails(String loanProductName) {
        loanProduct = [select ID, Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Fee_Set__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Loan_Cycle_Setup__c,
            loan__Loan_Product_Type__c,
            loan__Repayment_Procedure__c,
            loan__Interest_Calculation_Method__c,
            loan__Frequency_Of_Loan_Payment__c,
            loan__Funder__c,
            loan__Interest_Only_Period__c,
            loan__Time_Counting_Method__c,
            loan__Write_off_Tolerance_Amount__c,
            loan__Pre_Bill_Days__c,
            loan__Delinquency_Grace_Days__c,
            loan__Late_Charge_Grace_Days__c,
            loan__Amortize_Balance_type__c,
            loan__Amortization_Frequency__c,
            loan__Amortization_Enabled__c,
            loan__Amortization_Calculation_Method__c,
            loan__Grace_Period_for_Repayments__c,
            (select ID,Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Fee_Set__c,
            loan__Cycle_Number__c 
            from loan__Loan_Product_Cycle__r)
            from loan__Loan_Product__c where Name =: loanProductName];
        system.debug(Logginglevel.INFO,'Loan product '+ loanProduct );                                

    }
    
    @TestVisible private void createBorrowerACH(){
   
        List<loan__Bank__c> defaultBank = [Select Id,Name from loan__Bank__c limit 1];
        if(defaultBank != null && defaultBank.size() != 0){
            loanAccount.loan__ACH_Bank__c = defaultBank[0].Id;
        }
        
        List<loan__Bank_Account__c> borrowerACH = [Select Id,name from loan__Bank_Account__c limit 1];
        if(borrowerACH != null && borrowerACH.size() != 0){
            loanAccount.loan__Borrower_ACH__c = borrowerACH[0].Id;
        }
        
        loanAccount.loan__ACH_Account_Number__c = '234321';
        
        loanAccount.loan__ACH_Account_Type__c = 'Saving';
        loanAccount.loan__ACH_Bank_Name__c = 'Versapay Bank';
        loanAccount.loan__ACH_Debit_Amount__c = application.genesis__Payment_Amount__c;
        loanAccount.loan__Ach_Debit_Day__c = application.genesis__Expected_First_Payment_Date__c.day();
        //loanAccount.loan__ACH_Drawer_Name__c = application.;
        loanAccount.loan__ACH_End_Date__c = loanAccount.loan__Maturity_Date_Current__c;
        loanAccount.loan__ACH_Frequency__c = application.genesis__Payment_Frequency__c;
         
        //loanAccount.loan__Borrower_ACH__c = Id.valueOf('a3917000000KbX4AAK');
        //loanAccount.loan__ACH_Relationship_Type__c = application.ACH_Relationship_Type__c;
        
        loanAccount.loan__ACH_Routing_Number__c = '123456789';
        loanAccount.loan__ACH_Start_Date__c = application.genesis__Expected_First_Payment_Date__c;
        loanAccount.loan__ACH_Next_debit_date__c = application.genesis__Expected_First_Payment_Date__c;
        loanAccount.loan__ACH_On__c = true;
        //loanAccount.loan_vp__Versapay_token__c = application.PAD_Agreement_Token__c;
        
    }
          
}