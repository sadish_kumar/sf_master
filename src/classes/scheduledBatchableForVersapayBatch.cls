global class scheduledBatchableForVersapayBatch implements Schedulable{
    
    //private String sessionId;
    
    /*global scheduledBatchableForVersapayBatch(String sessionId){
        this.sessionId = sessionId;
    }
    */
    global void execute(SchedulableContext sc) {      
          VersapayBatchClass v = new VersapayBatchClass(); 
      //  VersapayBatchClass v = new VersapayBatchClass(sessionId); 
        database.executebatch(v,1);
    }
}