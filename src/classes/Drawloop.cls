public class Drawloop{
    
    /*
    deployId - Record Id of the deployment method, in this case Docusign.
    templateId - Record Id for DDP template record.
    loanAppId - Loan Application record Id.
    sId - User Session Id.
    */
    
    @future(callout = true)
    public static void generateDDP(String deployId,String templateId,String loanAppId,String sId){
        Map<string, string> variables = new Map<string,string>();
        variables = new Map<string, string> { 'deploy' => deployId };
        Loop.loopMessage lm = new Loop.loopMessage();
        lm.sessionId=sId;
        lm.requests.add(new Loop.loopMessage.loopMessageRequest(loanAppId,templateId,variables));
        lm.sendAllRequests();
    }
}