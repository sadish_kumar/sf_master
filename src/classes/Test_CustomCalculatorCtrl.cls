@isTest
public Class Test_CustomCalculatorCtrl{
   public testMethod static void testCal(){ 
     loan.TestHelper.createSeedDataForTesting();  
      Account acc = new Account();
      acc.Name = 'Ext';
      insert acc;
      
       /*genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id,genesis__Loan_Amount__c=1000);
       insert obj;*/
       genesis__applications__c obj = genesis.TestHelper.createLoanApplication();
       obj.genesis__Interest_Calculation_Method__c = null;
       obj.genesis__Expected_Start_Date__c  = date.today();
      // obj.genesis__Days_Convention__c      = '10';
       obj.genesis__Interest_Only_Period__c = 12;
       obj.genesis__Balloon_Payment__c      = 0;  
       obj.genesis__Term__c = 12;  
       update obj;
       
       loan__Bank_Account__c lba = new loan__Bank_Account__c(loan__Bank_Account_Number__c='123', loan__Bank_Name__c='test',loan__Account__c=acc.Id );
       insert lba; 
       
       system.debug('****obj'+obj.genesis__Term__c );
      
       system.debug('****Number of Payments'+obj.Number_of_Payments__c);
       
       List<genesis__Loan_Application_Loan_Mapping__c> listMap = new List<genesis__Loan_Application_Loan_Mapping__c>();
       genesis__Loan_Application_Loan_Mapping__c map1 = new genesis__Loan_Application_Loan_Mapping__c();
       map1.name='Amortizable Amount';
       map1.genesis__Application_Field_Name__c= 'Loan_Amount__c';
       listMap.add(map1);       
       genesis__Loan_Application_Loan_Mapping__c map2 = new genesis__Loan_Application_Loan_Mapping__c();
       map2.Name='Interest Rate';
       map2.genesis__Application_Field_Name__c= 'Interest_Rate__c';
       listMap.add(map2);
       genesis__Loan_Application_Loan_Mapping__c map3 = new genesis__Loan_Application_Loan_Mapping__c();
       map3.Name='Expected First Payment Date';
       map3.genesis__Application_Field_Name__c= 'Expected_First_Payment_Date__c';
       listMap.add(map3);
       genesis__Loan_Application_Loan_Mapping__c map4 = new genesis__Loan_Application_Loan_Mapping__c();
       map4.Name='Payment Amount';
       map4.genesis__Application_Field_Name__c= 'Payment_Amount__c';
       listMap.add(map4);
       genesis__Loan_Application_Loan_Mapping__c map5 = new genesis__Loan_Application_Loan_Mapping__c();
       map5.Name='Payment Frequency';
       map5.genesis__Application_Field_Name__c= 'Payment_Frequency__c';
       listMap.add(map5);
       genesis__Loan_Application_Loan_Mapping__c map6 = new genesis__Loan_Application_Loan_Mapping__c();
       map6.Name='Term';
       map6.genesis__Application_Field_Name__c= 'Term__c';
       listMap.add(map6);
       genesis__Loan_Application_Loan_Mapping__c map7 = new genesis__Loan_Application_Loan_Mapping__c();
       map7.Name='Expected Close Date';
       map7.genesis__Application_Field_Name__c= 'Expected_Close_Date__c';
       listMap.add(map7);
       insert listMap;
       genesis__Amortization_Schedule__c gAM = new genesis__Amortization_Schedule__c();
       gAM.genesis__Application__c           = obj.Id;
       insert gAM;                 

       Test.startTest();           
       ApexPages.StandardController com = new ApexPages.StandardController(obj);
       CustomCalculatorCtrl custCal = new CustomCalculatorCtrl(com);
       custCal.loanApp=obj;
       //custCal.loanApplicationID = obj.Id;
       
       custCal.termsPresent=true;
       custCal.totalAmount=1000;
       custCal.totalPrincipal=1000;
       custCal.totalInterest=1000;
       custCal.paymentAmount=1000;
       custCal.isSF1Request=true;
       custCal.apr=1000;
       custCal.renderAsValue='pdf';
       //custCal.saveAsPdf();
       custCal.getDynamicPageSection();
       custCal.getDynamicPgSectionTerms();
       custCal.initEntries();
       custCal.reset();
       custCal.generateSchedule();
       custCal.refreshCtrlRecord();
       custCal.save();
       custCal.regenerateSchedule();
       custCal.setBillingParams();
       custCal.valueChanged();
       custCal.saveAsPdf();
      // insert listMap;
       custCal.initEntries();
       custCal.loanApp.genesis__Expected_Start_Date__c = Date.today();
       custCal.generateSchedule();
       custCal.getGeneratedSchedule();
       custCal.isPreview = true ;
       custCal.save();
       custCal.loanApp.genesis__Interest_Only_Period__c = 1;
       custCal.loanApp.genesis__Interest_Calculation_Method__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Interest_Rate__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Payment_Frequency__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Expected_First_Payment_Date__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Loan_Amount__c = null;
       custCal.generateSchedule();
       Test.stopTest();
    }
   
    public testMethod static void testCal1(){ 
     loan.TestHelper.createSeedDataForTesting();  
      Account acc = new Account();
      acc.Name = 'Ext';
      insert acc;
      
       /*genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id,genesis__Loan_Amount__c=1000);
       insert obj;*/
       genesis__applications__c obj = genesis.TestHelper.createLoanApplication();
       obj.genesis__Interest_Calculation_Method__c = null;
       obj.genesis__Expected_Start_Date__c  = date.today();
       obj.genesis__Days_Convention__c      = '10';
       obj.genesis__Interest_Only_Period__c = 12;
       obj.genesis__Balloon_Payment__c      = 0;  
       obj.genesis__Term__c = 12;  
       update obj;
       
       loan__Bank_Account__c lba = new loan__Bank_Account__c(loan__Bank_Account_Number__c='123', loan__Bank_Name__c='test',loan__Account__c=acc.Id );
       insert lba; 
       
       system.debug('****obj'+obj.genesis__Term__c );
      
       system.debug('****Number of Payments'+obj.Number_of_Payments__c);
       
       List<genesis__Loan_Application_Loan_Mapping__c> listMap = new List<genesis__Loan_Application_Loan_Mapping__c>();
       genesis__Loan_Application_Loan_Mapping__c map1 = new genesis__Loan_Application_Loan_Mapping__c();
       map1.name='Amortizable Amount';
       map1.genesis__Application_Field_Name__c= 'Loan_Amount__c';
       listMap.add(map1);       
       genesis__Loan_Application_Loan_Mapping__c map2 = new genesis__Loan_Application_Loan_Mapping__c();
       map2.Name='Interest Rate';
       map2.genesis__Application_Field_Name__c= 'Interest_Rate__c';
       listMap.add(map2);
       genesis__Loan_Application_Loan_Mapping__c map3 = new genesis__Loan_Application_Loan_Mapping__c();
       map3.Name='Expected First Payment Date';
       map3.genesis__Application_Field_Name__c= 'Expected_First_Payment_Date__c';
       listMap.add(map3);
       genesis__Loan_Application_Loan_Mapping__c map4 = new genesis__Loan_Application_Loan_Mapping__c();
       map4.Name='Payment Amount';
       map4.genesis__Application_Field_Name__c= 'Payment_Amount__c';
       listMap.add(map4);
       genesis__Loan_Application_Loan_Mapping__c map5 = new genesis__Loan_Application_Loan_Mapping__c();
       map5.Name='Payment Frequency';
       map5.genesis__Application_Field_Name__c= 'Payment_Frequency__c';
       listMap.add(map5);
       genesis__Loan_Application_Loan_Mapping__c map6 = new genesis__Loan_Application_Loan_Mapping__c();
       map6.Name='Term';
       map6.genesis__Application_Field_Name__c= 'Term__c';
       listMap.add(map6);
       genesis__Loan_Application_Loan_Mapping__c map7 = new genesis__Loan_Application_Loan_Mapping__c();
       map7.Name='Expected Close Date';
       map7.genesis__Application_Field_Name__c= 'Expected_Close_Date__c';
       listMap.add(map7);
       insert listMap;
       genesis__Amortization_Schedule__c gAM = new genesis__Amortization_Schedule__c();
       gAM.genesis__Application__c           = obj.Id;
       insert gAM;                 

       Test.startTest();           
       ApexPages.StandardController com = new ApexPages.StandardController(obj);
       CustomCalculatorCtrl custCal = new CustomCalculatorCtrl(com);
       custCal.loanApp=obj;
       //custCal.loanApplicationID = obj.Id;
       
       custCal.termsPresent=true;
       custCal.totalAmount=1000;
       custCal.totalPrincipal=1000;
       custCal.totalInterest=1000;
       custCal.paymentAmount=1000;
       custCal.isSF1Request=true;
       custCal.apr=1000;
       custCal.renderAsValue='pdf';
       //custCal.saveAsPdf();
       custCal.getDynamicPageSection();
       custCal.getDynamicPgSectionTerms();
       custCal.initEntries();
       custCal.reset();
       custCal.generateSchedule();
       custCal.refreshCtrlRecord();
       custCal.save();
       custCal.regenerateSchedule();
       custCal.setBillingParams();
       custCal.valueChanged();
       custCal.saveAsPdf();
      // insert listMap;
       custCal.initEntries();
       custCal.loanApp.genesis__Expected_Start_Date__c = Date.today();
       custCal.generateSchedule();
       custCal.getGeneratedSchedule();
       custCal.isPreview = true ;
       custCal.save();
       custCal.loanApp.genesis__Interest_Only_Period__c = 1;
       custCal.loanApp.genesis__Interest_Calculation_Method__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Interest_Rate__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Payment_Frequency__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Expected_First_Payment_Date__c = null;
       custCal.generateSchedule();
       custCal.loanApp.genesis__Loan_Amount__c = null;
       custCal.generateSchedule();
       obj.genesis__Expected_Start_Date__c=null;
       update obj;
       custCal.initEntries();
       Test.stopTest();
    }
       
     public testMethod static void testCal2(){      
     
       loan.TestHelper.createSeedDataForTesting();  
      Account acc = new Account();
      acc.Name = 'Ext';
      insert acc;
      
       /*genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id,genesis__Loan_Amount__c=1000);
       insert obj;*/
       genesis__applications__c obj = genesis.TestHelper.createLoanApplication();
       obj.genesis__Interest_Calculation_Method__c = null;
       obj.genesis__Expected_Start_Date__c  = date.today();
       obj.genesis__Days_Convention__c      = '10';
       obj.genesis__Interest_Only_Period__c = 12;
       obj.genesis__Balloon_Payment__c      = 0;  
       obj.genesis__Term__c = 12;  
       update obj;
       ApexPages.StandardController com = new ApexPages.StandardController(obj);
       CustomCalculatorCtrl custCal = new CustomCalculatorCtrl(com);    
       custCal.loanApp=obj;    
       custCal.initEntries();
        List<genesis__Loan_Application_Loan_Mapping__c> listMap = new List<genesis__Loan_Application_Loan_Mapping__c>();
       genesis__Loan_Application_Loan_Mapping__c map1 = new genesis__Loan_Application_Loan_Mapping__c();
       map1.name='test';
       map1.genesis__Application_Field_Name__c= 'test';
       listMap.add(map1); 
       insert listMap;
       custCal.generateSchedule();
       }
       
   
}