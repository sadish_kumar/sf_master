global class scheduledBatchableForVersapayBatchV2 implements Schedulable{
    
    
    global void execute(SchedulableContext sc) {      
        VersapayBatchClassV2 v = new VersapayBatchClassV2(); 
        database.executebatch(v,1);
    }
}