/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class Test_EquifaxIntegration {
    
    static testMethod void myUnitTest() {
        Contact objContact = new Contact();
        objContact.LastName = 'TestContact';
        objContact.FirstName = 'TestData';
        objContact.MailingStreet='sonysignal,koramangala';
        objContact.Birthdate = date.today();
        insert objContact;
        
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        
        Loss_Rate_Decile_Comparator__c lrdc = new Loss_Rate_Decile_Comparator__c();
        lrdc.Max_Range__c=100;
        lrdc.Min_Range__c=100;
        lrdc.Loss_Rate__c=10;
        lrdc.Decile__c=10;
        lrdc.recordtypeId=beacon9RTID;
        insert lrdc;
        Loss_Rate_Decile_Comparator__c lrdc1 = new Loss_Rate_Decile_Comparator__c();
        lrdc1.Max_Range__c=100;
        lrdc1.Min_Range__c=100;
        lrdc1.Loss_Rate__c=10;
        lrdc1.Decile__c=10;
        lrdc1.recordtypeId=bni3RTID;
        insert lrdc1;
        Lend_Score__c ls = new Lend_Score__c();
        ls.Decile__c=10;
        ls.Applied_Interest_Rate__c=10;
        ls.Lend_Score_Max_Value__c=100;
        ls.Lend_Score_Min_Value__c=100;
        insert ls;
        genesis__Applications__c objApplication = new genesis__Applications__c();
        objApplication.genesis__Contact__c = objContact.id;
        objApplication.Beacon_9_Score__c=100;
        objApplication.BNI__c=100;
        objApplication.Beacon_9_Loss_Rate__c=100;
        insert objApplication;
        
        
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('ints__Contact__c',objContact.Id);
        fieldMap.put('application__c',objApplication.Id);
        TestHelper.createEquifaxCRRecords(fieldMap);
        
        Contact_Mapping_For_Equifax_Testing__c objContactMapping = new Contact_Mapping_For_Equifax_Testing__c();
        objContactMapping.Name =  'TestData';
        objContactMapping.Eid_Verification_Contact__c = objContact.id;
        insert objContactMapping;
        
        ints__EfxReport__c objEfxReport = new ints__EfxReport__c();
        objEfxReport.Application__c  = objApplication.id;
        objEfxReport.ints__Contact__c = objContact.id;
        insert  objEfxReport ;
        
        //equifaxCRApplication1
        EquifaxIntegration.equifaxCRAccountAction(objContact.id);
        EquifaxIntegration objIntegration = new EquifaxIntegration();
        
        //objIntegration.equifaxCRApplication1(objApplication.id,true);
        genesis__Applications__c app = [Select Id,Name,genesis__Contact__r.Id,Beacon_9_Score__c ,BNI__c,
                                        genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                        Number_of_Payments__c ,genesis__Expected_Close_Date__c 
                                        from genesis__Applications__c
                                        where Id = :objApplication.id]; 
        objIntegration.equifaxCRApplication1(app.id,true);
        objIntegration.equifaxCRApplication(app,true);
        objIntegration.lendScoreCalculate(objApplication);
    }
    
    static testMethod void myUnitTest01() {
        Contact objContact = new Contact();
        objContact.LastName = 'TestContact';
        objContact.FirstName = 'TestData';
        objContact.MailingStreet='sonysignal,koramangala';
        objContact.Birthdate = date.today();
        insert objContact;
        
        genesis__Applications__c objApplication = new genesis__Applications__c();
        objApplication.genesis__Contact__c = objContact.id;
        insert objApplication;
        
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('ints__Contact__c',objContact.Id);
        fieldMap.put('application__c',objApplication.Id);
        TestHelper.createEquifaxCRRecords(fieldMap);
        
        Contact_Mapping_For_Equifax_Testing__c objContactMapping = new Contact_Mapping_For_Equifax_Testing__c();
        objContactMapping.Name =  'TestData';
        objContactMapping.Eid_Verification_Contact__c = objContact.id;
        insert objContactMapping;
        //equifaxCRApplication1
        EquifaxIntegration.equifaxCRAccountAction(objContact.id);
        EquifaxIntegration objIntegration = new EquifaxIntegration();
        
        //objIntegration.equifaxCRApplication1(objApplication.id,true);
        genesis__Applications__c app = [Select Id,Name,genesis__Contact__r.Id,Beacon_9_Score__c ,BNI__c,
                                        genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                        Number_of_Payments__c ,genesis__Expected_Close_Date__c 
                                        from genesis__Applications__c
                                        where Id = :objApplication.id]; 
        objIntegration.equifaxCRApplication1(app.id,false);
        objIntegration.equifaxCRApplication(app,false);
        objIntegration.lendScoreCalculate(objApplication);
    }
    
    static testMethod void myUnitTest1() {
        Contact objContact = new Contact();
        objContact.LastName = 'TestContact';
        objContact.FirstName = 'TestData';
        objContact.MailingStreet='sonysignal,koramangala';
        objContact.Birthdate = date.today();
        insert objContact;
        
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('ints__Contact__c',objContact.Id);
        TestHelper.createEquifaxCRRecords(fieldMap);
        
        EquifaxIntegration.equifaxCRAccountAction(objContact.id);
    }
    
    static testMethod void myUnitTest2() {
        Contact objContact = new Contact();
        objContact.LastName = 'TestContact';
        objContact.FirstName = 'TestData';
        objContact.MailingStreet='sonysignal,koramangala';
        objContact.Birthdate = date.today();
        insert objContact;
        
        Map<String,String> fieldMap = new Map<String,String>();
        fieldMap.put('ints__Contact__c',objContact.Id);
        TestHelper.createEquifaxCRRecords(fieldMap);
        
        EquifaxIntegration.equifaxCRAccountAction(objContact.LastName);
    }
    
    
}