public class LendifiedUtil {
    
    // Change Salesforce DateTime to .Net Round-trip date/time pattern.
    public static String formatDateCreditEngine(Datetime dt) {
        //Format https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx#Roundtrip -- 2016-08-02T17:25:57.000Z
        String roundtripformat = string.valueOf(dt.date());
        roundtripformat += 'T' ;
        roundtripformat += dt.hourGMT() < 10 ? '0' + string.valueOf(dt.hourGMT()) : string.valueOf(dt.hourGMT());
        roundtripformat += ':' ;
        roundtripformat += dt.minuteGMT() < 10 ? '0' + string.valueOf(dt.minuteGMT()) : string.valueOf(dt.minuteGMT());
        roundtripformat += ':' ;
        roundtripformat += dt.secondGMT() < 10 ? '0' + string.valueOf(dt.secondGMT()) : string.valueOf(dt.secondGMT()); 
        roundtripformat += '.0000000Z';
        return roundtripformat;
    }
    
    // Change Date format to Datetime format
    public static Datetime convertDateFormat(Date d) {
        Datetime dt = datetime.newInstanceGMT(d.year(), d.month(),d.day());
        return dt;
    }
    
    // Send Salesforce Excpetion message
    public static void sendSalesforceExceptionEmail(String subject, Exception error)  {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setPlainTextBody('At line # : '+ error.getLineNumber() + '\n' +
                                'Type : ' + error.getTypeName() + '\n' +
                                'Exception Message : ' + error.getMessage());
        sendEmail(mail, subject);
   }

    // Send Credit Engine Exception message
    public static void sendCreditEngineExceptionEmail(String subject, HttpResponse errorMessage)  {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setPlainTextBody('Error Response code : '+ errorMessage.getStatusCode() + '\n' +
                                'Error Status : ' + errorMessage.getStatus());
        sendEmail(mail, subject);
   }

    // Salesforce Email handler
    public static void sendEmail(Messaging.SingleEmailMessage email, String subject) {
        String[] toAddresses = new String[]{}; 
        for(User UserRecord:[select Id, Email, Profile.Name from User where Profile.Name LIKE 'System Admin -%']) {
            toAddresses.add(UserRecord.Email);
        }
        email.setToAddresses(toAddresses);  
        email.setSubject('Exception occured: '+subject); 
        if(!Test.isRunningTest()) Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });  
    }
}