@isTest
private class TestScoringController {
    
    @isTest static void testScoringController() {
        // Implement test code
        genesis__applications__c application = genesis.TestHelper.createLoanApplication();
        application.Beacon_9_Score__c        = 600 ;
        application.BNI__c                   = 600 ;
        update application;
        
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        
        Loss_Rate_Decile_Comparator__c lrdc = new Loss_Rate_Decile_Comparator__c();
        lrdc.Max_Range__c=1000;
        lrdc.Min_Range__c=100;
        lrdc.Loss_Rate__c=10;
        lrdc.Decile__c=10;
        lrdc.recordtypeId=beacon9RTID;
        insert lrdc;
        Loss_Rate_Decile_Comparator__c lrdc1 = new Loss_Rate_Decile_Comparator__c();
        lrdc1.Max_Range__c=1000;
        lrdc1.Min_Range__c=100;
        lrdc1.Loss_Rate__c=10;
        lrdc1.Decile__c=10;
        lrdc1.recordtypeId=bni3RTID;
        insert lrdc1;
        Lend_Score__c ls = new Lend_Score__c();
        ls.Decile__c=10;
        ls.Applied_Interest_Rate__c=10;
        ls.Lend_Score_Max_Value__c=1000;
        ls.Lend_Score_Min_Value__c=100;
        insert ls;
     
        ApexPages.StandardController con     = new ApexPages.StandardController(application);
        scoringController scoring          = new scoringController(con);
        scoring.submit();
        scoring.lendScoreCalculate(application);

    }
}