/*
Schedule the job manually:
AcctSeedOriginationFeeJournalEntries j = new AcctSeedOriginationFeeJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedOriginationFeeJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedOriginationFeeJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id from loan__Loan_Account__c where loan__Loan_Status__c = \'Active - Good Standing\' or loan__Loan_Status__c = \'Active - Bad Standing\'';
        
    public AcctSeedOriginationFeeJournalEntries(){
    }
    public AcctSeedOriginationFeeJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedOriginationFeeJournalEntries job = new AcctSeedOriginationFeeJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            AcctSeedUtilityClass.createOriginationFeeJournalEntries(scope);
            
        }catch(Exception e){
              Database.rollback(sp); 
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedOriginationFeeJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedFeeChargeJournalEntries job = new AcctSeedFeeChargeJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}