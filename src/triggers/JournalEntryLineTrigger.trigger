trigger JournalEntryLineTrigger on AcctSeed__Journal_Entry_Line__c (before insert, before update) {
	// get default setting
	loan_seed__Journal_Creation_Options__c journalOptions = loan_seed__Journal_Creation_Options__c.getInstance();
	
	// set default GL Account Variable 1
	// this sets default Company that we're tracking with Accounting Seed
    for (AcctSeed__Journal_Entry_Line__c jeLine : Trigger.new) {
    	if (jeLine.AcctSeed__GL_Account_Variable_1__c == null
    			&& journalOptions.Default_GL_Account_Variable_1_Id__c != ''
    			&& journalOptions.Default_GL_Account_Variable_1_Id__c != null) {
    		jeLine.AcctSeed__GL_Account_Variable_1__c = journalOptions.Default_GL_Account_Variable_1_Id__c;
    	}
    }
}