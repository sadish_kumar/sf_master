public with sharing class OfferController extends genesis.ApplicationWizardBaseClass{
    public genesis__Applications__c application{get;set;}
    
    public OfferController(ApexPages.StandardController controller) {
        super(controller);
        
        application = (genesis__Applications__c)controller.getRecord();
        
        application = [Select Id,Name,
                              Application_Rating__c,
                              Credit_Score__c,
                              genesis__Interest_Rate__c,
                              genesis__Loan_Amount__c,
                              genesis__Term__c,
                              Offer_s_Max_Loan_Amount__c
                              
                       From genesis__Applications__c
                       Where Id =: application.Id];
    }
    
    public void getScore(){
        
        List<Scoring__c> sc = [Select ID,name,Credit_Score_High_Value__c,Credit_Score_Low_Value__c,
                                    Interest_Rate__c,Term__c,Max_Loan_Amount__c
                                    
                                    from Scoring__c
                                    where Credit_Score_High_Value__c >= :application.Credit_Score__c
                                    and Credit_Score_Low_Value__c <= :application.Credit_Score__c];
        if(sc != null && sc.size() != 0){
            application.Offer_s_Max_Loan_Amount__c = sc[0].Max_Loan_Amount__c;
            application.genesis__Interest_Rate__c = sc[0].Interest_Rate__c;
            application.genesis__Term__c = sc[0].Term__c;
        }
        else{
            application.Offer_s_Max_Loan_Amount__c = null;
            application.genesis__Interest_Rate__c = null;
            application.genesis__Term__c = null;
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.ERROR,'No Scoring Record found for the provided Credit Score.');
            ApexPages.addMEssage(m);
        }
        
        update application;
        
        
    }
    
    public PageReference submit(){
        try{
            update application;
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.CONFIRM,'SUCCESS: Application updated successfully.');
            ApexPages.addMEssage(m);
        }catch(Exception e){
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.ERROR,e.getMessage()+ ' from line '+e.getLineNumber());
            ApexPages.addMEssage(m);
            return null;
        }
        return null;
    }
}