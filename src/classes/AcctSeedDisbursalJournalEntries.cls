/*
Schedule the job manually:
AcctSeedDisbursalJournalEntries j = new AcctSeedDisbursalJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedDisbursalJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedDisbursalJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id, loan__Loan_Account__c from loan__Loan_Disbursal_Transaction__c where loan__Cleared__c = true and loan__Reversed__c = false and loan_seed__Journal_Entry__c = null and loan_vp__Sent_To_Versapay__c = true';
        
    public AcctSeedDisbursalJournalEntries(){
    }
    public AcctSeedDisbursalJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedDisbursalJournalEntries job = new AcctSeedDisbursalJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            List<loan__loan_Account__c > loanAccountList = new List<loan__loan_Account__c >();
            for (loan__Loan_Disbursal_Transaction__c record : (List<loan__Loan_Disbursal_Transaction__c> ) scope) {
            	loanAccountList.add(new loan__loan_Account__c(Id=record.loan__Loan_Account__c));
            }
            AcctSeedUtilityClass.createDisbursalJournalEntries(loanAccountList);
        }catch(Exception e){
              Database.rollback(sp);      

              System.debug('Daily Interest Accrual failed: '+e.getCause()+' '+e.getMessage());
              System.debug(e.getStackTraceString());

              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedDisbursalJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedDisbursalReversalJournalEntries job = new AcctSeedDisbursalReversalJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}