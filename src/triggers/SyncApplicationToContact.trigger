/*
*	Copies application data to Contact record. Used to filter contacts
*	by application status and other fields in Pardot.
*
*	- By Dmitri Sennikov
*/
trigger SyncApplicationToContact on genesis__Applications__c (after insert, after update) {
	Set<Id> contactIds = new Set<Id>();
	List<Contact> contactsToUpdate = new List<Contact>();
	Contact tempContact;
    for (genesis__Applications__c app : Trigger.new) {
    	// run for application with associated contact and only for applications
    	// where certain fields we're interested in have been modified
    	if (app.genesis__Contact__c != null
    			&& (Trigger.isInsert
    				|| (Trigger.isUpdate && app.genesis__Status__c != Trigger.oldMap.get(app.Id).genesis__Status__c))) {
    		if (!contactIds.contains(app.genesis__Contact__c)) {
				contactIds.add(app.genesis__Contact__c);
				tempContact = new Contact();
				tempContact.Id = app.genesis__Contact__c;
				tempContact.Active_Application__c = app.Id;
				tempContact.Application_Status__c = app.genesis__Status__c;
				contactsToUpdate.add(tempContact);
    		}
		}
    }
    if (contactsToUpdate.size() > 0) {
    	update contactsToUpdate;
    }
}