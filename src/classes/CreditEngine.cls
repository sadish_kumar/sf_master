public with sharing class CreditEngine {

    // Get Credit Engine settings
    public static Integrations__c settings = Integrations__c.getInstance('CREDIT_ENGINE');

    public CreditEngine() {
    }

    @future(callout=true)
    public static void postLoanApplciation(String applicationId) {
        try {
            genesis__Applications__c app = [Select Id,Name,genesis__Contact__r.Id,genesis__Contact__r.firstname,
                                                genesis__Contact__r.lastname,genesis__Contact__r.mobilephone,
                                                genesis__Contact__r.birthdate,genesis__Contact__r.email,
                                                genesis__Contact__r.mailingstreet,genesis__Loan_Amount__c,genesis__Term__c,
                                                genesis__Contact__r.mailingcity,genesis__Contact__r.mailingpostalcode,
                                                genesis__Contact__r.mailingstate,genesis__Contact__r.mailingcountry, com_loan__Loan_Purpose__c,
                                                com_loan__Business_Name__c,com_loan__Business_Number__c,
                                                com_loan__Street_Address__c,com_loan__City__c,com_loan__State__c,
                                                com_loan__Postal__c,com_loan__Industry__c,Beacon_9_Score__c ,BNI__c,
                                                genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                                Number_of_Payments__c ,genesis__Expected_Close_Date__c,
                                                genesis__Applications__c.com_loan__Trade_Name_of_Business__c
                                            from genesis__Applications__c
                                            where Id = :applicationId];
            
            // Get the JSON string.
            String body = JSONClass.generateJSON(app).getAsString();
            System.debug('-body-'+body);

            // Instantiate a new http object
            Http h = new Http();

            // Instantiate a new HTTP request
            HttpRequest req = new HttpRequest();
            req.setEndpoint(settings.Endpoint_URL__c);
            req.setTimeout(Integer.valueOf(settings.Time_Out__c));
            req.setMethod(settings.Method__c);
            req.setBody(body);
            req.setHeader('Content-Type',settings.Content_Type__c);
            req.setHeader('Ocp-Apim-Subscription-Key',settings.Secret_Key__c);

            // Send the request, and return a response
            HttpResponse res = !Test.isRunningTest() ? h.send(req) : createTestResponse();
            
            if(res.getStatus().tolowercase() == 'Created' && res.getStatusCode() == 201) {
                System.debug('-IF Block-'+res.getStatusCode()+'&'+res.getStatus());
                System.debug('-- JSON Response --'+res.getBody());
                parseJSONResult(app, res.getBody());                
            }
            else {
                System.debug('-Else Block-'+res.getStatusCode()+'&'+res.getStatus());
                LendifiedUtil.sendCreditEngineExceptionEmail('Credit Engine API failure',res);
            }
        }
        catch(Exception e) {
            System.debug('-Catch Block - Exception-'+e);
            LendifiedUtil.sendSalesforceExceptionEmail('Credit Engine Apex class failure',e);
        }
    }
    
    public static void parseJSONResult(genesis__Applications__c app, string responsemessage) {

        List<genesis__Applications__c> updateApplication = [SELECT BNI_CE__c, Beacon_9_Score_CE__c, 
                                                                    Credit_Rating__c,Interest_Rate_CE__c,
                                                                    Loan_Adjudication_Status__c,Transunion_Score__c,Loan_Adjudication_Error_Message__c
                                                                    FROM  genesis__Applications__c 
                                                            WHERE Id =: app.id];
        //res is a JSON string
        JSONParser parser = JSON.createParser(responsemessage);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                String fieldName = parser.getText();
                parser.nextToken();
                if(fieldName == 'bniScore' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].BNI_CE__c = Decimal.valueOf(parser.getText());
                } 
                else if(fieldName == 'beaconScore' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Beacon_9_Score_CE__c = Decimal.valueOf(parser.getText());
                }
                else if(fieldName == 'creditRating' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Credit_Rating__c = parser.getText();
                }
                else if(fieldName == 'interestRate' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Interest_Rate_CE__c = Decimal.valueOf(parser.getText());
                }
                else if(fieldName == 'loanAdjudicationStatus' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Loan_Adjudication_Status__c = parser.getText();
                }
                else if(fieldName == 'transUnionScore' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Transunion_Score__c = Decimal.valueOf(parser.getText());
                }
                else if(fieldName == 'loanAdjudicationErrorMessage' && parser.getText().toLowerCase() != 'null') {
                    updateApplication[0].Loan_Adjudication_Error_Message__c = parser.getText();
                }
            }
        }
        update updateApplication;
    }

    private static HttpResponse createTestResponse(){
        HttpResponse r = new HttpResponse();
        r.setStatusCode(200);
        r.setStatus('Created');
        return r;
    }
}