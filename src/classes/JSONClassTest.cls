@isTest
private class JSONClassTest {
	
    @isTest
    static void testMethod_PostivieTest() {
        
        // Create Application reord
        genesis__applications__c app = TestDataFactory.createApplicationTestRecord();

        // Pass the Application record to JSONClass apex class
        String body = JSONClass.generateJSON(app).getAsString();
        system.assert(body != '');
    }
}