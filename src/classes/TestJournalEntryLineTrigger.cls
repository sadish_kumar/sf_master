@isTest
public with sharing class TestJournalEntryLineTrigger {

    static testmethod void testJournalEntryLineTrigger() {  
    	AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();
    	glAccount.Name = 'Test GL Account';
    	glAccount.AcctSeed__Type__c = 'Balance Sheet';
    	glAccount.AcctSeed__Sub_Type_1__c = 'Assets';
    	insert glAccount;
    	
    	Date todayDate = Date.today();
    	AcctSeed__Accounting_Period__c accountingPeriod = new AcctSeed__Accounting_Period__c();
    	accountingPeriod.Name = todayDate.year()+'-'+('0'+todayDate.month()).right(2);
    	accountingPeriod.AcctSeed__Start_Date__c = Date.today();
    	accountingPeriod.AcctSeed__End_Date__c = Date.today();
    	accountingPeriod.AcctSeed__Status__c = 'Open';
    	insert accountingPeriod;
    	
    	AcctSeed__Journal_Entry__c journalEntry = new AcctSeed__Journal_Entry__c();
    	journalEntry.Name = 'Test Journal Entry';
    	journalEntry.AcctSeed__Journal_Date__c = Date.today();
    	insert journalEntry;
    	
    	AcctSeed__Journal_Entry_Line__c journalEntryLine = new AcctSeed__Journal_Entry_Line__c();
    	journalEntryLine.AcctSeed__Journal_Entry__c = journalEntry.Id;
    	journalEntryLine.AcctSeed__GL_Account__c = glAccount.Id;
    	journalEntryLine.AcctSeed__Credit__c = 1.00;
    	insert journalEntryLine;
    }  
}