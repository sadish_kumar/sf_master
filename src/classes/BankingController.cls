public with sharing class BankingController extends genesis.ApplicationWizardBaseClass{
    public genesis__Applications__c application{get;set;}
    public loan__Bank_Account__c bank {get;set;}
    
    public BankingController(ApexPages.StandardController controller) {
        super(controller);
        
        application = (genesis__Applications__c)controller.getRecord();
        
        application = [Select Id,Name,
                              Bank_Account_Verified__c,
                              genesis__Account__r.Id,
                              genesis__Account__c
                              
                       From genesis__Applications__c
                       Where Id =: application.Id];
    
        if(application.genesis__Account__r.Id != null){
            bank = [Select Id,name,loan__Bank_Name__c,
                            loan__Bank_Account_Number__c,
                            loan__Routing_Number__c,
                            loan__Account_Type__c
                            
                            from loan__Bank_Account__c
                            where loan__Account__c = : application.genesis__Account__r.Id];
                            
        }
        
    }
    
    public PageReference submit(){
        try{
            update application;
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.CONFIRM,'SUCCESS: Application updated successfully.');
            ApexPages.addMEssage(m);
        }catch(Exception e){
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.ERROR,e.getMessage()+ ' from line '+e.getLineNumber());
            ApexPages.addMEssage(m);
            return null;
        }
        return null;
    }
}