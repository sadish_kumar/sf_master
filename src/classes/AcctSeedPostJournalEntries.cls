/*
Schedule the job manually:
AcctSeedPostJournalEntries job = new AcctSeedPostJournalEntries();
Database.executeBatch(job,100);
*/
global class AcctSeedPostJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedPostJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id from AcctSeed__Journal_Entry__c where AcctSeed__Status__c = \'Approved\'';
        
    public AcctSeedPostJournalEntries(){
    }
    public AcctSeedPostJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedPostJournalEntries job = new AcctSeedPostJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            List<AcctSeed__Journal_Entry__c > journalEntryList= (List<AcctSeed__Journal_Entry__c > ) scope;
            AcctSeed.JournalEntryPostService.postJournalEntries(journalEntryList);
        }catch(Exception e){
              Database.rollback(sp);
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedPostJournalEntries failed: '+e.getCause()+' '+e.getMessage();
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		// this is the last job
    	}
    }
}