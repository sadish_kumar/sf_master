/*
Schedule the job manually:
AcctSeedFeeChargeJournalEntries j = new AcctSeedFeeChargeJournalEntries();
database.executeBatch(j);
*/

global class AcctSeedFeeChargeJournalEntries implements Schedulable,Database.stateful,Database.Batchable<sObject>,Database.AllowsCallouts{

    private static final String JOB_NAME = 'AcctSeedFeeChargeJournalEntries';
    private boolean submitNextJob = false;
    private static final String query = 'select Id,loan__Loan_Account__c from loan__Charge__c where loan__Waive__c = false and Journal_Entry__c = null';
        
    public AcctSeedFeeChargeJournalEntries(){
    }
    public AcctSeedFeeChargeJournalEntries(Boolean submitNextJob){
    	this.submitNextJob = submitNextJob;
    }
    
    public void execute(SchedulableContext sc){
        AcctSeedFeeChargeJournalEntries job = new AcctSeedFeeChargeJournalEntries();
        Database.executeBatch(job,100);
    }
    
     global Database.Querylocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            List<loan__loan_Account__c > loanAccountList = new List<loan__loan_Account__c >();
            for (loan__Charge__c record : (List<loan__Charge__c> ) scope) {
            	loanAccountList.add(new loan__loan_Account__c(Id=record.loan__Loan_Account__c));
            }
            AcctSeedUtilityClass.createFeeChargeJournalEntries(loanAccountList);
            
        }catch(Exception e){
              Database.rollback(sp); 
              
              loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
              log.loan__Message__c = 'AcctSeedFeeChargeJournalEntries failed: '+e.getMessage();
              if (e.getCause() != null) {
              	log.loan__Message__c += '\n\nCause: '+e.getCause();
              }
              if (e.getStackTraceString() != null) {
              	log.loan__Message__c += '\n\n'+e.getStackTraceString();
              }
              log.loan__Date__c = System.today();
                    
              insert log;  
       }
    }
    
    global void finish(Database.BatchableContext bc){
    	if (submitNextJob) {
    		AcctSeedWaivedFeeChargeJournalEntries job = new AcctSeedWaivedFeeChargeJournalEntries(submitNextJob);
        	Database.executeBatch(job,100);
    	}
    }
}