Public Class VaultCircleConstants{

    public static final string DDP_TEMPLATE = 'Loan Agreement';
    public static final string DDP_TEMPLATE_AB = 'Loan Agreement AB';
    public static final string DDP_TEMPLATE_QC = 'Loan Agreement QC';
    
    //application status...
    public Static final String EID_VERIFIED = 'eID Verified';
    public Static final String EID_FAILED = 'eID Failed'; 
}