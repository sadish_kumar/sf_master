public with sharing class scoringController extends genesis.ApplicationWizardBaseClass{
    public genesis__Applications__c application{get;set;}
    
    public scoringController(ApexPages.StandardController controller) {
        super(controller);
        
        application = (genesis__Applications__c)controller.getRecord();
        
        /*application = [Select Id,Name,
                              genesis__Interest_Rate__c,
                              Beacon_9_Decile__c,
                              Beacon_9_Loss_Rate__c,
                              BNI_Decile__c,
                              BNI_Loss_Rate__c,
                              Lend_Score__c,
                              Lend_Score_Decile__c,
                              genesis__Loan_Amount__c,
                              genesis__Term__c,
                              genesis__Payment_Amount__c
                              
                       From genesis__Applications__c
                       Where Id =: application.Id]; */
                       
        application = com_loan.CommunityUtil.getApplicationFromId(application.Id);
    }
    
    public PageReference submit(){
        try{
              genesis__applications__c application = com_loan.CommunityUtil.getApplicationFromId(application.Id);
            // doing for successful offer generation 
            LendScore scoreApi = new LendScore(application);
            String result = scoreApi.lendScoreCalculate();
            
            if(result != null && result.equalsIgnoreCase('SUCCESS.')){
            update application;
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.CONFIRM,'SUCCESS: Application updated successfully.');
            ApexPages.addMEssage(m);
            }else{
                 ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.ERROR,result);
            ApexPages.addMEssage(m);
            }
           
        }catch(Exception e){
            ApexPages.Message m = new ApexPages.Message(ApexPAges.SEVERITY.ERROR,e.getMessage()+ ' from line '+e.getLineNumber());
            ApexPages.addMEssage(m);
            return null;
        }
        return null;
    }
    
    public string lendScoreCalculate(genesis__Applications__c applicationObj){
     
        //Error Messages String
        String errMessageBeacon9NotinRange      = 'Beacon 9 score is not in Range of Equifax Sheet.';
        String errMessageBNI3NotinRange         = 'BNI 3.0 score is not in Range of Equifax Sheet.';
        String errMessageLendingScoreNotinRange = 'Lending Score is not in Range of Equifax Sheet.';
        String errMessageBeacon9Decile1         = 'Beacon 9 Decile is 1.';
        String errMessageBNI3Decile1            = 'BNI 3.0 Decile is 1.';
        String errMessageBeaconBNIMandt         = 'BNI 3.0 score and Beacon 9 are mandatory for calculation'; 
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        
        // For getting the List of Loss Rate Decile Comparator and differentiating the Beacon 9 and BNI 3 
        // based on Record Type.
        List<Loss_Rate_Decile_Comparator__c> lossRateList1 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                             RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                             where Max_Range__c <= :applicationObj.Beacon_9_Score__c and Min_Range__c >= :applicationObj.Beacon_9_Score__c
                                                             and RecordTypeId = :beacon9RTID];
        
        
        if(lossRateList1 == null || lossRateList1.size() == 0){
            return 'Error : ' + errMessageBeacon9NotinRange;
        }
        else{
            application.Beacon_9_Score__c = lossRateList1[0].Loss_Rate__c;
            application.Beacon_9_Decile__c = lossRateList1[0].Decile__c;
        }
        List<Loss_Rate_Decile_Comparator__c> lossRateList2 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                             RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                             where Max_Range__c <= :applicationObj.BNI__c and Min_Range__c >= :applicationObj.BNI__c
                                                             and RecordTypeId = :bni3RTID];
        
        
        if(lossRateList2 == null || lossRateList2.size() == 0){
            return 'Error : ' + errMessageBNI3NotinRange;
        }else{
            application.BNI_Loss_Rate__c = lossRateList2[0].Loss_Rate__c;
            application.BNI_Decile__c = lossRateList2[0].Decile__c;
        }
        
        
        if(applicationObj.Beacon_9_Decile__c == 1){
            return 'Error : ' + errMessageBeacon9Decile1;
        }else if(applicationObj.BNI_Decile__c == 1){
            return 'Error : ' + errMessageBNI3Decile1;
        }
        if(applicationObj.Beacon_9_Loss_Rate__c >= applicationObj.BNI_Loss_Rate__c){
            applicationObj.Lend_Score__c = applicationObj.Beacon_9_Loss_Rate__c;
        }else{ //if(applicationObj.Beacon_9_Loss_Rate__c < applicationObj.BNI_Loss_Rate__c){
            applicationObj.Lend_Score__c = (applicationObj.Beacon_9_Loss_Rate__c * 0.25) + (applicationObj.BNI_Loss_Rate__c * 0.75);
        }
        
        List<Lend_Score__c> lendScoreList = [Select Id, Applied_Interest_Rate__c, Decile__c, Lend_Score_Max_Value__c, Lend_Score_Min_Value__c 
                         from Lend_Score__c where Lend_Score_Max_Value__c <= :applicationObj.Lend_Score__c
                         and Lend_Score_Min_Value__c >= :applicationObj.Lend_Score__c];    
        
        if(lendScoreList == null || lendScorelist.size() == 0){
            return 'Error : ' + errMessageLendingScoreNotinRange;
        }else{
            applicationObj.Lend_Score_Decile__c           = lendScoreList[0].Decile__c;
            applicationObj.Bankruptcy_Navigation_Index__c = lendScoreList[0].Applied_Interest_Rate__c;
        }
    
        return 'Success.';
    }
    
}