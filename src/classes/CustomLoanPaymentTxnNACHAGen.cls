global class CustomLoanPaymentTxnNACHAGen {
    //extends loan.FileGenerator{
    /* Commenting because we are using CustomLoanPaymentTxnNACHAGenV1 class now...
    
    loan__ACH_Parameters__c achParams = loan.CustomSettingsUtil.getACHParameters();
    public Long amountTotal = 0;
    public integer detailCount = 0;
    Public integer batchCount = 0;
    @testvisible private List<Loan__loan_Payment_Transaction__c> requeryScope (List<SObject> scope){
        Set<ID> ids = new Set<Id>();
        for(SObject s : scope){
            ids.add(s.Id);
        }
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        mfiflexUtil.ObjectCache pmtOC = ec.createObject('LoanPaymentsForACH',
                                                    'Loan__Loan_Payment_Transaction__c');
        String s = 'loan__Payment_Mode__c,Loan__Loan_Account__c,Loan__Transaction_Amount__c,Loan__Transaction_Date__c';
        pmtOC.addFields(s);
        pmtOC.addFields('loan__Loan_Account__r.loan__ACH_On__c,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c,'+
                        'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.Branch_Transit_Number__c,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Account__r.Name,'+
                       'loan__Loan_Account__r.loan__Account__r.name,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.Branch_Transit_Number__c,'+ 
                       'loan__Loan_Account__r.loan__Account__r.peer__First_Name__c,'+
                       'loan__Loan_Account__r.loan__Account__r.peer__Last_Name__c,'+
                       'loan__Loan_Account__r.loan__Account__r.Name,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Account__c,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Bank_Account_Number__c,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account__r.Name,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account__c');
       pmtOC.addNamedParameter('ids',ids);
       pmtOC.setWhereClause('Id IN:ids');
       pmtOC.executeQuery();
       return(List<loan__loan_Payment_Transaction__c>)pmtOC.getRecords();
    }

    public override String getSimpleFileName() {
            return  'Loan_Payments';
    }
    
   // private Boolean invalid = false;
        
    public override List<String> getEntries(loan.TransactionSweepToACHState state, List<SObject> scope) {
        // need to requery scope!
        List<loan__Loan_Payment_Transaction__c> pmts = requeryScope(scope);
        List<String> retVal = new List<String>();
        List<SObject> objects = new List<SObject>();
        integer pmtCount = 0;
        Decimal batchDetailAmount = 0;
        integer batchDetailCount = 0;
        
        for (loan__Loan_Payment_Transaction__c p : pmts) {
             String name  = p.loan__Loan_Account__r.loan__Account__r.peer__Last_Name__c + ', ' + p.loan__Loan_Account__r.loan__Account__r.peer__First_Name__c;
             System.debug('name----'+name);
            
            if(pmtCount == 0 || math.mod(pmtCount,5) == 0){
                Batch_Header__c batchHeader = new Batch_Header__c();
                batchHeader.Transaction_Code__c = '351';// achPArams.Payments_Batch_Header_Transaction_Code__c; 
                batchHeader.Value_Date__c =  (Date) new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
                objects.add(batchHeader);
                batchDetailAmount  = 0 ;
                batchDetailCount = 0; 
                batchCount++;
            }
             if(p.loan__Loan_Account__r.loan__ACH_On__c == true){
                if(p.loan__Loan_Account__r.loan__Borrower_ACH__r!=null){
                    Detail__c detail = new Detail__c();
                    detail.Crdeit_Debit_Indentifier__c= 'D';
                    detail.Institution_ID__c = achParams.Institution_ID__c;
                    detail.Branch_Transit_Number__c = p.loan__Loan_Account__r.loan__Borrower_ACH__r.Branch_Transit_Number__c ; 
                    detail.Payee_Payor_Account_Number__c = p.loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Bank_Account_Number__c;
                    detail.Amount__c = String.valueOf(p.loan__Transaction_Amount__c.setScale(2));
                    amountTotal+=(Long)Double.valueOf(detail.Amount__c);
                    detail.Cross_Reference_Number__c = p.name;
                    detail.Payee_Payor_Name__c = p.loan__Loan_Account__r.loan__Account__r.Name;//name;
                    system.debug('Detail:'+detail);
                    objects.add(detail);
                    batchDetailAmount = batchDetailAmount + p.loan__Transaction_Amount__c.setScale(2);
                    addToValidScope(p);
                    detailCount++;
                    pmtCount++;
                    batchDetailCount++;
                }else{
                    addToInvalidScope(p,'bank details not present for transaction');
                }
            }else{
                if(p.loan__Loan_Account__r.loan__OT_Borrower_ACH__c!=null){
                    Detail__c detail = new Detail__c();
                    detail.Crdeit_Debit_Indentifier__c= 'D';
                    detail.Institution_ID__c = achParams.Institution_ID__c;
                    detail.Branch_Transit_Number__c = p.loan__Loan_Account__r.loan__OT_Borrower_ACH__r.Branch_Transit_Number__c ;
                    detail.Payee_Payor_Account_Number__c = p.loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c; 
                    detail.Amount__c = String.valueOf(p.loan__Transaction_Amount__c.setScale(2));
                    amountTotal+=(Long)Double.valueOf(detail.Amount__c);
                    detail.Cross_Reference_Number__c = p.name;
                    detail.Payee_Payor_Name__c =name;
                    system.debug('Detail:'+detail);
                    objects.add(detail);
                    batchDetailAmount = batchDetailAmount +p.loan__Transaction_Amount__c.setScale(2);
                    addToValidScope(p);
                    detailCount++;
                    pmtCount++;
                    batchDetailCount++;
                }else{
                    addToInvalidScope(p,'bank details not present for transaction');
                }
            }
            If(pmtCount == pmts.size()|| math.mod(pmtCount,5) == 0 ){
                    //batchtrailer
                    Batch_Trailer__c batchTrailer = new Batch_Trailer__c();
                    batchTrailer.Transaction_Code__c = '351';
                    batchTrailer.Batch_Entry_Count__c = String.valueof(batchDetailCount);
                    batchTrailer.Reserved__c = '0';
                    batchTrailer.Entry_Dollar_Amount__c = String.valueof(batchDetailAmount); 
                    objects.add(batchTrailer);
            }
            
        }
        filegen.CreateSegments segments =new filegen.CreateSegments(objects);
        retVal = segments.retString();
        for(String line:retVal){
            line = line+'\r\n';
            addEntry(line);
        }
        return retVal;
    }
    
    public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
       List<String> retVal = new List<String>();
       List<Sobject> objects = new List<Sobject>();
       List<Sobject> headerRecords = new List<Sobject>();
       String header='';
       objects = getValidScope();
       if(objects.size()!=0){
           File_Header__c fileHeader = new File_Header__c();
           fileHeader.Origination_Number__c = achParams.Originator_Number__c;
           fileHeader.Destination_Data_Center__c = achParams.Originator_Number__c.substring(0,5);
           fileHeader.File_Creation_Date__c = Date.Today(); //(Date) new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
           fileHeader.File_Creation_Number__c ='0001';// String.valueof(state.counter) ;
           fileHeader.Institution_Number__c = achParams.Institution_ID__c;
           fileHeader.Branch_Transit_Number__c = achParams.Branch_Transit_Number__c;
           fileHeader.Account_Number__c = achParams.CIBC_settlement_account_number__c;
           fileHeader.Originators_Short_Name__c = achParams.Originator_s_Short_Name__c;
           fileHeader.Currency_Indicator__c = 'CAD';
           headerRecords.add(fileHeader);
           //Batch_Header__c batchHeader = new Batch_Header__c();
           //batchHeader.Transaction_Code__c = '351';// achPArams.Payments_Batch_Header_Transaction_Code__c; 
           //batchHeader.Sundry_Information__c = 
           //batchHeader.Value_Date__c =  (Date) new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
           //headerRecords.add(batchHeader);
           filegen.CreateSegments segments = new filegen.CreateSegments(headerRecords);
           retval = segments.retString();
           header =retVal[0]+'\r\n';
           //header+=retVal[1] + '\r\n';
           return header;
       }else{
           return 'header'; 
       }
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
        List<String> retVal = new List<String>();
        List<Sobject> objects = new List<Sobject>();
        List<Sobject> trailerRecords = new List<Sobject>();
        String trailer ='';
        Decimal sumOfAmount = 0;
        objects = getValidScope();
        if(objects.size()!=0){
            File_Trailer__c fileTrailer = new File_Trailer__c();
            for(SObject obj:getValidScope()){  
                sumOfAmount+=(Decimal)obj.get('loan__Transaction_Amount__c');
            }
            fileTrailer.Detail_Count__c = String.valueOf(detailCount);
            fileTrailer.Batch_Count__c = String.valueof(batchCount);
            //Batch_Trailer__c batchTrailer = new Batch_Trailer__c();
            //batchTrailer.Transaction_Code__c = '351';
            //batchTrailer.Batch_Entry_Count__c = '1';
            //batchTrailer.Reserved__c = '0';
            //batchTrailer.Entry_Dollar_Amount__c = String.valueOf(sumOfAmount.setScale(2));
            
            //trailerRecords.add(batchTrailer);
            trailerRecords.add(fileTrailer);
            filegen.CreateSegments segments = new filegen.CreateSegments(trailerRecords);
            retval = segments.retString();
            trailer =retVal[0]+'\r\n';
            //trailer+=retVal[1]+'\r\n';
            return trailer;
       }else{
           return 'trailer'; 
       }
    }
    public static Integer getJulianDate(integer I,integer J,integer K){   
        
        return ( K-32075+1461*(I+4800+(J-14)/12)/4+367*(J-2-(J-14)/12*12) /12-3*((I+4900+(J-14)/12)/100)/4 );
    }
    */
    
}