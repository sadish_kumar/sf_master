@isTest
private class CreditEngineTest {

    @isTest(SeeAllData=true)
    static void testMethod_PostLoanApplicationSuccess() {
        // Create Application reord
        genesis__applications__c app = TestDataFactory.createApplicationTestRecord(); 

        // Pass the Application record to JSONClass apex class
        CreditEngine.postLoanApplciation(app.id);
    } 

    @isTest(SeeAllData=true)
    static void testMethod_PostLoanApplicationFailure() {
        // Create Application reord
        String applicationId = 'failureCase';

        // Pass the Application record to JSONClass apex class
        CreditEngine.postLoanApplciation(applicationId);
    }

    @isTest(SeeAllData=true)
    static void testMethod_parseJSONResult() {
        String JSONResponseString = '{'+ '"loanAdjudication": {' +
                                        ' "loanAmount": 0.0,' +
                                        ' "creditRating": null,' +
                                        ' "interestRate": 0.1499,' +
                                        ' "maxApprovedLoanAmount": 0.0,' +
                                        ' "paymentAmount": 0.0,' +
                                        ' "processingBaseAmount": 0.0,' +
                                        ' "processingFee": 0.0, ' +
                                        ' "bniScore": 123.0, ' +
                                        ' "beaconScore": 456.0, ' +
                                        ' "transUnionScore": 0.0, ' +
                                        ' "loanAdjudicationStatus": null' + '}'+
                                    '}';

        // Create Application reord
        genesis__applications__c app = TestDataFactory.createApplicationTestRecord(); 

        // Pass the Application record to JSONClass apex class
        CreditEngine.parseJSONResult(app, JSONResponseString );
    }    
}