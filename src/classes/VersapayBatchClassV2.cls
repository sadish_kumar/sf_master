global class VersapayBatchClassV2 implements Database.Batchable<sObject>,database.allowsCallouts{
    
    String query;
    String appStatus = 'Offer Accepted';
    List<loan__Batch_Process_Log__c> batchLogList = new List<loan__Batch_Process_Log__c>();
    
    
    global VersapayBatchClassV2() {
        
        // ddpTemplate           = new DDP_Template__c();
        // ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
        //  this.sessionId             = sessionId ;
        
        query = 'Select Id,genesis__Status__c,Versapay_Token__c,'+
        'Versapay_Token_Status__c,genesis__Account__c,genesis__Contact__c,genesis__Contact__r.name,genesis__Contact__r.email,'+
        'genesis__Contact__r.MailingState from genesis__Applications__c'+
            ' where genesis__Status__c =: appStatus AND Contract_Document_Sent__c = false'+ 
            ' AND Versapay_Token__c != null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<genesis__Applications__c> scope) {
        
        try{       
            List<genesis__Applications__c> appList= (List<genesis__Applications__c> ) scope;
            VersapayBatchClassHandlerV2 handler = new VersapayBatchClassHandlerV2(appList);
            String sessionId;
            if(!Test.isRunningTest()){
                sessionId=Login.login('',''); // now passgin blank values because username and password is hardcoded in login class
                // system.debug('***'+sessionId);
            }
            else{
                sessionId='test';
            }
            //*/
            if(sessionId!=null)
                handler.checkVersapayTokenStatus(sessionId);
            else{           
                loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
                log.loan__Message__c = 'Session id is not generated';
                log.loan__Date__c = System.today();                    
                //insert log;
                batchLogList.add(log); 
            }
        }catch(Exception e){
            //Database.rollback(sp);
            System.debug('Versapay tokecn check and document send failed: '+e.getCause()+' '+e.getMessage());
            System.debug(e.getStackTraceString());              
            loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
            log.loan__Message__c = 'Versapay tokecn check and document send failed: '+e.getCause()+' '+e.getMessage();
            log.loan__Date__c = System.today();                    
            //insert log;
            batchLogList.add(log);  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        try{
            if(batchLogList != null && batchLogList.size() > 0)
                insert batchLogList;
        }catch(Exception e){}
        //Id batchJobId = Database.executeBatch(new DocusignBatchClass(), 1);
    }
    
}