global class ApplicationToContractConverter implements ints.IProcessContract{
    public genesis__Applications__c application = new genesis__Applications__c();
    public loan__Loan_Account__c loanAccount = new loan__Loan_Account__c();
    private Map<String,String> paymentFrequencyMap = new Map<String,String>();
    public genesis__Application_Parties__c genesisParties = new genesis__Application_Parties__c();
    //public LP_Custom__c c;
    global virtual void setContracts(Map<SObject,SObject> objectMap){
        Set<SObject> applicationSet = objectMap.keySet();
        for(SObject obj:applicationSet){
            application = (genesis__Applications__c)obj;
        }
        this.loanAccount = (loan__Loan_Account__c)objectMap.get(application);
        //lpCustom = CustomSettings.getLPCustom();
        //paymentFrequencyMap.put('MONTHLY','Monthly');
        //paymentFrequencyMap.put('QUARTERLY','Quarterly');
        //paymentFrequencyMap.put('ANNUAL','Annual');
    }
    global virtual String processContract(){
        try{
            String retMsg = '';
            //genesis__Applications__c app = [select genesis__Product_Type__c,genesis__Lending_Product__r.Name from genesis__Applications__c where Id=:application.Id limit 1];
            loan__Office_Name__c office;
            loan__Loan_Product__c product;
            application = getApplicationDetails(application.id);
            Lendified_Parameters__c lp = Lendified_Parameters__c.getOrgDefaults();
            
            String productType = application.genesis__Product_Type__c;
            String checker = appchecker();    
            if(!checker.Equals('1'))
                return checker;
                
            if(application.Company__c != null){
                office = getOffice(application.Company__r.Id); 
                System.debug('Company: '+office); 
            }else if(lp.Default_Company_Id__c != null){
                office = getOffice(lp.Default_Company_Id__c);
            }else
                return 'Provide Company information in Application or Custom Settings.';
                
            system.debug('*****application.Lending_Product__c'+application.Lending_Product__c);
            
            if(application.Lending_Product__c != null){
                product = getLoanProductDetails(application.Lending_Product__r.Id );
            }else if(lp.Default_Lending_Product_Id__c != null){
                product = getLoanProductDetails(lp.Default_Lending_Product_Id__c); 
            }else
                return 'Provide Lending Product information in Application or Custom Settings.';
            
            
            application.Company__c = office.Id;
            application.Lending_Product__c = product.Id;
            update application;
            
            if(productType.equals('LOAN')){
                 system.debug('*****product'+product);
                 system.debug('*****application'+application);
                CreateLoanAccountForLoanTypeHandler loanHandler = new CreateLoanAccountForLoanTypeHandler(application,loanAccount,product);
                retMsg = loanHandler.createLoanAccount();
            }
            
            /*if(productType.equals('LINE OF CREDIT')){
                CreateLoanAccountForLOCHandler locHandler = new CreateLoanAccountForLOCHandler(app,loanAccount,product);
                retMsg = locHandler.createLoanAccount();
            }
            */
            
            return retMsg;
        }catch(Exception e){
            return 'message:'+e.getMessage() +',line number:'+e.getLineNumber(); 
        }
            
    }
    
    public static genesis__Applications__c getApplicationDetails(String appId){
        genesis__Applications__c application = [select id,RecordTypeId,CreatedDate,
                                                        genesis__Loan_Amount__c,
                                                        genesis__Status__c,
                                                        Lending_Product__c,
                                                        genesis__Account__c,
                                                        Company__c,
                                                        Lending_Account__c,
                                                        Lending_Product__r.Name,
                                                        Lending_Product__r.Id,
                                                        Company__r.Name,
                                                        Company__r.Id,
                                                        //payment_mode__r.Name,
                                                        genesis__Contact__c,
                                                        genesis__Account__r.Id,
                                                        genesis__Contact__r.Id,
                                                        genesis__Payment_Amount__c,
                                                        genesis__Term__c,genesis__Credit_Limit__c,
                                                        genesis__Payment_Frequency__c,genesis__Draw_Term__c,
                                                        genesis__Interest_Rate__c,genesis__Product_Type__c,
                                                        genesis__Lending_Product__c,genesis__Days_Convention__c,
                                                        genesis__Expected_First_Payment_Date__c  ,
                                                        genesis__Expected_Start_Date__c,
                                                        genesis__Draw_Period_End_Date__c,
                                                        genesis__Funding_in_Tranches__c,
                                                        genesis__Balloon_Payment__c,
                                                        genesis__Bank_Account_Number__c, 
                                                        genesis__Bank_Account_Type__c,
                                                        genesis__Bank_Name__c, 
                                                        genesis__Routing_Number__c, 
                                                        genesis__Interest_Only_Period__c,
                                                        Number_of_Payments__c,
                                                        Origination_Fee__c,
                                                        Total_Loan_Amount__c,
                                                        com_loan__Loan_Purpose__c,
                                                        Versapay_Token__c,
                                                        Contract_Document_Sent__c,
                                                        Contract_Document_Status__c,
                                                        Versapay_Token_Status__c
                                                   from genesis__Applications__c where Id=:appId];
                                                        
        return application;
    }
    
    public String appChecker(){
        if(application.Lending_Account__c != null)
                return 'Application has already been converted to Loan Account';
        if(application.genesis__Term__c == null || application.genesis__Term__c == 0)
            return 'Term cannot be null';
        if(application.genesis__Loan_Amount__c ==null || application.genesis__Loan_Amount__c == 0)
            return 'Loan Amount cannot be null';
        if(application.genesis__Contact__c ==null)
            return 'Contact cannot be null';
        if(application.genesis__Payment_Amount__c == null || application.genesis__Payment_Amount__c == 0)
            return 'Payment Amount cannot be null';
        if(application.genesis__Expected_First_Payment_Date__c   ==null)
            return 'Expected First Payment Date cannot be null';
        if(application.genesis__Expected_Start_Date__c ==null)
            return 'Expected Start Date cannot be null';
        if(application.genesis__Interest_Rate__c ==null || application.genesis__Interest_Rate__c == 0)
            return 'Interest Rate cannot be null'; 
        if(application.genesis__Payment_Frequency__c ==null)
            return 'Payment Frequency cannot be null';
        /*if(application.ACH_Account_Number__c == null || application.ACH_Account_Type__c == null ||
                application.ACH_Bank_Name__c == null || application.ACH_Routing_Number__c == null)
            return 'ACH information is missing'; */
        if(application.genesis__Status__c.equals('Application Declined by Lendified'))
            return 'Application already declined by Lendified.';
        if(application.Contract_Document_Sent__c != true)
            return 'Contract Document not sent to Applicant.';
        if(application.Contract_Document_Status__c != null && !application.Contract_Document_Status__c.containsIgnoreCase('Completed'))
            return 'Contract Document not yet signed by Applicant.';   
        if(String.isEmpty(application.Versapay_Token__c))
            return 'Versapay Token is not present.';
        if(application.Versapay_Token_Status__c != null && !application.Versapay_Token_Status__c.equalsIgnoreCase('APPROVED'))
            return 'Versapay Token is not Approved.'; 
        
        return '1';
    }
     
    public static loan__Loan_Product__c getLoanProductDetails(String loanProductId) {
        loan__Loan_Product__c product = [select ID, Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Fee_Set__c,
            loan__Draw_Term_Payment_Percent__c,
            loan__Repayment_Billing_Method__c,
            loan__Repayment_Term_Payment_Percent__c,
            loan__Maximum_Draw_Amount__c,
            loan__Minimum_Draw_Amount__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Loan_Cycle_Setup__c,
            loan__Loan_Product_Type__c,
            loan__Repayment_Procedure__c,
            loan__Interest_Calculation_Method__c,
            loan__Frequency_Of_Loan_Payment__c,
            loan__Funder__c,
            loan__Funding_in_Tranches__c,
            loan__Interest_Only_Period__c,
            loan__Draw_Billing_Method__c,
            loan__Time_Counting_Method__c,
            loan__Write_off_Tolerance_Amount__c,
            loan__Pre_Bill_Days__c,
            loan__Delinquency_Grace_Days__c,
            loan__Late_Charge_Grace_Days__c,
            loan__Amortize_Balance_type__c,
            loan__Amortization_Frequency__c,
            loan__Amortization_Enabled__c,
            loan__Amortization_Calculation_Method__c,
            loan__Grace_Period_for_Repayments__c,
            (select ID,Name,
            loan__Default_Interest_Rate__c,
            loan__Default_Number_of_Installments__c,
            loan__Default_Overdue_Interest_Rate__c,
            loan__Fee_Set__c,
            loan__Cycle_Number__c 
            from loan__Loan_Product_Cycle__r)
            from loan__Loan_Product__c where Id = : loanProductId];
        system.debug(Logginglevel.INFO,'Loan product '+ product );   
        return product;                             

    }
   /* public static loan__Coborrower__c getNeonParties(String appId){
        
        loan__Coborrower__c neonParties = [select id,Name
                                                  loan__Account__c,
                                                  loan__Loan__c,
                                                  loan__Contact__c,
                                                  loan__Party_Type__c
                                             from loan__Coborrower__c ];
                                            
          
    }*/
    /*public static genesis__Application_Parties__c getGenesisParties(String appId){
        genesis__Application_Parties__c genesisParties = [select id,Name,
                                                                  genesis__Application__c,
                                                                  genesis__Party_Account_Name__c,
                                                                  genesis__Party_Name__c,
                                                                  genesis__Party_Type__c
                                                             from genesis__Application_Parties__c 
                                                            Where genesis__Application__c =:appId];
        return genesisParties;
    }*/
    public static loan__Office_Name__c getOffice(String officeId){
        loan__Office_Name__c ofc = [Select Id,Name 
                                        from loan__Office_Name__c
                                        where Id = :officeId];
        return ofc;
    }
    
}