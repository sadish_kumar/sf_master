Global class EquifaxIntegration{
    
   
    Webservice static string equifaxCRAccountAction(String contactId){
        Map<String,String> relatedMap = new Map<String,String>();
        relatedMap.put('ints__Contact__c',contactId);
        
        String resp = equifaxCRContact(contactId,relatedMap);
        
        if(String.isEmpty(resp))
            return 'Error : Something went wrong while pulling Credit Report.';
        else if(resp.Contains('Error'))
            return resp;
        
        if([Select Id from ints__EfxReport__c where Id = :resp].size() == 0)
            return 'Error : Something went wrong while pulling Credit Report.';
            
        ints__EfxReport__c ecrp = [Select Id,name,
                                        ints__Contact__c
                                        from ints__EfxReport__c
                                        where Id = :resp];
        
        ecrp.ints__Contact__c = contactId;
        
        try{
            update ecrp;
        }catch(Exception e){
            return e.getMessage();        
        }
        
        return 'Credit Pull Successful.';
    }
    public string equifaxCRApplication1(String applicationId,boolean isTest){
        
        genesis__Applications__c app = [Select Id,Name,genesis__Contact__r.Id,Beacon_9_Score__c ,BNI__c,
                                        genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                        Number_of_Payments__c ,genesis__Expected_Close_Date__c 
                                    from genesis__Applications__c
                                    where Id = :applicationId]; 
                                    
        
        return equifaxCRApplication(app,isTest);
        
    }
    
    public string equifaxCRApplication(genesis__Applications__c app,boolean isTest){
        
        Map<String,String> relatedMap = new Map<String,String>();
        relatedMap.put('ints__Contact__c',app.genesis__Contact__r.Id);
        relatedMap.put('Application__c',app.Id);
        system.debug(LoggingLevel.ERROR,' Contact Id '+app.genesis__Contact__r.Id);
        String resp = '';
        if(isTest){
            List<Contact_Mapping_For_Equifax_Testing__c> mappingList = [Select Equifax_CR_Contact__r.id,Equifax_CR_Contact__r.name from Contact_Mapping_For_Equifax_Testing__c where Eid_Verification_Contact__r.id = :app.genesis__Contact__r.Id limit 1];
            if(mappingList.get(0) != null){
                Id equifaxCRTestingContact = mappingList.get(0).Equifax_CR_Contact__r.id;
                resp = equifaxCRContact(equifaxCRTestingContact,relatedMap);
            }else{
                resp = equifaxCRContact(app.genesis__Contact__r.Id,relatedMap);
            }    
        }else{
        
            resp = equifaxCRContact(app.genesis__Contact__r.Id,relatedMap);
        }    
        system.debug(resp);
        
        if(String.isEmpty(resp))
            return 'Error : Something went wrong while pulling Credit Report.';
        else if(resp.Contains('Error'))
            return resp;
        
        List<ints__EfxReport__c> ecrp = [Select Id,name,
                                        ints__FileSinceDate__c,
                                        ints__Hit_Code__c,
                                        ints__Hit_Description__c,
                                        ints__Contact__c,
                                        Application__c
                                        
                                        from ints__EfxReport__c
                                        where Id = :resp];
        
        if(ecrp == null || ecrp.size() == 0){
            return 'Error : Equifax Report Record not found.';
        }
        
        ecrp[0].ints__Contact__c = app.genesis__Contact__c;
        ecrp[0].application__c = app.Id;
        
        List<ints__EfxReport_Score__c> ecrpScoreList = [Select Id,name,ints__Value__c,ints__description__c from ints__EfxReport_Score__c
                                   where ints__efxReport__c = :ecrp[0].Id];
        
        for(ints__EfxReport_Score__c ecrpScore : ecrpScoreList){
            String desp = ecrpScore.ints__description__c != null ? ecrpScore.ints__description__c : '';
            Decimal score = ecrpScore.ints__Value__c != null ? Decimal.valueOf(ecrpScore.ints__Value__c) : 0;
            if(desp.contains('BEACON')){
                app.Beacon_9_Score__c = score;
            }
            else if(desp.contains('BANK. NAV. INDEX 3')){
                app.BNI__c = score;
            }
        }
        
        try{
            update ecrp;
            update app;
            /*
            LendScore l = new LendScore(app);
            String scoringResp = l.lendScoreCalculate();
            app = l.applicationObj;
            
            if(scoringResp.equalsIgnorecase('Success')){
                update app;
                return scoringResp;        
            }
            else{
                update app;
                return resp;
            }
            
        }catch(DMLException e){
            return e.getMessage();
        */
        
        } catch(Exception e){
            update app;
            return e.getMessage();        
        }
        
        return resp;
    }
    
    public static string equifaxCRContact(String contactId,Map<String,String> relatedMap){
        
        try{
        
            Contact c;
            
            if([Select Id from Contact where Id = :contactId].size() == 0)
                return 'Error: Contact record not found.';
            
            c = [Select Id,FirstName,com_loan__SIN__c,
                        LastName,Birthdate,
                        MailingStreet,MailingCity,MailingState,
                        MailingPostalCode
                        from Contact
                        where Id = :contactId];
            
            System.debug(LoggingLevel.ERROR,'Contact Details '+c);
            
            String civic,street;
            
            if(String.isNotEmpty(c.MailingStreet)){
                Integer l = c.MailingStreet.indexOf(',');
                if(l != -1){
                    civic = c.MailingStreet.substring(0,l);
                    street = c.MailingStreet.substring(l + 1,c.MailingStreet.length());
                }
                else{
                    street = c.MailingStreet;
                }
            }
            
            String dob;
            if(c.Birthdate!= null){
                dob = String.valueOf(c.Birthdate.year());
                if(String.valueOf(c.Birthdate.month()).length() == 1)
                    dob += '0';
                dob += String.valueOf(c.Birthdate.month());
                if(String.valueOf(c.Birthdate.day()).length() == 1)
                    dob += '0';
                dob += String.valueOf(c.Birthdate.day());
            }       
            
            Map<String,String> infoMap = new Map<String,String>();
            infoMap.put('LastName',c.LastName);
            infoMap.put('FirstName',c.FirstName);
            infoMap.put('SocialInsuranceNumber',c.com_loan__SIN__c);
            infoMap.put('DateOfBirth',dob); //19651029
            infoMap.put('CivicNumber',civic);
            infoMap.put('StreetName',street);
            infoMap.put('City',c.MailingCity);
            infoMap.put('ProvinceCode',c.MailingState);
            infoMap.put('ProvinceDescription',null);
            infoMap.put('PostalCode',c.MailingPostalCode);
            
            System.debug(LoggingLevel.ERROR,' CAlling credit pull ');
            
            ints.EquifaxCRAPI ecr = new ints.EquifaxCRAPI();
            
            String resp;
            if(!Test.isRunningTest()){
                resp = ecr.EquifaxCreditPull(infoMap,relatedMap);
                String relatedID = relatedMAp.get('Application__c') != null ? relatedMAp.get('Application__c') : relatedMAp.get('ints__Contact__c');
                system.debug(ecr.getEquifaxResponse().getBody());
                Attachment att = new Attachment();
                att.Body = Blob.valueOf(ecr.getEquifaxResponse().getBody());
                att.parentId = relatedId;
                att.Name = 'Equifax Report - ' + Datetime.now() + '.xml';
                att.ContentType = 'text/xml';
                insert att;
            }
            else{
                //resp = TestHelper.createEquifaxRecords(c.Id).Id;
                return 'test'; 
            }
            System.debug(LoggingLevel.ERROR,' resp '+resp);
            //system.debug(LoggingLevel.ERROR,' body '+ecr.getEquifaxResponse().getBody());
            if(ecr.getErrorMessage() != null)
                return 'Error :' + ecr.getErrorMessage();
                
            if(ecr.getEquifaxRecord() != null)
                return ecr.getEquifaxRecord().Id;
                
        }catch(Exception e){
            return 'Error :' + e.getMessage()+'. from line: '+e.getLineNumber();
        }
                
        
        return null;
    }
    
    public string lendScoreCalculate(genesis__Applications__c applicationObj){
     
        //Error Messages String
        String errMessageBeacon9NotinRange      = 'Beacon 9 score is not in Range of Equifax Sheet.';
        String errMessageBNI3NotinRange         = 'BNI 3.0 score is not in Range of Equifax Sheet.';
        String errMessageLendingScoreNotinRange = 'Lending Score is not in Range of Equifax Sheet.';
        String errMessageBeacon9Decile1         = 'Beacon 9 Decile is 1.';
        String errMessageBNI3Decile1            = 'BNI 3.0 Decile is 1.';
        String errMessageBeaconBNIMandt         = 'BNI 3.0 score and Beacon 9 are mandatory for calculation'; 
        
        // For getting the List of Loss Rate Decile Comparator and differentiating the Beacon 9 and BNI 3 
        // based on Record Type.
        
         String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        
        List<Loss_Rate_Decile_Comparator__c> lossRateList1 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                             RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                             where Max_Range__c <= :applicationObj.Beacon_9_Score__c and Min_Range__c >= :applicationObj.Beacon_9_Score__c
                                                             and RecordTypeId = :beacon9RTID];
        
        
        
        
        if(lossRateList1 == null || lossRateList1.size() == 0){
            return 'Error : ' + errMessageBeacon9NotinRange;
        }
        else{
            applicationObj.Beacon_9_Score__c = lossRateList1[0].Loss_Rate__c;
            applicationObj.Beacon_9_Decile__c = lossRateList1[0].Decile__c;
        }
        
        
        
        List<Loss_Rate_Decile_Comparator__c> lossRateList2 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                             RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                             where Max_Range__c <= :applicationObj.BNI__c and Min_Range__c >= :applicationObj.BNI__c
                                                             and RecordTypeId = :bni3RTID];
        
        
        if(lossRateList2 == null || lossRateList2.size() == 0){
            update applicationObj;
            return 'Error : ' + errMessageBNI3NotinRange;
        }else{
            applicationObj.BNI_Loss_Rate__c = lossRateList2[0].Loss_Rate__c;
            applicationObj.BNI_Decile__c = lossRateList2[0].Decile__c;
        }
        
        
        if(applicationObj.Beacon_9_Decile__c == 1){
            update applicationObj;
            return 'Error : ' + errMessageBeacon9Decile1;
        }else if(applicationObj.BNI_Decile__c == 1){
            update applicationObj;
            return 'Error : ' + errMessageBNI3Decile1;
        }
        if(applicationObj.Beacon_9_Loss_Rate__c >= applicationObj.BNI_Loss_Rate__c){
            applicationObj.Lend_Score__c = applicationObj.Beacon_9_Loss_Rate__c;
        }else{ //if(applicationObj.Beacon_9_Loss_Rate__c < applicationObj.BNI_Loss_Rate__c){
            applicationObj.Lend_Score__c = (applicationObj.Beacon_9_Loss_Rate__c * 0.25) + (applicationObj.BNI_Loss_Rate__c * 0.75);
        }
        
        List<Lend_Score__c> lendScoreList = [Select Id, Applied_Interest_Rate__c, Decile__c, Lend_Score_Max_Value__c, Lend_Score_Min_Value__c 
                         from Lend_Score__c where Lend_Score_Max_Value__c <= :applicationObj.Lend_Score__c
                         and Lend_Score_Min_Value__c >= :applicationObj.Lend_Score__c];    
        
        if(lendScoreList == null || lendScorelist.size() == 0){
            update applicationObj;
            return 'Error : ' + errMessageLendingScoreNotinRange;
        }else{
            applicationObj.Lend_Score_Decile__c           = lendScoreList[0].Decile__c;
            applicationObj.Bankruptcy_Navigation_Index__c = lendScoreList[0].Applied_Interest_Rate__c;
        }
        
        
        update applicationObj;
        
        return 'Success.';
    }
    
    
    
}