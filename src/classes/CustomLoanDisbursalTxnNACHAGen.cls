global class CustomLoanDisbursalTxnNACHAGen{ 
    // extends loan.FileGenerator{
    /* commenting since we are disbursing through Versapay....
    
    loan__ACH_Parameters__c achParams = loan.CustomSettingsUtil.getACHParameters();
    public Long amountTotal = 0;
    @TestVisible private List<Loan__loan_Disbursal_Transaction__c> requeryScope(List<SObject> scope){
        Set<ID> ids = new Set<ID>();
        for(SObject s : scope){
            ids.add(s.Id);
        }
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        mfiflexUtil.ObjectCache dsbOC = ec.createObject('LoanDisbursalsForACH',
                                                'Loan__loan_Disbursal_Transaction__c');
        String s = 'loan__Mode_of_Payment__c,Loan__Loan_Account__c,loan__Disbursed_Amt__c,loan__Disbursal_Date__c';
        dsbOC.addFields(s);
        dsbOC.addFields('loan__Loan_Account__r.loan__ACH_On__c,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Account__r.Name,'+
                       'loan__Loan_Account__r.loan__Account__r.name,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.Branch_Transit_Number__c,'+ 
                       'loan__Loan_Account__r.loan__Account__r.peer__First_Name__c,'+
                       'loan__Loan_Account__r.loan__Account__r.peer__Last_Name__c,'+
                       'loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Account__c,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Bank_Account_Number__c,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account__r.Name,'+
                       'loan__Loan_Account__r.loan__Borrower_ACH__r.loan__Account__c');
       dsbOC.addNamedParameter('ids',ids);
       dsbOC.setWhereClause('Id IN:ids');
       dsbOC.executeQuery();
        return(List<loan__loan_disbursal_transaction__c>)dsbOC.getRecords();   
    }
    
    public override String getSimpleFileName(){
        return 'Loan_Disbursals';
    }
    
    public override List<String> getEntries(Loan.TransactionSweepToACHState state, List<SObject> scope){
        //need to requery scope
        List<loan__loan_Disbursal_Transaction__c> dsbTxns = requeryScope(scope);
        List<string> retVal = new List<String>();
        List<SObject> objects = new List <SObject>();
        for (loan__loan_Disbursal_Transaction__c d :dsbTxns  ) {
            String name  = d.loan__Loan_Account__r.loan__Account__r.peer__Last_Name__c + ', ' + d.loan__Loan_Account__r.loan__Account__r.peer__First_Name__c;
            System.debug('name----'+name);
            if(d.loan__Loan_Account__r.loan__ACH_On__c == true){
                if(d.loan__Loan_Account__r.loan__Borrower_ACH__r!=null){
                    Detail__c detail = new Detail__c();
                    detail.Crdeit_Debit_Indentifier__c= 'C';
                    detail.Institution_ID__c = achParams.Institution_ID__c;
                    detail.Branch_Transit_Number__c = d.loan__Loan_Account__r.loan__Borrower_ACH__r.Branch_Transit_Number__c ; 
                    detail.Payee_Payor_Account_Number__c = d.loan__Loan_Account__r.loan__OT_Borrower_ACH__r.loan__Bank_Account_Number__c;
                    detail.Amount__c = String.valueOf(d.loan__Disbursed_Amt__c.setScale(2));
                    amountTotal+=(Long)Double.valueOf(detail.Amount__c);
                    detail.Cross_Reference_Number__c = d.name;
                    detail.Payee_Payor_Name__c = name;
                    system.debug('Detail:'+detail);
                    objects.add(detail);
                    addToValidScope(d);
                }else{
                    addToInvalidScope(d,'bank details not present for transaction');
                }
            }
        
        }
        filegen.CreateSegments segments =new filegen.CreateSegments(objects);
        retVal = segments.retString();
        for(String line:retVal){
            line = line+'\r\n';
            addEntry(line);
        }
        return retVal;
    }
    
     public override String getHeader(loan.TransactionSweepToACHState state, List<SObject> scope) {
       List<String> retVal = new List<String>();
       List<Sobject> objects = new List<Sobject>();
       List<Sobject> headerRecords = new List<Sobject>();
       String header='';
       objects = getValidScope();
       if(objects.size()!=0){
           File_Header__c fileHeader = new File_Header__c();
           fileHeader.Origination_Number__c = achParams.Originator_Number__c;
           fileHeader.Destination_Data_Center__c = achParams.Originator_Number__c.substring(0,5);
           fileHeader.File_Creation_Date__c = Date.Today(); //(Date) new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
           fileHeader.File_Creation_Number__c = String.valueof(state.counter) ;
           fileHeader.Institution_Number__c = achParams.Institution_ID__c;
           fileHeader.Branch_Transit_Number__c = achParams.Branch_Transit_Number__c;
           fileHeader.Account_Number__c = achParams.CIBC_settlement_account_number__c;
           fileHeader.Originators_Short_Name__c = achParams.Originator_s_Short_Name__c;
           fileHeader.Currency_Indicator__c = 'CAD';
           headerRecords.add(fileHeader);
           Batch_Header__c batchHeader = new Batch_Header__c();
           batchHeader.Transaction_Code__c = '351';// achPArams.Payments_Batch_Header_Transaction_Code__c; 
           //batchHeader.Sundry_Information__c = 
           batchHeader.Value_Date__c =  (Date) new loan.GlobalLoanUtilFacade().getCurrentSystemDate();
           headerRecords.add(batchHeader);
           filegen.CreateSegments segments = new filegen.CreateSegments(headerRecords);
           retval = segments.retString();
           header =retVal[0]+'\r\n';
           header+=retVal[1] + '\r\n';
           return header;
       }else{
           return 'header'; 
       }
    }
    
    public override String getTrailer(loan.TransactionSweepToACHState state, LIST<SObject> scope) {
       List<String> retVal = new List<String>();
       List<Sobject> objects = new List<Sobject>();
       List<Sobject> trailerRecords = new List<Sobject>();
       String trailer='';
       Decimal sumOfAmount = 0;
       objects = getValidScope();
       if(objects.size()!=0){
            File_Trailer__c fileTrailer = new File_Trailer__c();
            for(SObject obj:getValidScope()){  
                sumOfAmount+=(Decimal)obj.get('loan__Disbursed_Amt__c');
            }
            fileTrailer.Detail_Count__c = String.valueOf(sumOfAmount.setScale(2));
            fileTrailer.Batch_Count__c = '1';
            
            
            Batch_Trailer__c batchTrailer = new Batch_Trailer__c();
            batchTrailer.Transaction_Code__c = '391';
            batchTrailer.Batch_Entry_Count__c = '1';
            batchTrailer.Reserved__c = '0';
            batchTrailer.Entry_Dollar_Amount__c = String.valueOf(sumOfAmount.setScale(2));
            
            trailerRecords.add(batchTrailer);
            trailerRecords.add(fileTrailer);
            filegen.CreateSegments segments = new filegen.CreateSegments(trailerRecords);
            retval = segments.retString();
            trailer =retVal[0]+'\r\n';
            trailer+=retVal[1]+'\r\n';
            return trailer;
       }else{
           return 'trailer'; 
       }
    }
    */
    
}