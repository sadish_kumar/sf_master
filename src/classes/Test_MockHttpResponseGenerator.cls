@isTest
global class Test_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) { 
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><metadataServerUrl>https://cs22.salesforce.com/services/Soap/m/31.0/00D17000000AfTG</metadataServerUrl>'+
        '<passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>https://cs22.salesforce.com/services/Soap/u/31.0/00D17000000AfTG</serverUrl><sessionId>00D17000000AfTG!ARkAQPMVnZOHUlaHI3Bnl0mGiQMFpTHnQGFrU.9GoQGpAIljJewt4l5nHJEUNOdKPLn_VFscBspu58GTYWq4nY0H5K6jPgYb</sessionId><userId>0051a000000R3LDAA0</userId><userInfo><accessibilityMode>false</accessibilityMode>'+
        '<currencySymbol>$</currencySymbol><orgAttachmentFileSizeLimit>5242880</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode>USD</orgDefaultCurrencyIsoCode><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments><orgHasPersonAccounts>false</orgHasPersonAccounts><organizationId>00D17000000AfTGEA0</organizationId><organizationMultiCurrency>false</organizationMultiCurrency>'+
        '<organizationName>Vault Circle</organizationName><profileId>00e1a000000m8aoAAA</profileId><roleId>00E1a000000Xbf9EAC</roleId><sessionSecondsValid>7200</sessionSecondsValid><userDefaultCurrencyIsoCode xsi:nil="true"/><userEmail>mfiflex.code@gmail.com</userEmail><userFullName>CLI Admin</userFullName><userId>0051a000000R3LDAA0</userId><userLanguage>en_US</userLanguage><userLocale>en_CA</userLocale>'+
        '<userName>mfiflex.code@14trialforce.com.qa</userName><userTimeZone>Asia/Kolkata</userTimeZone><userType>Standard</userType><userUiSkin>Theme3</userUiSkin></userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>');
        res.setStatusCode(200);
        return res;
    }
}