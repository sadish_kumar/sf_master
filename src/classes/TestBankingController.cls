@isTest
public class TestBankingController{
      public testMethod static void testBank(){
      Account acc = new Account();
      acc.Name = 'Ext';
      insert acc;
      
       genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id);
       insert obj;
       loan__Bank_Account__c lba = new loan__Bank_Account__c(loan__Bank_Account_Number__c='123', loan__Bank_Name__c='test',loan__Account__c=acc.Id );
       insert lba;       
       ApexPages.StandardController com = new ApexPages.StandardController(obj);
        BankingController bankingCon = new BankingController(com);
        bankingCon.submit();
    }  

   public testMethod static void testBank1(){
      Account acc = new Account();
      acc.Name = 'Ext';
      insert acc;
      
       genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id);
       insert obj;
       loan__Bank_Account__c lba = new loan__Bank_Account__c(loan__Bank_Account_Number__c='123', loan__Bank_Name__c='test',loan__Account__c=acc.Id );
       insert lba;         
        ApexPages.StandardController com = new ApexPages.StandardController(obj);
        BankingController bankingCon = new BankingController(com);
        bankingCon.application.com_loan__Annual_Revenues__c=Decimal.ValueOf('12345812312312777');
        bankingCon.submit();
    }   
}