@isTest
public class Test_CustomLoanDisbursalTxnNACHAGen {
    /* Commenting since we are no longer using CustomLoanDisbursalTxnNACHAGen class...
  

    public static TestMethod void testData() {
         loan__ACH_Parameters__c objAchParameters = new loan__ACH_Parameters__c();
         objAchParameters.loan__Days_In_Advance_To_Create_File__c = 0;
         objAchParameters.loan__ACH_Return_Filegen__c = 'ACHReturnFileParser';
         objAchParameters.loan__Loan_Payment_Transaction_Filegen__c = 'CustomLoanPaymentTxnNACHAGen';
         objAchParameters.loan__Lock_Period_for_Loan_Payments__c  = 3;
         objAchParameters.loan__Merge_One_Time_and_Recurring_ACH__c = true;
         objAchParameters.Originator_Number__c = '15256';
         objAchParameters.CIBC_settlement_account_number__c = '817217';
         objAchParameters.loan__Organization_Name__c = 'Vault Circle';
         objAchParameters.loan__Folder_Name__c = 'ACH Folder';
         objAchParameters.loan__Loan_Disbursal_Transaction_Filegen__c = 'CustomLoanDisbursalTxnNACHAGen';
         insert objAchParameters ;
         try{
         CustomLoanDisbursalTxnNACHAGen  objCustom = new CustomLoanDisbursalTxnNACHAGen();
         objCustom.getSimpleFileName();
         objCustom.getEntries(null,null);
         objCustom.getHeader(null,null);
         objCustom.getTrailer(null,null);
         }Catch(Exception ex) {
         
        }
    }
  
  public static TestMethod void testData1(){
     loan__ACH_Parameters__c objAchParameters = new loan__ACH_Parameters__c();
     objAchParameters.loan__Days_In_Advance_To_Create_File__c = 0;
     objAchParameters.loan__ACH_Return_Filegen__c = 'ACHReturnFileParser';
     objAchParameters.loan__Loan_Payment_Transaction_Filegen__c = 'CustomLoanPaymentTxnNACHAGen';
     objAchParameters.loan__Lock_Period_for_Loan_Payments__c  = 3;
     objAchParameters.loan__Merge_One_Time_and_Recurring_ACH__c = true;
     objAchParameters.Originator_Number__c = '15256';
     objAchParameters.CIBC_settlement_account_number__c = '817217';
     objAchParameters.loan__Organization_Name__c = 'Vault Circle';
     objAchParameters.loan__Folder_Name__c = 'ACH Folder';
     objAchParameters.loan__Loan_Disbursal_Transaction_Filegen__c = 'CustomLoanDisbursalTxnNACHAGen';
     objAchParameters.loan__Use_Lock_Based_ACH__c=true;
     insert objAchParameters ;
     
      loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c gnAp= genesis.TestHelper.createLoanApplication(); 
        gnAp.Contract_Document_Status__c='Completed';
        update gnAp;
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         loan__Fee__c dummyFee1 = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         dummyFee1.loan__Time_of_charge__c ='Pre-Paid Fees';
         update dummyFee1;                                 
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;       
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;
        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        loan__Product_Pre_Paid_Fees__c PreFee = new loan__Product_Pre_Paid_Fees__c(loan__Fee_Type__c=dummyFee1.id,
                                                                                   loan__Enabled__c =true,
                                                                                   loan__Lending_Product__c=dummyLP.id,
                                                                                   loan__Amortize_Balance__c='Pre-Paid Fees'
                                                                                    );
        insert PreFee ;
        
        
        loan__Bank_Account__c bankacc = new loan__Bank_Account__c(loan__Bank_Account_Number__c='12345678',
                                                                  loan__Bank_Name__c='ICICI'
                                                                       );
        insert   bankacc ;                                                             
         loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
                                                    
        loanAccount .loan__ACH_On__c = true;
        loanAccount.loan__Borrower_ACH__c =bankacc .id; 
        loanAccount.loan__ACH_Routing_Number__c = '123345678';
        update loanAccount;                                             
     loan__Contract_Pre_Paid_Fee__c  PreConFee = loan.TestHelper.createContractPrePaidFee(loanAccount,dummyFee1,PreFee,true);                                               
     list<loan__Loan_Disbursal_Transaction__c > lstLoanDisbursalTxn = new list<loan__Loan_Disbursal_Transaction__c >();
   
     loan__Loan_Disbursal_Transaction__c objDisbursalTxn = new loan__Loan_Disbursal_Transaction__c();
     objDisbursalTxn.loan__Loan_Account__c = loanAccount.id; 
     objDisbursalTxn.loan__Disbursal_Date__c = date.today();
     lstLoanDisbursalTxn.add(objDisbursalTxn );
     
     Test.startTest();
     insert lstLoanDisbursalTxn;
     
     list<Sobject> lstSobj = new list<Sobject>();
     for(loan__Loan_Disbursal_Transaction__c  objLoanDistxn :lstLoanDisbursalTxn){
      Sobject sobj = (Sobject)objLoanDistxn ;
      lstSobj.add(sobj);
     }
     loan.TransactionSweepToACHState state = new loan.TransactionSweepToACHState();
     state.counter = 2; 
     CustomLoanDisbursalTxnNACHAGen  objCustom = new CustomLoanDisbursalTxnNACHAGen();
     objCustom.requeryScope(lstLoanDisbursalTxn);
     objCustom.getSimpleFileName();
     objCustom.getEntries(state,lstLoanDisbursalTxn);
     try{
     objCustom.getHeader(state,lstLoanDisbursalTxn);
     }catch(Exception e){
     }
      try{
     objCustom.getTrailer(state,lstLoanDisbursalTxn);
     }catch(Exception e){
     }
     
     
     Test.stopTest();
    }
    */
    
}