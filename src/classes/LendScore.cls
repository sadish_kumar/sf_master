Public class LendScore{
    
    public genesis__Applications__c applicationObj;
    
    public LendScore(genesis__Applications__c app){
        this.applicationObj = app;
    }
    
    public string lendScoreCalculate(){
        
        //Error Messages String
        String errMessageBeacon9NotinRange      = 'Beacon 9 score is not in Range of Equifax Sheet.';
        String errMessageBNI3NotinRange         = 'BNI 3.0 score is not in Range of Equifax Sheet.';
        String errMessageLendingScoreNotinRange = 'Lending Score is not in Range of Equifax Sheet.';
        String errMessageBeacon9Decile1         = 'Beacon 9 Decile is 1.';
        String errMessageBNI3Decile1            = 'BNI 3.0 Decile is 1.';
        String errMessageBeaconBNIMandt         = 'BNI 3.0 score and Beacon 9 are mandatory for calculation'; 
        
        // For getting the List of Loss Rate Decile Comparator and differentiating the Beacon 9 and BNI 3 
        // based on Record Type.
        if(this.applicationObj.BNI__c == null || this.applicationObj.Beacon_9_Score__c == null)
            return 'Error : BNI or Beacon 9 value not found.';
        
        system.debug('beacon 9 : ' + this.applicationObj.Beacon_9_Score__c);
        
        String beacon9RTID = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('Beacon 9 Model').getRecordTypeId();
        String bni3RTID    = Schema.SObjectType.Loss_Rate_Decile_Comparator__c.getRecordTypeInfosByName().get('BNI 3.0 Model').getRecordTypeId();
        
        List<Loss_Rate_Decile_Comparator__c> lossRateList1 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                              RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                              where Max_Range__c >= :this.applicationObj.Beacon_9_Score__c and Min_Range__c <= :this.applicationObj.Beacon_9_Score__c
                                                              and RecordTypeId = : beacon9RTID];
        
        
        
        
        if(lossRateList1 == null || lossRateList1.size() == 0){
            return 'Error : ' + errMessageBeacon9NotinRange;
        }
        else{
            this.applicationObj.Beacon_9_Loss_Rate__c = lossRateList1[0].Loss_Rate__c;
            this.applicationObj.Beacon_9_Decile__c = lossRateList1[0].Decile__c;
        }
        
        
        
        List<Loss_Rate_Decile_Comparator__c> lossRateList2 = [Select Id, Decile__c, Loss_Rate__c, Max_Range__c, Min_Range__c, RecordTypeId,
                                                              RecordType.Name from Loss_Rate_Decile_Comparator__c 
                                                              where Max_Range__c >= :this.applicationObj.BNI__c and Min_Range__c <= :this.applicationObj.BNI__c
                                                              and RecordTypeId = : bni3RTID];
        
        
        if(lossRateList2 == null || lossRateList2.size() == 0){
            return 'Error : ' + errMessageBNI3NotinRange;
        }else{
            this.applicationObj.BNI_Loss_Rate__c = lossRateList2[0].Loss_Rate__c;
            this.applicationObj.BNI_Decile__c = lossRateList2[0].Decile__c;
        }
        
        
        if(this.applicationObj.Beacon_9_Decile__c == 1){
            return 'Error : ' + errMessageBeacon9Decile1;
        }else if(this.applicationObj.BNI_Decile__c == 1){
            return 'Error : ' + errMessageBNI3Decile1;
        }
        if(this.applicationObj.Beacon_9_Loss_Rate__c >= this.applicationObj.BNI_Loss_Rate__c){
            this.applicationObj.Lend_Score__c = this.applicationObj.Beacon_9_Loss_Rate__c;
        }else{ //if(this.applicationObj.Beacon_9_Loss_Rate__c < this.applicationObj.BNI_Loss_Rate__c){
            this.applicationObj.Lend_Score__c = (this.applicationObj.Beacon_9_Loss_Rate__c * 0.25) + (this.applicationObj.BNI_Loss_Rate__c * 0.75);
        }
        
        List<Lend_Score__c> lendScoreList = [Select Id, Applied_Interest_Rate__c, Decile__c, Lend_Score_Max_Value__c, Lend_Score_Min_Value__c,
                                             Grade__c, Origination_Fee__c,Lending_Product__c
                                             from Lend_Score__c where Lend_Score_Max_Value__c >= :this.applicationObj.Lend_Score__c
                                             and Lend_Score_Min_Value__c <= :this.applicationObj.Lend_Score__c];    
        
        if(lendScoreList == null || lendScorelist.size() == 0){
            return 'Error : ' + errMessageLendingScoreNotinRange;
        }else{
            this.applicationObj.Lend_Score_Decile__c = lendScoreList[0].Decile__c;
            this.applicationObj.genesis__Interest_Rate__c = lendScoreList[0].Applied_Interest_Rate__c * 100;
            this.applicationObj.Lend_Score_Grade__c   = lendScoreList[0].Grade__c;
            this.applicationObj.Origination_Fee_Percent__c = lendScoreList[0].Origination_Fee__c;
            this.applicationObj.Lending_Product__c   =  lendScoreList[0].Lending_Product__c;
        }
        
        //EMI calculation...
        
        //system.debug('EMI - Start');
        
        /*this.applicationObj.genesis__Expected_Start_Date__c = date.Today();
        this.applicationObj.genesis__Expected_First_Payment_Date__c= date.Today().addDays(14);
        */
        
        //set application's start date to CL Loan's date... and first payment date to CL Loan's date + 14...
        loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();
        this.applicationObj.genesis__Expected_Start_Date__c = info.getCurrentSystemDate();
        this.applicationObj.genesis__Expected_First_Payment_Date__c = this.applicationObj.genesis__Expected_Start_Date__c.addDays(14);
        
        //System.debug(this.applicationObj);
        
        if(this.applicationObj.genesis__Term__c != null){
            this.applicationObj.genesis__Expected_Close_Date__c = loan.DateUtil.addCycle(this.applicationObj.genesis__Expected_First_Payment_Date__c,
                                                        (Integer)this.applicationObj.genesis__Expected_First_Payment_Date__c.day(),
                                                        this.applicationObj.genesis__Payment_Frequency__c,
                                                        (Integer)(this.applicationObj.Number_of_Payments__c -1));
        }
        
        
        
        
        
        String dayConvention = this.applicationObj.genesis__Days_Convention__c != null ?
            this.applicationObj.genesis__Days_Convention__c : '30/360';
        String billingMethodCode = this.applicationObj.genesis__Interest_Calculation_Method__c != null ? 
            this.applicationObj.genesis__Interest_Calculation_Method__c : 'Declining Balance';   
        
        genesis__Lending_Calculator__c calc = new genesis__Lending_Calculator__c(
            genesis__Accrual_Base_Method_Code__c = dayConvention,
            genesis__Action__c = 'CALCULATION_ALL',
            genesis__Additional_Interest_Amount__c = 0,
            genesis__Amortization_Calculation_Method_Code__c = 'NONE', 
            genesis__APR__c = 0,
            genesis__Balance_Amount__c = 0,
            genesis__Balloon_Method_Code__c = 'DUMMY',
            genesis__Balloon_Payment_Amount__c = 0,
            genesis__Billing_Method_Code__c = billingMethodCode,
            genesis__Contract_Date__c = this.applicationObj.genesis__Expected_Start_Date__c,
            genesis__Final_Payment_Amount__c = 0,
            genesis__Financed_Amount__c = 0,
            genesis__Financed_Fees__c = 0,
            genesis__First_Payment_Date__c = this.applicationObj.genesis__Expected_First_Payment_Date__c, 
            genesis__First_Period_Calender_Days__c = 0,
            genesis__First_Period_Interest__c = 0,
            genesis__Flexible_Repayment_Flag__c = false,
            genesis__Installment_Method_Code__c = 'UNDEFINED',
            genesis__Interest_Amount__c = 0,
            genesis__Interest_Only_Period__c = 0,
            genesis__Loan_Amount__c = this.applicationObj.genesis__Loan_amount__c,
            genesis__Payment_Amount__c = 0,
            genesis__Payment_Frequency_Code__c = this.applicationObj.genesis__Payment_Frequency__c,
            genesis__Prepaid_Fees__c = 0,
            genesis__Principal_Payment_Amount__c = 0,
            genesis__Rate__c = this.applicationObj.genesis__Interest_Rate__c,
            genesis__Repayment_Type_Code__c = 'UNDEFINED',
            genesis__Term__c = this.applicationObj.Number_of_Payments__c,
            genesis__Total_Finance_Charge__c = 0,
            genesis__Total_Financed_Amount__c = 0,
            genesis__Total_Of_Payments__c = 0
            
        );
          
        /*Decimal totalAmount = 0;
        Decimal totalPrincipal = 0;
        Decimal totalInterest = 0;
        */
        
        List<genesis__Amortization_schedule__c> emi;
        System.debug( 'before calculations, calc : ' + calc );
        try {
            
            
            
            emi = genesis.LendingCalculator.calculate(calc, null);
            system.debug(calc);
            
            for(genesis__Amortization_Schedule__c e : emi){
                /*totalAmount += e.genesis__Total_Due_Amount__c;
                totalPrincipal += e.genesis__Due_Principal__c;
                totalInterest += e.genesis__Due_interest__c;*/
                
                e.genesis__Application__c = applicationObj.Id;
            }
            this.applicationObj.genesis__Payment_Amount__c = calc.genesis__Payment_Amount__c;
            /*totalAmount = totalAmount.setScale(2);
            totalPrincipal = totalPrincipal.setScale(2);
            totalInterest = totalinterest.setScale(2); 
            */
            
            List<genesis__Amortization_schedule__c> emilist = [Select id from genesis__Amortization_schedule__c where genesis__Application__c =: this.applicationObj.Id];
            delete emilist;
            
            this.applicationObj.genesis__Status__c = 'Offer Generated';
            
            update this.applicationObj;
            insert emi;
            
        } catch (Exception e) {
            emi = new List<genesis__Amortization_Schedule__c>();
            return 'Error : ' + e.getMessage()+ e.getStackTraceString();
        }
        
        return 'Success.';
    }
  
 
}