global with sharing class CreditPullCtrl extends genesis.ApplicationWizardBaseClass{
    public genesis__Applications__c application {get; set;}
    
    public genesis__Applications__c app;
    public String reportId;
    
    global CreditPullCtrl(ApexPages.StandardController controller) {
        super(controller);
        this.application = (genesis__Applications__c)controller.getRecord();
        this.application = [Select Id,Name,genesis__Account__r.Id,
                                    genesis__Contact__c,Beacon_9_Score__c,
                                    BNI__c,genesis__Term__c
                                    
                                    from genesis__Applications__c
                                    where Id = :this.application.Id];
    }
    
    global void CreditPull(){
        try{
            
            ints__Org_Parameters__c org = ints__Org_Parameters__c.getOrgDefaults();
            Boolean isTest =false;
            if(org != null && org.Equifax_Test__c){
                isTest = true;
            }
            EquifaxIntegration eInts = new EquifaxIntegration();
            String resp = eInts.equifaxCRApplication1(this.application.Id,isTest);
            
            system.debug(resp);
            CreditEngine.postLoanApplciation(this.application.Id);
            if(String.isEmpty(resp)){
                this.application.genesis__Status__c = 'Declined - No Hit';
                update this.application;
                createMessage(ApexPages.Severity.ERROR,'Error : Something went wrong while pulling Credit Report.');
                return ;
            }
            else if(resp.Contains('Error')){
                this.application.genesis__Status__c = 'Declined - No Hit';
                update this.application;
                createMessage(ApexPages.Severity.ERROR, resp);
                return ;
            }
            
            if([Select Id from ints__EfxReport__c where Id = :resp].size() == 0){
                this.application.genesis__Status__c = 'Declined - No Hit';
                update this.application;
                
                createMessage(ApexPages.Severity.ERROR,'Error : Something went wrong while pulling Credit Report.' + resp);
                return ;
            }
                
            ints__EfxReport__c ecrp = [Select Id,name,
                                            Account__c,
                                            application__c,
                                            ints__FileSinceDate__c,
                                            ints__Hit_Code__c
                                            
                                            from ints__EfxReport__c
                                            where Id = :resp];
            
            //this.application = eInts.getCRValues(this.application,ecrp);
            
            //String checks = EquifaxIntegration.MBR(this.application);
            /*if(checks.equals('Error : Hit Code failed.')){
                this.application.genesis__Status__c = 'Declined - Failed.';
                this.application.failure_reason__c = checks;
            }    
            else if(!checks.equals('1')){
                this.application.genesis__Status__c = 'Declined - Failed.';
                this.application.failure_reason__c = checks;
                createMessage(ApexPages.Severity.ERROR, checks);
            }
            else{ 
                this.application.genesis__Status__c = 'New - In Credit Check';
                
            }
            system.debug(this.application.genesis__Status__c);    
            */
            
            update this.application;
            
        }catch(Exception e){
            createMessage(ApexPages.Severity.ERROR,e.getMessage() + e.getLineNumber());
            return ;        
        }
        
        createMessage(ApexPages.Severity.CONFIRM,'Credit Pull Successful.');
        
    }

    private void createMessage(ApexPages.severity severity, String message) {
        ApexPages.addmessage(new ApexPages.message(severity, message));
    }   
    
    
    
}