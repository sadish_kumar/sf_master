@isTest
private class Test_CreditPullCtrl {
    
    @isTest static void test_method_one() {
        // Implement test code
        genesis__applications__c application = genesis.TestHelper.createLoanApplication();
        application.Beacon_9_Score__c        = 600 ;
        application.BNI__c                   = 600 ;
        ApexPages.StandardController con     = new ApexPages.StandardController(application);
        CreditPullCtrl newcreditObj          = new CreditPullCtrl(con);
        newcreditObj.CreditPull();

        ints__Org_Parameters__c intOrg = new ints__Org_Parameters__c(Equifax_Test__c = true, ints__Namespace_Prefix__c = 'ints');
        insert intOrg;
        newcreditObj.CreditPull();

    }
}