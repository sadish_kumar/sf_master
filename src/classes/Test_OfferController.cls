@istest
public class Test_OfferController {
  
    public  testMethod static void testM1(){
        Account acc = new Account();
        acc.Name = 'Ext';
        insert acc;
      
        genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id);
        insert obj;
        
        Scoring__c objScore = new Scoring__c(Credit_Score_High_Value__c=10,Credit_Score_Low_Value__c=5,Interest_Rate__c=2,
                                           Max_Loan_Amount__c=20,Term__c=10);
        insert objScore;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        OfferController  objOffer = new OfferController(sc);
        objOffer.getScore();
        objOffer.application.com_loan__Annual_Revenues__c=Decimal.ValueOf('12345812312312777');
        objOffer.submit();
    }
    
    public  testMethod static void testM2(){
        Account acc = new Account();
        acc.Name = 'Ext';
        insert acc;
      
        genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id);
        insert obj;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        OfferController  objOffer = new OfferController(sc);
        objOffer.getScore();
        objOffer.submit();
        
        
    }
}