global without sharing class LoanOfferController extends com_loan.BaseController {
    global Decimal rateOfInterest { get; set; }
    global Decimal paymentAmount { get; set; }
    global Decimal arrangementFees { get; set; }
    global String applicantEmail { get; set; }
    global String rejectedFeedback { get; set; }
    global String rejectedReason  { get; set; }
    global Decimal lendScore {get;set;}
    global String lendScoreGrade {get;set;}
    global String applicationTrackId { get; set; }
    
    global LoanOfferController() {
        applicationTrackId=ApexPages.currentPage().getParameters().get('id');
       // arrangementFees=2.00;
        genesis__applications__c application = com_loan.CommunityUtil.getApplicationFromId(applicationId);
        rateOfInterest = application.genesis__Interest_Rate__c;
        if(rateOfInterest!=null)
        rateOfInterest=rateOfInterest.setScale(2);
        paymentAmount = application.genesis__Payment_Amount__c;
        applicantEmail = application.Applicant_s_Email__c;
        lendScore = application.Lend_Score__c;
        arrangementFees=application.Origination_Fee_Percent__c;
        lendScoreGrade=application.Lend_Score_Grade__c;
        
    }
    
    global PageReference continueAction() {
        //accept logic goes here
        try {
            //system.debug('***');
             genesis__applications__c application = [Select Name,Id,Versapay_Token__c,genesis__Status__c,genesis__Company__c, 
                                                    genesis__Lending_Product__c,Lending_Product__c from genesis__applications__c
                                                    where Id =: applicationId limit 1];
                  
            if(application.Versapay_Token__c==null || application.Versapay_Token__c==''){
                loan_vp.VersapayImpl par = new loan_vp.VersapayImpl();
                loan_vp.PADResponse pr = par.createPADAgreement(applicantEmail, '', applicationId);
                application.Versapay_Token__c=pr.token;
                application.genesis__Status__c='PAD AGREEMENT SENT';
            }
            // system.debug('***'+pr);
            //drawloop doc gene
           /* try{
      //  system.debug('***');
               // sendDocument();
               // system.debug('***');
            }catch(Exception ex )
            {
                System.debug('Error'+ ex.getStackTraceString());
            }
            */
     
            List<loan__Office_Name__c> companyList=[ SELECT Name,ID FROM loan__Office_Name__c ];
            if(companyList!=null && companyList.size()>0){
                application.Company__c = companyList.get(0).id;
            }
           // system.debug('***companyList'+companyList);
            Lendified_Parameters__c org = Lendified_Parameters__c.getOrgDefaults(); 
              
            List<loan__Loan_Product__c> productList = [ SELECT Name,ID FROM loan__Loan_Product__c where Id =: org.Default_Lending_Product_Id__c];
            if(application.Lending_Product__c==null && productList!=null && productList.size()>0){
                application.Lending_Product__c =productList.get(0).id;
            }
           // system.debug('***productList'+productList);
           
           application.genesis__Status__c='Offer Accepted';
           
            update application ;
          //  system.debug('***');
          
          //Commenting the conversion code becuase of new changes in C20.
           // genesis.ConvertApplicationCtrl ctrl = new genesis.ConvertApplicationCtrl();
            // system.debug('***');
         //   String retMsg = ctrl.convertApplication(applicationId);
         
             //system.debug('***');
             //system.debug('***getCLContractFromId(retMsg)'+getCLContractFromId(retMsg));
          //  loan__Loan_Account__c clAccount = getCLContractFromId(retMsg);
            // system.debug('***clAccount'+clAccount);
          //  if(clAccount != null) {
           //     clAccount.loan_vp__Versapay_Token__c = application.Versapay_Token__c;
            //    update clAccount;
           // }
            
           // Account acc = com_loan.CommunityUtil.getAccountFromApplication(applicationId);
           // Contact c = com_loan.communityUtil.getContactFromApplication(applicationId);
           // String userid = createPortalUser(acc, c, acc.com_loan__Portal_Password__c);
         //   System.debug('User id :'+userid);
            return gotoNext();
        } catch(Exception ex) {
            system.debug('Error in versaypay:'+ex);
           // system.debug('getLineNumber'+ex.getLineNumber());
            PageReference pr = new PageReference('/com_loan__error');
            // pr.getParameters().put('error', ex.getMessage());
            pr.getParameters().put('id', applicationId);
            return  pr;
            //new PageReference('/com_loan__error');
        }
    }
    
    global void sendDocument(){ 
        String sessionId = UserInfo.getSessionId();
        DDP_Template__c ddpTemplate = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
        System.debug(LoggingLevel.ERROR, 'Drawloop iD :'+ ddpTemplate);
        try{
            if(ddpTemplate != null){
                System.debug(LoggingLevel.ERROR, 'Deployment id :: ' + ddpTemplate.Deploy_Id__c);
                System.debug(LoggingLevel.ERROR, 'Template id :: ' + ddpTemplate.Template_Id__c);
                Drawloop.generateDDP(ddpTemplate.Deploy_Id__c,ddpTemplate.Template_Id__c,applicationId,sessionId);
                // For time being this is hard coded for attch deployment. But it needs to be DocuSign
                // Drawloop.generateDDP('a62170000004CMD','a63170000004CjB',applicationId,sessionId);
                System.debug(LoggingLevel.ERROR, 'End of Drawloop call');
            }    
            
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
        
    }
    
     public List<SelectOption> getrejectedReasonList() {
       
        List<SelectOption> options = new List<SelectOption>();
        if(com_loan.CommunityUtil.getPicklistValues('genesis__applications__c', 'Rejected_Reason__c') != null){
            options.add(new SelectOption('---Select---','---Select---'));
            List<SelectOption> tempOptions = new List<SelectOption>();
            tempOptions.addAll(com_loan.CommunityUtil.getPicklistValues('genesis__applications__c', 'Rejected_Reason__c'));
            tempOptions.sort();
            options.addAll(tempOptions);
        }
        return options;

    }
    
    global PageReference abortAction() {
        //reject logic goes here
        //com_loan.CommunityUtil.doCleanup(applicationId);
         system.debug('***id');
        genesis__applications__c application = [Select Id,genesis__Status__c,Rejected_Feedback__c,Rejected_Reason__c
                                                    from genesis__applications__c
                                                    where Id =: applicationId limit 1];
                                                    
        if(application!=null){
        try{
        application.Rejected_Feedback__c=rejectedFeedback;
        application.Rejected_Reason__c=rejectedReason;
        application.genesis__Status__c='Offer Rejected';
        update  application;
        
        }
        catch(Exception ex) {
            system.debug('Error in application rejected feedback:'+ex);
            PageReference pr = new PageReference('/com_loan__error'); 
            pr.getParameters().put('id', applicationId);            
            return  pr;
            
        }
        }
      //  system.debug('***id'+application.id);
        return new PageReference('http://lendified.com');
    }
    
    global PageReference validate() {
    
          PageReference pr;  
        try{         
          pr = ValidateApplicationSequence();
          if(pr==null){
           
             genesis__applications__c application = com_loan.CommunityUtil.getApplicationFromId(applicationId);
             if(application.genesis__Status__c.containsIgnoreCase('Reject'))
              pr = new PageReference('http://lendified.com');
             else
             pr=null;
             
          }
          }
          catch(Exception ex) {           
              pr = new PageReference('/com_loan__error');
              pr.getParameters().put('id', applicationId);
            return  pr;
            
        }
        
        return pr;
    }
    
    global PageReference validateOffer() {
        try{
            String applicationId = ApexPages.currentPage().getParameters().get('id');
            genesis__applications__c application = com_loan.CommunityUtil.getApplicationFromId(applicationId);
            // doing for successful offer generation 
            LendScore scoreApi = new LendScore(application);
            String result = scoreApi.lendScoreCalculate();
            
            if(result != null && result.equalsIgnoreCase('SUCCESS.')){
                application.genesis__Status__c ='Offer Generated';
                update application;
                return gotoNext();
            }else{
                application.genesis__Status__c ='Lend Score Failed';
                update application;
                PageReference pr = new PageReference('/ApplicationRejected');
                pr.getParameters().put('id', applicationId);
               // pr.getParameters().put('error', result );
                return  pr;
            }
        }catch(Exception e){
            PageReference pr = new PageReference('/com_loan__error');
            pr.getParameters().put('id', applicationId);
           // pr.getParameters().put('error', e.getMessage());
            return  pr;
        } 
    }
    
    @TestVisible private loan__Loan_Account__c getCLContractFromId(String id) {
        system.debug('getting contract for id:'+id);
        mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
        mfiflexUtil.ObjectCache applicationOC = ec.createObject('TempLoanAccount', 'loan__Loan_Account__c');
        applicationOC.addNamedParameter('id', id);
        applicationOC.setWhereClause(com_loan.CommunityUtil.constructWhere('Id', 'id'));
        String allFields = null;
        applicationOC.addFields(allFields);
        applicationOC.executeQuery();
        List<loan__Loan_Account__c> applications = applicationOC.getRecords();
        if(applications.size() == 0) {
            return null;
        }
        loan__Loan_Account__c application = applications.get(0);
        return application;
    }
    
   /* public String createPortalUser(Account acc, Contact c, String password) {
        try {
            system.debug('Under create user function');
            Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
            //TODO need to check if we need to impart a 1-1 constraint between account and contact.
            mfiflexUtil.ExecutionContext ec = mfiflexUtil.ExecutionContext.getExecContext();
            mfiflexUtil.ObjectCache userObject = ec.createObject('User', 'User', true)
                .addFields('id')           
                .addNamedParameter('email', c.Email)           
                .setWhereClause('Email =:email')
                .setLimitClause('1')
                .executeQuery();
            if(userObject.getRecords().size() == 0) {
                User u = new User(FirstName=c.FirstName, ProfileID = p.Id, Email=c.Email, LastName = c.LastName, 
                                  UserName = c.Email+'.cl.loan', CommunityNickname=c.Email, 
                                  ContactId=c.Id);
                String userId = Site.createPortalUser(u, acc.Id, password);
                system.debug(ApexPages.getMessages());
                return userId;
            } else {
                return null;
            }            
        } catch(Exception e) {
            return null;
        }
    }
    */
}