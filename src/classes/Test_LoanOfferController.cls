@isTest
public class Test_LoanOfferController {
    public static void init(boolean isEmail){
        
        Account acc = new Account();
        acc.Name = 'Ext';
        insert acc;
        
        Contact con;
        if(isEmail){
         con = new contact(firstName='Ext123',lastname='dummy',email='extentor@gmail.com');
        }else{
           
         con = new contact(firstName='Ext123',lastname='dummy');
        }
        
        insert con;
        
        loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c gnAp= genesis.TestHelper.createLoanApplication(); 
        gnAp.Contract_Document_Status__c='Completed';
        gnAp.genesis__Account__c =acc.id;
        gnAp.genesis__Contact__c=con.id;
        //gnAp.Application_Landing_Sequence__c=1;
        update gnAp;
        
        ApexPages.currentPage().getParameters().put('id',gnAp.Id);
        String strAppid = ApexPages.currentPage().getParameters().get('id');
        
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         loan__Fee__c dummyFee1 = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
         dummyFee1.loan__Time_of_charge__c ='Pre-Paid Fees';
         update dummyFee1;                                 
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;       
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;
        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        loan__Product_Pre_Paid_Fees__c PreFee = new loan__Product_Pre_Paid_Fees__c(loan__Fee_Type__c=dummyFee1.id,
                                                                                   loan__Enabled__c =true,
                                                                                   loan__Lending_Product__c=dummyLP.id,
                                                                                   loan__Amortize_Balance__c='Pre-Paid Fees'
                                                                                    );
        insert PreFee ;
        
         loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
       loanAccount.loan__Account__c=acc.id;
        update acc;
        Test.startTest();
        
    /*  com_loan__Application_Wizard__c apwizard = new com_loan__Application_Wizard__c();
        apwizard.page_name__c='com_loan__ApplicantDetails'; 
        apwizard.sequence__c =1;
        apwizard.name='test';
        insert apwizard;
        PageReference currentPage = Page.com_loan__ApplicantDetails;
        Test.setCurrentPage(currentPage);
        ApexPages.currentPage().getParameters().put('id', gnAp.id);
        */
         LoanOfferController objLoanOffer = new LoanOfferController();
           objLoanOffer.applicationId =gnAp.id;
        objLoanOffer.getrejectedReasonList();
        objLoanOffer.abortAction();
        objLoanOffer.continueAction();
        objLoanOffer.validateOffer();
        objLoanOffer.validate();
        objLoanOffer.sendDocument();
       // objLoanOffer.createPortalUser(acc,con,'ext123');
        gnAp.Versapay_Token__c='12312';
        update gnAp;
        objLoanOffer.continueAction();
        objLoanOffer.getCLContractFromId(loanAccount.id);
        Test.stopTest();
        
        
    } 
    public static testmethod void testme1(){
    init(true);
    }
    public static testmethod void testme2(){
    init(false);
    }
}