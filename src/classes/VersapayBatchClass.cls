global class VersapayBatchClass implements Database.Batchable<sObject>,database.allowsCallouts{
    
    String query;
    //DDP_Template__c ddpTemplate;
   // String sessionId;
    String loanStatus = 'Approved';
    
    global VersapayBatchClass() {
        
        // ddpTemplate           = new DDP_Template__c();
        // ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
       //  this.sessionId             = sessionId ;
        
        query = 'Select Id, loan__Loan_Status__c, loan_vp__Versapay_Token__c,'+
        'Application__c,Versapay_Token_Status__c,loan__Account__c,loan__Contact__c,loan__Contact__r.name,loan__Contact__r.email,'+
        'loan__Contact__r.MailingState from loan__Loan_Account__c'+
            ' where loan__Loan_Status__c =: loanStatus AND Contract_Document_Send__c = false'+ 
            ' AND loan_vp__Versapay_Token__c != null and Application__c!=null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<loan__Loan_Account__c> scope) {
        
        try{       
            List<loan__loan_Account__c > loanAccountList= (List<loan__loan_Account__c > ) scope;
            VersapayBatchClassHandler handler = new VersapayBatchClassHandler(loanAccountList);
            String sessionId;
             if(!Test.isRunningTest()){
             sessionId=Login.login('',''); // now passgin blank values because username and password is hardcoded in login class
            // system.debug('***'+sessionId);
            }
            else{
             sessionId='test';
            }
            //*/
            if(sessionId!=null)
                handler.checkVersapayTokenStatus(sessionId);
            else{           
                loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
                log.loan__Message__c = 'Session id is not generated';
                log.loan__Date__c = System.today();                    
                insert log; 
            }
        }catch(Exception e){
            //Database.rollback(sp);
            System.debug('Versapay tokecn check and document send failed: '+e.getCause()+' '+e.getMessage());
            System.debug(e.getStackTraceString());              
            loan__Batch_Process_Log__c log = new loan__Batch_Process_Log__c();
            log.loan__Message__c = 'Versapay tokecn check and document send failed: '+e.getCause()+' '+e.getMessage();
            log.loan__Date__c = System.today();                    
            insert log;  
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //Id batchJobId = Database.executeBatch(new DocusignBatchClass(), 1);
    }
    
}