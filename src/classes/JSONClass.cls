public with sharing class JSONClass {
    public JSONClass() {
        
    }
    
    public static JSONGenerator generateJSON (genesis__Applications__c app) {

        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator loanAppGenerator = JSON.createGenerator(true);

        // open the JSON generator
        loanAppGenerator.writeStartObject();

        // Create an object to write to the JSON string for Address.
        address personaddress = new address(app.genesis__Contact__r.mailingstreet,app.genesis__Contact__r.mailingcity,app.genesis__Contact__r.mailingstate,app.genesis__Contact__r.mailingpostalcode,app.genesis__Contact__r.mailingcountry);
        address businessaddress = new address(app.com_loan__Street_Address__c,app.com_loan__City__c,app.com_loan__State__c,app.com_loan__Postal__c, 'Canada');
        
        // Write data to the JSON string.
        loanAppGenerator.writeStringField('loanApplicationId', app.Name != NULL ? app.Name : '');
        loanAppGenerator.writeStringField('institutionNumber', '9000'); // 9000 is constant 
        loanAppGenerator.writeStringField('salesforceId', app.id);
        loanAppGenerator.writeFieldName('person');
        loanAppGenerator.writeStartObject();
            loanAppGenerator.writeStringField('UserName', app.genesis__Contact__r.email);
            loanAppGenerator.writeStringField('FirstName', app.genesis__Contact__r.firstname != NULL ? app.genesis__Contact__r.firstname : '');
            loanAppGenerator.writeStringField('MiddleName', '');
            loanAppGenerator.writeStringField('LastName', app.genesis__Contact__r.lastname);
            loanAppGenerator.writeStringField('DateOfBirth', LendifiedUtil.formatDateCreditEngine(LendifiedUtil.convertDateFormat(app.genesis__Contact__r.birthdate)));
            loanAppGenerator.writeStringField('WorkEmail', app.genesis__Contact__r.email);
            loanAppGenerator.writeFieldName('PersonAddress');
            loanAppGenerator.writeObject(personaddress);
        loanAppGenerator.writeEndObject();
        loanAppGenerator.writeFieldName('business');
        loanAppGenerator.writeStartObject();
            loanAppGenerator.writeStringField('businessNumber', app.com_loan__Business_Number__c != NULL ? app.com_loan__Business_Number__c : '');
            loanAppGenerator.writeStringField('name', app.com_loan__Business_Name__c);
            loanAppGenerator.writeStringField('legalName', app.com_loan__Business_Name__c);
            loanAppGenerator.writeFieldName('BusinessAddress');
            loanAppGenerator.writeObject(businessaddress);
            loanAppGenerator.writeStringField('phone', app.genesis__Contact__r.mobilephone);
            loanAppGenerator.writeObjectField('industry', app.com_loan__Industry__c != NULL ? app.com_loan__Industry__c : '');
            loanAppGenerator.writeNumberField('numberEmployees', 0);
            loanAppGenerator.writeBooleanField('doesOwnFacilities', false);                             // Hardcoded currently
        loanAppGenerator.writeEndObject();
        loanAppGenerator.writeFieldName('loanRequest');
        loanAppGenerator.writeStartObject();
            loanAppGenerator.writeNumberField('requestedLoanAmount', app.genesis__Loan_Amount__c);
            loanAppGenerator.writeStringField('amortizationTermName', String.valueOf(app.genesis__Term__c));
            loanAppGenerator.writeStringField('amortizationTermPeriod', '');
            loanAppGenerator.writeNumberField('amortizationTermPeriodCount', 0);
            loanAppGenerator.writeStringField('paymentFrequencyName', 'Bi-weekly');
            loanAppGenerator.writeStringField('paymentFrequencyPeriod', 'Week');
            loanAppGenerator.writeNumberField('paymentFrequencyPeriodCount', 1);
            loanAppGenerator.writeStringField('purpose', app.com_loan__Loan_Purpose__c);
            loanAppGenerator.writeStringField('purposeAdditionalExplanation', '');
            loanAppGenerator.writeStringField('submittedDate', LendifiedUtil.formatDateCreditEngine(LendifiedUtil.convertDateFormat(app.genesis__Expected_Close_Date__c)));
            loanAppGenerator.writeStringField('appliedDate', LendifiedUtil.formatDateCreditEngine(system.now()));
        loanAppGenerator.writeEndObject();
        loanAppGenerator.writeFieldName('loanAdjudication');
        loanAppGenerator.writeStartObject();
            loanAppGenerator.writeNumberField('loanAmount', 0.0);
            loanAppGenerator.writeStringField('creditRating', '');
            loanAppGenerator.writeNumberField('interestRate', 0.0);
            loanAppGenerator.writeNumberField('maxApprovedLoanAmount', 0.0);
            loanAppGenerator.writeNumberField('paymentAmount', 0.0);
            loanAppGenerator.writeNumberField('processingBaseAmount', 0.0);
            loanAppGenerator.writeNumberField('processingFee', 0.0);
            //loanAppGenerator.writeStringField('bniScore', app.BNI__c != NULL ? String.valueOf(app.BNI__c) : '');
            loanAppGenerator.writeNumberField('bniScore', app.BNI__c != NULL ? app.BNI__c : 0.0);
            //loanAppGenerator.writeStringField('beaconScore', app.Beacon_9_Score__c != NULL ? String.valueOf(app.Beacon_9_Score__c) : '');
            loanAppGenerator.writeNumberField('beaconScore', app.Beacon_9_Score__c != NULL ? app.Beacon_9_Score__c : 0.0);
            loanAppGenerator.writeNumberField('transunionScore', 0);
            loanAppGenerator.writeStringField('loanAdjudicationStatus', '');
            loanAppGenerator.writeStringField('loanAdjudicationErrorMessage', '');
        loanAppGenerator.writeEndObject();

        return loanAppGenerator;
    }

    public class address { 
        String street;
        String city;
        String province;
        String postalcode;
        String country;
        public address(String streetString, String cityString, String provinceString, String postalcodeString, String countryString) { 
            street      = streetString      != NULL ? streetString : '';
            city        = cityString        != NULL ? cityString : '';
            province    = provinceString    != NULL ? provinceString : '';
            postalcode  = postalcodeString  != NULL ? postalcodeString : '';
            country     = countryString     != NULL ? countryString : 'Canada';
        }
    }
}