public without sharing class DailyInterestAccrualHandler {

    List<loan__loan_Account__c> scope;

    public DailyInterestAccrualHandler(List<loan__loan_Account__c> scope) {
        this.scope = scope;
    }

    public void processLoanAccounts() {
        List<String> monthNames = new List<String>{'','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'};
		Date startDate = Date.valueOf('2015-01-01');
		Date endDate = Date.valueOf('2015-12-31');
		
		// loan payments
		List<loan__Loan_Payment_Transaction__c> lpts = [select Id,
															loan__Loan_Account__c,
		                                                	loan__Transaction_Date__c,
		                                                	loan__Principal__c,
		                                                	loan__Interest__c,
		                                                	loan__Fees__c
		                                                from loan__Loan_Payment_Transaction__c
		                                                where loan__Loan_Account__c =: scope
		                                                and loan__Cleared__c = true
		                                                and loan__Reversed__c = false
		                                                and loan__Rejected__c = false];
		
		// put payments into date map
		Map<String,List<loan__Loan_Payment_Transaction__c>> lptMap = new Map<String,List<loan__Loan_Payment_Transaction__c>>();
		List<loan__Loan_Payment_Transaction__c> tmpPaymentList;
		for (loan__Loan_Payment_Transaction__c lpt : lpts) {
			String dateStr = lpt.loan__Transaction_Date__c.year() +'-'+lpt.loan__Transaction_Date__c.month()+'-'+lpt.loan__Transaction_Date__c.day();
			if (lptMap.containsKey(lpt.loan__Loan_Account__c+'_'+dateStr)) {
				tmpPaymentList = lptMap.get(lpt.loan__Loan_Account__c+'_'+dateStr);
				tmpPaymentList.add(lpt);
				lptMap.put(lpt.loan__Loan_Account__c+'_'+dateStr,tmpPaymentList);
			} else {
				lptMap.put(lpt.loan__Loan_Account__c+'_'+dateStr,new List<loan__Loan_Payment_Transaction__c>{lpt});
			}
		}
		
		Map<String,Double> dailyInterestMap;
		loan__Loan_Payment_Transaction__c lpt;
		
		Date tempDate;
		Integer dateDiff = startDate.daysBetween(endDate);
		String dateStr,prevDateStr,dateStrFull,dateMonthStr,prevDateMonthStr;
		
		// get existing Daily Interest Accrual records to be updated
		Map<String,Daily_Interest_Accrual__c> dailyInterestAccrualsMap = new Map<String,Daily_Interest_Accrual__c>();
		Daily_Interest_Accrual__c dailyInterestAccrual;
		for (Daily_Interest_Accrual__c diaRecord : [select Id,CL_Contract__c,Year__c from Daily_Interest_Accrual__c where CL_Contract__c =: scope]) {
			dailyInterestAccrualsMap.put(diaRecord.CL_Contract__c+'_'+diaRecord.Year__c,diaRecord);
		}
		
		
		
		
		dateStr = null;
		prevDateStr = null;
		dateMonthStr = null;
		prevDateMonthStr = null;
		for (loan__Loan_Account__c clContract : scope) {
			Date currentDate = clContract.loan__Expected_Disbursal_Date__c;
			
			Double principal = clContract.loan__Loan_Amount__c;
			Double dailyInterestRate = clContract.loan__Contractual_Interest_Rate__c / 365 / 100;
			Double interest = 0;
			Double charges = 0;
			Double totalAmt = 0;
			Double dailyInterest = 0;
			Double totalInterest = 0;
			Double dailyInterestSum = 0;
			Double monthTotal = 0;
			Date lastPaymentDate = clContract.loan__Expected_Disbursal_Date__c;
			
			startDate = clContract.loan__Expected_Disbursal_Date__c;
			if (startDate == null) {
				continue;
			}
			if (clContract.loan__Closed_Date__c != null) {
				endDate = clContract.loan__Closed_Date__c;
			} else if (clContract.loan__Charged_Off_Date__c != null) {
				endDate = clContract.loan__Charged_Off_Date__c;
			} else {
				endDate = Date.today();
			}
			dateDiff = startDate.daysBetween(endDate);
			
			for(Integer i=0;i<=dateDiff;i++) {
				tempDate = startDate + i;
			    dateStrFull = tempDate.year() +'-'+tempDate.month()+'-'+tempDate.day();
			    dateStr = tempDate.year() +'-'+tempDate.month()+'-'+tempDate.day();
			    dateMonthStr = tempDate.year() +'-'+tempDate.month();
				
				if (dailyInterestAccrualsMap.containsKey(clContract.id+'_'+tempDate.year())) {
					dailyInterestAccrual = dailyInterestAccrualsMap.get(clContract.id+'_'+tempDate.year());
				} else {
					dailyInterestAccrual = new Daily_Interest_Accrual__c();
					dailyInterestAccrual.Year__c = tempDate.year()+'';
					dailyInterestAccrual.CL_Contract__c = clContract.id;
					dailyInterestAccrualsMap.put(clContract.id+'_'+tempDate.year(),dailyInterestAccrual);
				}
				
				if (tempDate >= currentDate 
						&& (clContract.loan__Charged_Off_Date__c == null || (tempDate < clContract.loan__Charged_Off_Date__c))
						&& (clContract.loan__Closed_Date__c == null || (tempDate < clContract.loan__Closed_Date__c))) {
			    
				    if (lptMap.containsKey(clContract.Id+'_'+dateStrFull)) {
				    	tmpPaymentList = lptMap.get(clContract.Id+'_'+dateStrFull);
				    	for (loan__Loan_Payment_Transaction__c tempLPT : tmpPaymentList) {
					    
				            principal -= tempLPT.loan__Principal__c;
				            interest -= tempLPT.loan__Interest__c;
				            //charges -= tempLPT.loan__Fees__c;
				            
				            lastPaymentDate = tempLPT.loan__Transaction_Date__c;
				    	}
				    }
				    
				    totalAmt = principal + interest;
				    dailyInterest = principal * dailyInterestRate;
				    interest += dailyInterest;
				    totalInterest += dailyInterest;
				    
				    //System.debug('daily: '+Math.round(dailyInterest * 100.0) / 100.0);
				    
				    // round daily interest for display
					if (dateStr == prevDateStr) {
				    	dailyInterestSum += Math.round(dailyInterest * 100.0) / 100.0;
					} else {
						dailyInterestSum = Math.round(dailyInterest * 100.0) / 100.0;
					}
				} else {
					dailyInterestSum = 0;
				}
				if (dateMonthStr == prevDateMonthStr) {
					monthTotal += dailyInterestSum;
				} else {
					monthTotal = dailyInterestSum;
				}
				
			    dailyInterestAccrual.put(monthNames.get(tempDate.month())+'_'+tempDate.day()+'__c',Math.round( dailyInterestSum * 100.0) / 100.0);
			    dailyInterestAccrual.put(monthNames.get(tempDate.month())+'__c',Math.round( monthTotal * 100.0) / 100.0);
			    
			    // if last month and period is closed,
			    // clear balance and aging as the loan is closed before month end
			    if (i+1>dateDiff && endDate != Date.today()) {
			    
				    dailyInterestAccrual.put('Balance_'+monthNames.get(tempDate.month())+'__c',null);
				    dailyInterestAccrual.put('Aging_'+monthNames.get(tempDate.month())+'__c',null);
				    
			    } else {
					dailyInterestAccrual.put('Balance_'+monthNames.get(tempDate.month())+'__c',Math.round( principal * 100.0) / 100.0);
				    
				    if (lastPaymentDate == null) {
				    	dailyInterestAccrual.put('Aging_'+monthNames.get(tempDate.month())+'__c',0);
				    } else {
				    	dailyInterestAccrual.put('Aging_'+monthNames.get(tempDate.month())+'__c',lastPaymentDate.daysBetween(tempDate));
				    }
				}
		    	
			    
			    prevDateStr = dateStr;
			    prevDateMonthStr = dateMonthStr;
			}
			//System.debug('Total interest: '+totalInterest);
			
		}
		
		// delete pre-existing Daily Interest Accrual records
		/*
		List<Daily_Interest_Accrual__c> dailyInterestAccrualsToDelete = [select Id from Daily_Interest_Accrual__c where CL_Contract__c =: clContracts];
		if (dailyInterestAccrualsToDelete.size() > 0) {
			delete dailyInterestAccrualsToDelete;
		}
		*/
		
		if (dailyInterestAccrualsMap.values().size() > 0) {
			upsert dailyInterestAccrualsMap.values();
		}
		
    }

}