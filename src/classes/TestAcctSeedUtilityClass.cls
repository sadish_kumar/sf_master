@isTest
private class TestAcctSeedUtilityClass {
    
    private static void createDummyData(){
        /*
        Date todayDate = Date.today();
        loan__Fiscal_Year__c fiscalYear = new loan__Fiscal_Year__c(loan__Start_Date__c=Date.newInstance(todayDate.year(),1,1),loan__End_Date__c=Date.newInstance(todayDate.year(),12,31),loan__Status__c='Open');
        insert fiscalYear;
        
        genesis__Applications__c app = new genesis__Applications__c(); 
        app.Contract_Document_Status__c='Completed';
        insert app;
        
        loan__Currency__c curr = new loan__Currency__c();
        insert curr;
        
        //Create a dummy MF_Account
        loan__MF_Account_Type__c dummyAccountType = new loan__MF_Account_Type__c(Name='10000 - ASSETS');
        insert dummyAccountType;
        loan__MF_Account__c dummyAccount = new loan__MF_Account__c(Name='XXXAccountForTest',loan__Description__c='10000 - ASSETS',loan__Is_Master__c=true,loan__Account_Type__c=dummyAccountType.Id);
        loan__MF_Account__c dummyIncAccount = new loan__MF_Account__c(Name='XXXIncAccountForTest',loan__Description__c='30000 - INCOME');
        insert new List<loan__MF_Account__c>{dummyAccount,dummyIncAccount};
        
        //Create a Fee Set
        loan__Fee__c dummyFee = new loan__Fee__c(loan__Currency__c=curr.Id,loan__Income_Account__c=dummyIncAccount.Id,loan__Amount__c=10);// ,dummyAccount);  
        loan__Fee__c dummyFee1 = new loan__Fee__c(loan__Currency__c=curr.Id,loan__Income_Account__c=dummyIncAccount.Id,loan__Amount__c=10);// ,dummyAccount);  
        dummyFee1.loan__Time_of_charge__c ='Pre-Paid Fees';
        insert new List<loan__Fee__c>{dummyFee,dummyFee1};   
                                      
        loan__Fee_Set__c dummyFeeSet = new loan__Fee_Set__c();
        insert dummyFeeSet;
        
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        
        //loan__Fee_Junction__c dummyFeeJunction = new loan__Fee_Junction__c(loan__Fee__c=dummyFee.Id,loan__Fee_Set__c=dummyFeeSet.Id);
        //insert dummyFeeJunction;
        
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;      
        
        
        loan__Bank__c dummyBank = new loan__Bank__c();
        insert dummyBank;
        
        loan__Office_Type__c dummyOfficeType = new loan__Office_Type__c();
        dummyOfficeType.loan__Office_Type_Short_Code__c = 'HO';
        insert dummyOfficeType;
        
        loan__Branch_Location__c dummyBranchLocation = new loan__Branch_Location__c();
        dummyBranchLocation.loan__Address_Line_1__c = '123 Main St';
        dummyBranchLocation.loan__State__c = 'ON';
        insert dummyBranchLocation;
        
        loan__Office_Name__c dummyOffice = new loan__Office_Name__c();
        dummyOffice.loan__Bank_Account_Checks__c = dummyAccount.Id;
        dummyOffice.loan__Check_Account__c = dummyAccount.Id;
        dummyOffice.loan__Cash_Account__c = dummyAccount.Id;
        dummyOffice.loan__Inward_Check_Clearance_Account__c = dummyAccount.Id;
        dummyOffice.loan__Outward_Check_Clearance_Account__c = dummyAccount.Id;
        dummyOffice.loan__Profit_and_Loss_Account__c = dummyAccount.Id;
        dummyOffice.loan__Mark_Suspended_After_Days__c = 90;
        dummyOffice.loan__Write_Off_Suggestion_Days__c = 10;
        dummyOffice.loan__Office_Short_Name__c = 'TO';
        dummyOffice.loan__Days_in_advance_to_create_Collection_Rep__c = 7;
        dummyOffice.RecordTypeId = Schema.SObjectType.loan__Office_Name__c.getRecordTypeInfosByName().get('Root Branch').getRecordTypeId();
        dummyOffice.loan__Is_Branch__c = true;
        dummyOffice.loan__Branch_s_Bank__c = dummyBank.Id;
        dummyOffice.loan__Office_Type__c = dummyOfficeType.Id;
        dummyOffice.loan__Office_Location__c = dummyBranchLocation.Id;
        insert dummyOffice;
        
        */
        /*
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = new loan__Loan_Product__c();
        //dummyLP. = dummyOffice;
        dummyLP.loan__Product_Excess_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_GL_Code_Interest__c = dummyAccount.Id;
        dummyLP.loan__Product_GL_Code_Principal__c = dummyAccount.Id;
        dummyLP.loan__Product_Interest_Amortization_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Interest_Income_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Int_On_Overdue_Income_Acc__c = dummyAccount.Id;
        dummyLP.loan__Product_Loan_Control_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Loan_Loss_Provision_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Loan_Loss_Reserve_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Overdue_Interest_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Suspended_Interest_Account__c = dummyAccount.Id;
        dummyLP.loan__Product_Suspended_Int_On_Overdue_Acc__c = dummyAccount.Id;
        dummyLP.loan__Product_Write_Off_Recovery_Account__c = dummyAccount.Id;
        dummyLP.loan__Currency__c = curr.Id;
        dummyLP.loan__Fee_Set__c = dummyFeeSet.Id;
        dummyLP.loan__Payment_Application_Mode__c = 'Current Dues';
        insert dummyLP;
        
        loan__Loan_Purpose__c dummyLoanPurpose = new loan__Loan_Purpose__c();        
        insert dummyLoanPurpose;
        
        loan__Client__c dummyClient = new loan__Client__c(loan__Office__c=dummyOffice.Id);
        insert dummyClient;
        
        loan__Product_Pre_Paid_Fees__c preFee = new loan__Product_Pre_Paid_Fees__c(loan__Fee_Type__c=dummyFee1.id,
                                                                                   loan__Enabled__c =true,
                                                                                   loan__Lending_Product__c=dummyLP.id,
                                                                                   loan__Amortize_Balance__c='Pre-Paid Fees'
                                                                                    );
        insert preFee ;
        */
        
        
        
        
        loan.TestHelper.createSeedDataForTesting();
        genesis__Applications__c gnAp= genesis.TestHelper.createLoanApplication(); 
        gnAp.Contract_Document_Status__c='Completed';
        update gnAp;
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
        loan__Fee__c dummyFee1 = loan.TestHelper.createFee(curr,dummyIncAccount ,dummyAccount);  
        dummyFee1.loan__Time_of_charge__c ='Pre-Paid Fees';
        update dummyFee1;                                 
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        dummyFee.loan__Amount__c=2;
        update dummyFee;
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        Lendified_Parameters__c param = new Lendified_Parameters__c();
        param.Pre_Paid_Fee__c=dummyFee.name;
        param.name='Test';
        insert param;       
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        
        //Create a dummy Loan Product
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProduct(dummyOffice,
                            dummyAccount, 
                            curr, 
                            dummyFeeSet);
        dummyLP.loan__Payment_Application_Mode__c = loan.LoanConstants.LOAN_PAYMENT_APPLICATION_CURRENT_DUES;
        
        update dummyLP;
        //System.debug(LoggingLevel.ERROR,'LoanProduct in test: ' + dummyLP.Disable_Reserve_Amount_for_Next_Due__c);
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();        
        
        loan__Client__c dummyClient = loan.TestHelper.createClient(dummyOffice);
        
        /*
        loan__Product_Pre_Paid_Fees__c PreFee = new loan__Product_Pre_Paid_Fees__c(loan__Fee_Type__c=dummyFee1.id,
                                                                                   loan__Enabled__c =true,
                                                                                   loan__Lending_Product__c=dummyLP.id,
                                                                                   loan__Amortize_Balance__c='Pre-Paid Fees'
                                                                                    );
        insert PreFee ;
        */
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        Date todayDate = Date.today();
        String dateStr = String.valueOf(journalEntryDate.year());
        if (String.valueof(journalEntryDate.month()).length() == 1) {
            dateStr += '-0'+journalEntryDate.month();
        } else {
            dateStr += '-'+journalEntryDate.month();
        }
        
        loan__Loan_Account__c loanAccount = loan.TestHelper.createLoanAccount(dummyLP,
                                                    dummyClient,
                                                    dummyFeeSet,
                                                    dummyLoanPurpose,
                                                    dummyOffice);
        
        loanAccount.loan_vp__Versapay_Token__c = '8TNJ5BFZBAZW';
        loanAccount.loan__Loan_Status__c = 'Active - Good Standing';
        loanAccount.Application__c=gnAp.id;
        //loanAccount.Origination_Fee__c=loanAccount.loan__Loan_Amount__c*0.02;
        loanAccount.Origination_Fee__c = 1000.0;
        loanAccount.loan__Expected_Disbursal_Date__c = Date.today();
        loanAccount.loan__Last_Installment_Date__c = journalEntryDate.addYears(1);
        update loanAccount;
        
        loan_vp__Versapay_Parameters__c params = new loan_vp__Versapay_Parameters__c();
        params.loan_vp__Versapay_Username__c = 'dummyUserName';
        params.loan_vp__Versapay_Password__c = 'dummyPassword';
        
        AcctSeed__Accounting_Period__c acctPeriod = new AcctSeed__Accounting_Period__c();
        acctPeriod.Name = dateStr;
        acctPeriod.AcctSeed__Start_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1);
        acctPeriod.AcctSeed__End_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month()+1,1).addDays(-1);
        acctPeriod.AcctSeed__Status__c = 'Open';
        insert acctPeriod;
        
        dateStr = String.valueOf(todayDate.year());
        if (String.valueof(todayDate.month()).length() == 1) {
            dateStr += '-0'+todayDate.month();
        } else {
            dateStr += '-'+todayDate.month();
        }
        
        acctPeriod = new AcctSeed__Accounting_Period__c();
        acctPeriod.Name = dateStr;
        acctPeriod.AcctSeed__Start_Date__c = Date.newInstance(todayDate.year(),todayDate.month(),1);
        acctPeriod.AcctSeed__End_Date__c = Date.newInstance(todayDate.year(),todayDate.month()+1,1).addDays(-1);
        acctPeriod.AcctSeed__Status__c = 'Open';
        insert acctPeriod;
        
        Date nextMonthDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(1);
        
        dateStr = String.valueOf(nextMonthDate.year());
        if (String.valueof(nextMonthDate.month()).length() == 1) {
            dateStr += '-0'+nextMonthDate.month();
        } else {
            dateStr += '-'+nextMonthDate.month();
        }
        
        acctPeriod = new AcctSeed__Accounting_Period__c();
        acctPeriod.Name = dateStr;
        acctPeriod.AcctSeed__Start_Date__c = Date.newInstance(nextMonthDate.year(),nextMonthDate.month(),1);
        acctPeriod.AcctSeed__End_Date__c = Date.newInstance(nextMonthDate.year(),nextMonthDate.month()+1,1).addDays(-1);
        acctPeriod.AcctSeed__Status__c = 'Open';
        insert acctPeriod;
        
        
        AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();
        glAccount.Name = '1202-10 Loans Receivable - Commercial';
        glAccount.AcctSeed__Active__c = true;
        glAccount.AcctSeed__Type__c = 'Balance Sheet';
        glAccount.AcctSeed__Sub_Type_1__c = 'Assets';
        glAccount.AcctSeed__Sub_Type_2__c = 'Other current assets';
        glAccount.AcctSeed__Billing_Description__c = '1202-10 Loans Receivable - Commercial';
        insert glAccount;
        
        loan_seed__Lending_Product_Accounting_Seed__c lendingProductAcctSeed = new loan_seed__Lending_Product_Accounting_Seed__c();
        lendingProductAcctSeed.loan_seed__Principal_Receivable_GLA__c = glAccount.Id; // 1202-10 Loans Receivable - Commercial
        lendingProductAcctSeed.loan_seed__Interest_Receivable_GLA__c = glAccount.Id; // 1215-10 Interest Receivable - Commercial Loans
        lendingProductAcctSeed.loan_seed__Interest_Income_GLA__c = glAccount.Id; 
        lendingProductAcctSeed.loan_seed__Prepaid_Fee_GLA__c = glAccount.Id; // 4020-10 Origination Fee Income
        lendingProductAcctSeed.loan_seed__Prepaid_Fee_Unearned_GLA__c = glAccount.Id; // 2180-10 Unearned Revenue
        lendingProductAcctSeed.loan_seed__Loan_Loss_Reserve_GLA__c = glAccount.Id; // 5250-10 Bad Debt Expense
        lendingProductAcctSeed.loan_seed__Service_Fee_GLA__c = glAccount.Id; // 4025-10 Dishonour Fee, Income
        lendingProductAcctSeed.Disbursal_Bank_GLA__c = glAccount.Id; // 1100-10 VersaPay Account
        lendingProductAcctSeed.Payment_Bank_GLA__c = glAccount.Id; // 1000-10 CIBC - General Account
        lendingProductAcctSeed.Service_Fee_Receivable_GLA__c = glAccount.Id; 
        lendingProductAcctSeed.Uncategorized_Income_GLA__c = glAccount.Id; 
        lendingProductAcctSeed.loan_seed__Unearned_Income_GLA__c = glAccount.Id; 
        lendingProductAcctSeed.Last_Daily_Interest_Journal_Entry__c = Date.newInstance(todayDate.year(),todayDate.month(),todayDate.day()).addDays(-1); 
        insert lendingProductAcctSeed;
        
        
        loan__Loan_Disbursal_Transaction__c ldt = new loan__Loan_Disbursal_Transaction__c();
        ldt.loan__Loan_Account__c = loanAccount.Id;
        ldt.loan__Cleared__c = true;
        ldt.loan__Reversed__c = false;
        ldt.loan_vp__Sent_To_Versapay__c = true;
        ldt.loan__Disbursal_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1);
        ldt.loan__Disbursed_Amt__c = 10000;
        ldt.loan__Financed_Amount__c = 9000;
        insert ldt;
    }
    
    private testMethod static void testAcctSeedDisbursalJournalEntries1(){
        testAcctSeedDisbursalJournalEntries(true);
    }
    private testMethod static void testAcctSeedDisbursalJournalEntries2(){
        testAcctSeedDisbursalJournalEntries(false);
    }
    
    private static void testAcctSeedDisbursalJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedDisbursalJournalEntries job = new AcctSeedDisbursalJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    private testMethod static void testAcctSeedDisbursalReversalJournalEntries1(){
        testAcctSeedDisbursalReversalJournalEntries(true);
    }
    private testMethod static void testAcctSeedDisbursalReversalJournalEntries2(){
        testAcctSeedDisbursalReversalJournalEntries(false);
    }
    
    private static void testAcctSeedDisbursalReversalJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Disbursal_Transaction__c ldt = [select Id,loan__Loan_Account__c,loan__Disbursal_Date__c from loan__Loan_Disbursal_Transaction__c];
        
        loan__Disbursal_Adjustment__c ldtReversed = new loan__Disbursal_Adjustment__c();
        ldtReversed.loan__Adjustment_Txn_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1);
        ldtReversed.loan__Cleared__c = true;
        ldtReversed.loan__Loan_Disbursal_Transaction__c = ldt.Id;
        insert ldtReversed;
        
        AcctSeed__Journal_Entry__c journalEntry = new AcctSeed__Journal_Entry__c();
        journalEntry.Name = 'Test Journal Entry';
        journalEntry.CL_Contract__c = ldt.loan__Loan_Account__c;
        journalEntry.AcctSeed__Journal_Date__c = ldt.loan__Disbursal_Date__c;
        journalEntry.AcctSeed__Status__c = 'Approved';
        insert journalEntry;
        
        ldt.loan_seed__Journal_Entry__c = journalEntry.Id;
        update ldt;
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedDisbursalReversalJournalEntries job = new AcctSeedDisbursalReversalJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(2,journalEntries.size());
        }
    }
    
    
    
    private testMethod static void testAcctSeedPaymentJournalEntries1(){
        testAcctSeedPaymentJournalEntries(true);
    }
    private testMethod static void testAcctSeedPaymentJournalEntries2(){
        testAcctSeedPaymentJournalEntries(false);
    }
    
    private static void testAcctSeedPaymentJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        
        loan__Loan_Payment_Transaction__c lpt = new loan__Loan_Payment_Transaction__c();
        lpt.loan__Loan_Account__c = loanAccount.Id;
        lpt.loan__Transaction_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        lpt.loan__Cleared__c = true;
        lpt.loan_vp__Sent_To_Versapay__c = true;
        lpt.loan__Transaction_Amount__c = 1020;
        lpt.loan__Principal__c = 1000;
        lpt.loan__Interest__c = 20;
        lpt.loan__Fees__c = 0;
        insert lpt;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedPaymentJournalEntries job = new AcctSeedPaymentJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    
    private testMethod static void testAcctSeedPaymentReversalJournalEntries1(){
        testAcctSeedPaymentReversalJournalEntries(true);
    }
    private testMethod static void testAcctSeedPaymentReversalJournalEntries2(){
        testAcctSeedPaymentReversalJournalEntries(false);
    }
    
    private static void testAcctSeedPaymentReversalJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        
        loan__Loan_Payment_Transaction__c lpt = new loan__Loan_Payment_Transaction__c();
        lpt.loan__Loan_Account__c = loanAccount.Id;
        lpt.loan__Transaction_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        lpt.loan__Cleared__c = true;
        lpt.loan_vp__Sent_To_Versapay__c = true;
        lpt.loan__Transaction_Amount__c = 1020;
        lpt.loan__Principal__c = 1000;
        lpt.loan__Interest__c = 20;
        lpt.loan__Fees__c = 0;
        insert lpt;
        
        loan__Repayment_Transaction_Adjustment__c lptReversed = new loan__Repayment_Transaction_Adjustment__c();
        lptReversed.loan__Adjustment_Txn_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        lptReversed.loan__Cleared__c = true;
        lptReversed.loan__Loan_Payment_Transaction__c = lpt.Id;
        insert lptReversed;
        
        AcctSeed__Journal_Entry__c journalEntry = new AcctSeed__Journal_Entry__c();
        journalEntry.Name = 'Test Journal Entry';
        journalEntry.CL_Contract__c = lpt.loan__Loan_Account__c;
        journalEntry.AcctSeed__Journal_Date__c = lpt.loan__Transaction_Date__c;
        journalEntry.AcctSeed__Status__c = 'Approved';
        insert journalEntry;
        
        lpt.loan__Reversed__c = true;
        lpt.loan_seed__Journal_Entry__c = journalEntry.Id;
        update lpt;
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedPaymentReversalJournalEntries job = new AcctSeedPaymentReversalJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(2,journalEntries.size());
        }
    }
    
    private testMethod static void testAcctSeedDailyInterestJournalEntries1(){
        testAcctSeedDailyInterestJournalEntries(true);
    }
    private testMethod static void testAcctSeedDailyInterestJournalEntries2(){
        testAcctSeedDailyInterestJournalEntries(false);
    }
    
    private static void testAcctSeedDailyInterestJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),Date.today().day());
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        
        List<String> monthNames = new List<String>{'','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'};
        
        Daily_Interest_Accrual__c dailyInterest = new Daily_Interest_Accrual__c();
        dailyInterest.Year__c = String.valueOf(journalEntryDate.year());
        dailyInterest.CL_Contract__c = loanAccount.Id;
        dailyInterest.put(monthNames.get(journalEntryDate.month())+'_'+journalEntryDate.day()+'__c',100);
        insert dailyInterest;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedDailyInterestJournalEntries job = new AcctSeedDailyInterestJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    private testMethod static void testAcctSeedOriginationFeeJournalEntries1(){
        testAcctSeedOriginationFeeJournalEntries(true);
    }
    private testMethod static void testAcctSeedOriginationFeeJournalEntries2(){
        testAcctSeedOriginationFeeJournalEntries(false);
    }
    
    private static void testAcctSeedOriginationFeeJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        loanAccount.loan__Charged_Off_Date__c = Date.today().addDays(14);
        update loanAccount;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedOriginationFeeJournalEntries job = new AcctSeedOriginationFeeJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    private testMethod static void testAcctSeedWriteOffJournalEntries1(){
        testAcctSeedWriteOffJournalEntries(true);
    }
    private testMethod static void testAcctSeedWriteOffJournalEntries2(){
        testAcctSeedWriteOffJournalEntries(false);
    }
    
    private static void testAcctSeedWriteOffJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        loanAccount.loan__Charged_Off_Date__c = journalEntryDate.addDays(14);
        loanAccount.loan__Charged_Off_Fees__c = 50;
        loanAccount.loan__Charged_Off_Interest__c = 100;
        loanAccount.loan__Charged_Off_Principal__c = 1000;
        update loanAccount;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedWriteOffJournalEntries job = new AcctSeedWriteOffJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    
    
    private testMethod static void testAcctSeedFeeChargeJournalEntries1(){
        testAcctSeedFeeChargeJournalEntries(true);
    }
    private testMethod static void testAcctSeedFeeChargeJournalEntries2(){
        testAcctSeedFeeChargeJournalEntries(false);
    }
    
    private static void testAcctSeedFeeChargeJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        
        loan__Currency__c currencyRecord = [select Id from loan__Currency__c limit 1];
        
        loan__Fee__c feeRecord = new loan__Fee__c();
        feeRecord.Name = 'Origination Fee';
        feeRecord.loan__Currency__c = currencyRecord.Id;
        feeRecord.loan__State__c = 'Active';
        feeRecord.loan__Fee_Category__c = 'Loan';
        feeRecord.loan__Time_of_charge__c = 'Late Fees';
        feeRecord.loan__Fee_Calculation_Method__c = 'Fixed';
        feeRecord.loan__Amount__c = 50;
        //feeRecord.loan__Income_Account__c = 
        //feeRecord. loan__Suspended_Fee_Account__c = 
        insert feeRecord;
        
        loan__Charge__c chargeFee = new loan__Charge__c();
        chargeFee.loan__Loan_Account__c = loanAccount.Id;
        chargeFee.loan__Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        chargeFee.loan__Original_Amount__c = 50;
        chargeFee.loan__Fee__c = feeRecord.Id;
        chargeFee.loan__Waive__c = false;
        insert chargeFee;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedFeeChargeJournalEntries job = new AcctSeedFeeChargeJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(1,journalEntries.size());
        }
    }
    
    
    
    private testMethod static void testAcctSeedWaivedFeeChargeJournalEntries1(){
        testAcctSeedWaivedFeeChargeJournalEntries(true);
    }
    private testMethod static void testAcctSeedWaivedFeeChargeJournalEntries2(){
        testAcctSeedWaivedFeeChargeJournalEntries(false);
    }
    
    private static void testAcctSeedWaivedFeeChargeJournalEntries(Boolean testException) {
        createDummyData();
        
        Test.startTest();
        
        Date journalEntryDate = Date.newInstance(Date.today().year(),Date.today().month(),1).addMonths(-1);
        
        loan__Loan_Account__c loanAccount = [select Id from loan__Loan_Account__c];
        
        loan__Currency__c currencyRecord = [select Id from loan__Currency__c limit 1];
        
        loan__Fee__c feeRecord = new loan__Fee__c();
        feeRecord.Name = 'Origination Fee';
        feeRecord.loan__Currency__c = currencyRecord.Id;
        feeRecord.loan__State__c = 'Active';
        feeRecord.loan__Fee_Category__c = 'Loan';
        feeRecord.loan__Time_of_charge__c = 'Late Fees';
        feeRecord.loan__Fee_Calculation_Method__c = 'Fixed';
        feeRecord.loan__Amount__c = 50;
        //feeRecord.loan__Income_Account__c = 
        //feeRecord. loan__Suspended_Fee_Account__c = 
        insert feeRecord;
        
        AcctSeed__Journal_Entry__c journalEntry = new AcctSeed__Journal_Entry__c();
        journalEntry.Name = 'Test Journal Entry';
        journalEntry.CL_Contract__c = loanAccount.Id;
        journalEntry.AcctSeed__Journal_Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        journalEntry.AcctSeed__Status__c = 'Approved';
        insert journalEntry;
        
        loan__Charge__c chargeFee = new loan__Charge__c();
        chargeFee.loan__Loan_Account__c = loanAccount.Id;
        chargeFee.loan__Date__c = Date.newInstance(journalEntryDate.year(),journalEntryDate.month(),1).addDays(14);
        chargeFee.loan__Original_Amount__c = 50;
        chargeFee.loan__Fee__c = feeRecord.Id;
        chargeFee.loan__Waive__c = true;
        chargeFee.Journal_Entry__c = journalEntry.Id;
        insert chargeFee;
        
        
        if (testException) {
            // test exception
            loan_seed__Lending_Product_Accounting_Seed__c lendingProductAccSeed = [select Id from loan_seed__Lending_Product_Accounting_Seed__c];
            delete lendingProductAccSeed;
        }
        
        AcctSeedWaivedFeeChargeJournalEntries job = new AcctSeedWaivedFeeChargeJournalEntries();
        Database.executeBatch(job,8);
        
        Test.stopTest();
        
        if (!testException) {
            List<AcctSeed__Journal_Entry__c> journalEntries = [select Id from AcctSeed__Journal_Entry__c];
            System.assertEquals(2,journalEntries.size());
        }
    }
}