@isTest
private class Test_ApplicationToContractConverter {
    public static genesis__applications__c application; 
    public static void init(boolean haveProd) {
         loan.TestHelper.createSeedDataForTesting();
         application            = genesis.TestHelper.createLoanApplication();
        //Create a dummy MF_Account
        loan__MF_Account__c dummyAccount = loan.TestHelper.createMFAccount('XXXAccountForTest','10000 - ASSETS');
        loan__MF_Account__c dummyIncAccount = loan.TestHelper.createMFAccount('XXXIncAccountForTest','30000 - INCOME');
        
        //create Currency
        loan__Currency__c curr = loan.TestHelper.createCurrency();
        
        //Create a Fee Set
        loan__Fee__c dummyFee = loan.TestHelper.createFee(curr);                                    
        loan__Fee_Set__c dummyFeeSet = loan.TestHelper.createFeeSet();
        loan__Fee_Junction__c dummyFeeJunction = loan.TestHelper.createFeeJunction(dummyFee,dummyFeeSet);
        
        loan__Office_Name__c dummyOffice = loan.TestHelper.createOffice();
        //create a Loan Product
        String Name='loan product';
        loan__Loan_Product__c dummyLP = loan.TestHelper.createLoanProductwithProductType(Name,                                                  
                                                                                         dummyOffice,
                                                                                         dummyAccount,
                                                                                         curr,
                                                                                         dummyFeeSet,
                                                                                         'Interest Only',
                                                                                         40,
                                                                                         12,
                                                                                         null,
                                                                                         'Loan');
         
      
        
        loan__Loan_Purpose__c dummyLoanPurpose = loan.TestHelper.createLoanPurpose();
        Account acc = new Account(Name = name);
        acc.loan__Borrower__c = true;
        insert acc;
        if(haveProd){
         application.Lending_Product__c = dummyLP.id;
         application.Company__c                          = dummyOffice.Id;
          application.com_loan__Loan_Purpose__c =dummyLoanPurpose.id;
        }
        
         application.genesis__Product_Type__c            = 'Loan';
         application.genesis__Expected_Start_Date__c = date.today();
         update application;
         
         Lendified_Parameters__c lenp = new Lendified_Parameters__c();
         lenp.Default_Company_Id__c=dummyOffice.id;
         lenp.Default_Lending_Product_Id__c=dummyLP.id;
         lenp.Pre_Paid_Fee__c='Origination Fee';
         insert lenp;
       
        
        
      

    }
    public static testmethod void test1(){
    init(true);
     test.startTest();
        
        ApplicationToContractConverter apptoContractObj = new ApplicationToContractConverter();
        apptoContractObj.application                    =  application;
         Map<SObject,SObject> objectMap = new Map<SObject,SObject>();
         objectMap.put(application,new loan__Loan_Account__c());
        apptoContractObj.setContracts(objectMap);
        apptoContractObj.processContract();
       
        
        test.stopTest();
    
    }
      public static testmethod void test2(){
    init(false);
     test.startTest();
        
        ApplicationToContractConverter apptoContractObj = new ApplicationToContractConverter();
        apptoContractObj.application                    = application;
         Map<SObject,SObject> objectMap = new Map<SObject,SObject>();
         objectMap.put(application,new loan__Loan_Account__c());
        apptoContractObj.setContracts(objectMap);
        apptoContractObj.processContract();
       
        
        test.stopTest();
    
    }
    
     public static testmethod void test3(){
    
     test.startTest();
        
        ApplicationToContractConverter apptoContractObj = new ApplicationToContractConverter();
     //   apptoContractObj.application                    = application;
         Map<SObject,SObject> objectMap = new Map<SObject,SObject>();
         objectMap.put(application,new loan__Loan_Account__c());
          apptoContractObj.application.genesis__Term__c=0;
          apptoContractObj.appChecker();
          apptoContractObj.application.genesis__Term__c=12;
          apptoContractObj.application.genesis__Loan_Amount__c=0;
          apptoContractObj.appChecker();
          apptoContractObj.application.genesis__Term__c=12;
          apptoContractObj.application.genesis__Loan_Amount__c=100;
          apptoContractObj.application.genesis__Payment_Amount__c=0;
          apptoContractObj.appChecker();
          apptoContractObj.application.genesis__Term__c=12;
          apptoContractObj.application.genesis__Loan_Amount__c=100;
          apptoContractObj.application.genesis__Payment_Amount__c=100;
          apptoContractObj.application.genesis__Expected_First_Payment_Date__c=null;
          apptoContractObj.appChecker();
          apptoContractObj.application.genesis__Term__c=12;
          apptoContractObj.application.genesis__Loan_Amount__c=100;
          apptoContractObj.application.genesis__Payment_Amount__c=100;
          apptoContractObj.application.genesis__Expected_First_Payment_Date__c=date.today()+14;
          apptoContractObj.application.genesis__Expected_Start_Date__c=NULL;
          apptoContractObj.appChecker();
          apptoContractObj.application.genesis__Term__c=12;
          apptoContractObj.application.genesis__Loan_Amount__c=100;
          apptoContractObj.application.genesis__Payment_Amount__c=100;
          apptoContractObj.application.genesis__Expected_First_Payment_Date__c=date.today()+14;
          apptoContractObj.application.genesis__Expected_Start_Date__c=DATE.today();
          apptoContractObj.application.genesis__Payment_Frequency__c=null;
          apptoContractObj.appChecker();
       
        
        test.stopTest();
    
    }
    
}