@istest
public class Test_NewDocumentCtrl {
    public testMethod static void testM1(){
      
        Account acc = new Account();
        acc.Name = 'Ext';
        insert acc;
        
      
        genesis__Applications__c obj = new genesis__Applications__c(genesis__Account__c=acc.Id);
        insert obj;
        
        genesis__Document_Master__c objDocMaster = new genesis__Document_Master__c();
        objDocMaster.genesis__Doc_Name__c = 'TestData';
        insert objDocMaster;
        
        Blob b = Blob.valueOf('Test Data');  
      
        Attachment attachment = new Attachment();  
        attachment.ParentId = obj.id;  
        attachment.Name = 'Test Attachment for Parent';  
        attachment.Body = b;
        insert attachment ;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        NewDocumentCtrl  objOffer = new NewDocumentCtrl(sc);
        objOffer.cancelFileUpload();
        objOffer.uploadFile();
        objOffer.insertDocumentStatus('TestData');
        objOffer.insertDocumentStatus('TestData 1');
        objOffer.showPanel();
        objOffer.hidePanel();
        objOffer.getmynote();
        objOffer.Savedoc();
        objOffer.mynote.title='test';
        objOffer.Savedoc();
        objOffer.mynote.title='test';
        objOffer.mynote.body='test';
        objOffer.Savedoc();
        objOffer.sendDocument();
        objOffer.sendPADDoc();
        objOffer.genDocument();  
        objOffer.processUpload();
    }
}