public with sharing class DailyInterestTableController {
	
	public List<loan__Loan_Account__c> clContracts {get;set;}
	public Map<String,Map<String,Double>> contractDailyInterestMap {get;set;}
	public List<String> dateList {get;set;}
	
	public DailyInterestTableController() {
		
	}
	
	public void calcDailyInterest() {
		// calculate daily interest
		String clContractId = ApexPages.currentPage().getParameters().get('id');//'a0l1a000000SgUt';
		String startDateStr = ApexPages.currentPage().getParameters().get('startDate');
		String endDateStr = ApexPages.currentPage().getParameters().get('endDate');
		String sumByMonthStr = ApexPages.currentPage().getParameters().get('sumByMonth');
		Date startDate = Date.valueOf('2015-01-01');
		Date endDate = Date.valueOf('2015-12-31');
		Boolean sumByMonth = (sumByMonthStr != null);
		
		List<String> monthNames = new List<String>{'','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'};
		
		
		if (startDateStr != null && startDateStr != '') {
			startDate = Date.valueOf(startDateStr);
		}
		if (endDateStr != null && endDateStr != '') {
			endDate = Date.valueOf(endDateStr);
		}
		if (clContractId != null && clContractId != '') {
			clContracts = [select Id,Name,loan__Contractual_Interest_Rate__c,loan__Expected_Disbursal_Date__c, loan__Loan_Amount__c, loan__Charged_Off_Date__c, loan__Closed_Date__c  from loan__Loan_Account__c where Id =: clContractId];
		} else {
			clContracts = [select Id,Name,loan__Contractual_Interest_Rate__c,loan__Expected_Disbursal_Date__c, loan__Loan_Amount__c, loan__Charged_Off_Date__c, loan__Closed_Date__c  from loan__Loan_Account__c];
		}
		
		//System.debug(clContract);
		List<loan__Loan_Payment_Transaction__c> lpts = [select Id,
															loan__Loan_Account__c,
		                                                	loan__Transaction_Date__c,
		                                                	loan__Principal__c,
		                                                	loan__Interest__c,
		                                                	loan__Fees__c
		                                                from loan__Loan_Payment_Transaction__c
		                                                where loan__Loan_Account__c =: clContracts
		                                                and loan__Cleared__c = true
		                                                and loan__Reversed__c = false
		                                                and loan__Rejected__c = false];
		
		Map<String,loan__Loan_Payment_Transaction__c> lptMap = new Map<String,loan__Loan_Payment_Transaction__c>();
		for (loan__Loan_Payment_Transaction__c lpt : lpts) {
			String dateStr = lpt.loan__Transaction_Date__c.year() +'-'+lpt.loan__Transaction_Date__c.month()+'-'+lpt.loan__Transaction_Date__c.day();
			lptMap.put(lpt.loan__Loan_Account__c+'_'+dateStr,lpt);
		}
		
		contractDailyInterestMap = new Map<String,Map<String,Double>>();
		Map<String,Double> dailyInterestMap;
		loan__Loan_Payment_Transaction__c lpt;
		
		//generate dateList
		Date tempDate;
		dateList = new List<String>();
		Integer dateDiff = startDate.daysBetween(endDate);
		String dateStr,prevDateStr,dateStrFull,dateMonthStr,prevDateMonthStr;
		for(Integer i=0;i<=dateDiff;i++) {
			tempDate = startDate + i;
			dateStr = tempDate.year() +'-'+tempDate.month()+'-'+tempDate.day();
			if (sumByMonth) {
				dateStr = tempDate.year() +'-'+tempDate.month();
			}
			if (dateStr != prevDateStr) {
				dateList.add(dateStr);
			}
			prevDateStr = dateStr;
		}
		Map<String,Daily_Interest_Accrual__c> dailyInterestAccrualsMap = new Map<String,Daily_Interest_Accrual__c>();
		Daily_Interest_Accrual__c dailyInterestAccrual;
		for (Daily_Interest_Accrual__c diaRecord : [select Id,CL_Contract__c,Year__c from Daily_Interest_Accrual__c where CL_Contract__c =: clContracts]) {
			dailyInterestAccrualsMap.put(diaRecord.CL_Contract__c+'_'+diaRecord.Year__c,diaRecord);
		}
		
		
		
		
		dateStr = null;
		prevDateStr = null;
		dateMonthStr = null;
		prevDateMonthStr = null;
		for (loan__Loan_Account__c clContract : clContracts) {
			dailyInterestMap = new Map<String,Double>();
			Date currentDate = clContract.loan__Expected_Disbursal_Date__c;
			
			Double principal = clContract.loan__Loan_Amount__c;
			Double dailyInterestRate = clContract.loan__Contractual_Interest_Rate__c / 365 / 100;
			Double interest = 0;
			Double charges = 0;
			Double totalAmt = 0;
			Double dailyInterest = 0;
			Double totalInterest = 0;
			Double dailyInterestSum = 0;
			Double monthTotal = 0;
			
			startDate = clContract.loan__Expected_Disbursal_Date__c;
			if (clContract.loan__Closed_Date__c != null) {
				endDate = clContract.loan__Closed_Date__c;
			} else if (clContract.loan__Charged_Off_Date__c != null) {
				endDate = clContract.loan__Closed_Date__c;
			} else {
				endDate = Date.today();
			}
			dateDiff = startDate.daysBetween(endDate);
			
			for(Integer i=0;i<=dateDiff;i++) {
				tempDate = startDate + i;
			    dateStrFull = tempDate.year() +'-'+tempDate.month()+'-'+tempDate.day();
			    dateStr = tempDate.year() +'-'+tempDate.month()+'-'+tempDate.day();
			    dateMonthStr = tempDate.year() +'-'+tempDate.month();
		    	if (sumByMonth) {
					dateStr = tempDate.year() +'-'+tempDate.month();
				}
				
				if (dailyInterestAccrualsMap.containsKey(clContract.id+'_'+tempDate.year())) {
					dailyInterestAccrual = dailyInterestAccrualsMap.get(clContract.id+'_'+tempDate.year());
				} else {
					dailyInterestAccrual = new Daily_Interest_Accrual__c();
					dailyInterestAccrual.Year__c = tempDate.year()+'';
					dailyInterestAccrual.CL_Contract__c = clContract.id;
					dailyInterestAccrualsMap.put(clContract.id+'_'+tempDate.year(),dailyInterestAccrual);
				}
				
				if (tempDate >= currentDate 
						&& (clContract.loan__Charged_Off_Date__c == null || (tempDate < clContract.loan__Charged_Off_Date__c))
						&& (clContract.loan__Closed_Date__c == null || (tempDate < clContract.loan__Closed_Date__c))) {
			    
				    if (lptMap.containsKey(clContract.Id+'_'+dateStrFull)) {
				    	lpt = lptMap.get(clContract.Id+'_'+dateStrFull);
			    	
			            principal -= lpt.loan__Principal__c;
			            interest -= lpt.loan__Interest__c;
			            //charges -= lpt.loan__Fees__c;
				    }
				    
				    totalAmt = principal + interest;
				    dailyInterest = principal * dailyInterestRate;
				    interest += dailyInterest;
				    totalInterest += dailyInterest;
				    
				    System.debug('daily: '+Math.round(dailyInterest * 100.0) / 100.0);
				    
				    // round daily interest for display
					if (dateStr == prevDateStr) {
				    	dailyInterestSum += Math.round(dailyInterest * 100.0) / 100.0;
					} else {
						dailyInterestSum = Math.round(dailyInterest * 100.0) / 100.0;
					}
				} else {
					dailyInterestSum = 0;
				}
				if (dateMonthStr == prevDateMonthStr) {
					monthTotal += dailyInterestSum;
				} else {
					monthTotal = dailyInterestSum;
				}
				dailyInterestMap.put(dateStr, Math.round( dailyInterestSum * 100.0) / 100.0);
			    
			    dailyInterestAccrual.put(monthNames.get(tempDate.month())+'_'+tempDate.day()+'__c',Math.round( dailyInterestSum * 100.0) / 100.0);
			    dailyInterestAccrual.put(monthNames.get(tempDate.month())+'__c',Math.round( monthTotal * 100.0) / 100.0);
		    	
			    
			    prevDateStr = dateStr;
			    prevDateMonthStr = dateMonthStr;
			}
			dailyInterestMap.put('total', Math.round(totalInterest * 100.0)/100.0);
			System.debug('Total interest: '+totalInterest);
			
			contractDailyInterestMap.put(clContract.Id,dailyInterestMap);
			
		}
		
		// delete pre-existing Daily Interest Accrual records
		/*
		List<Daily_Interest_Accrual__c> dailyInterestAccrualsToDelete = [select Id from Daily_Interest_Accrual__c where CL_Contract__c =: clContracts];
		if (dailyInterestAccrualsToDelete.size() > 0) {
			delete dailyInterestAccrualsToDelete;
		}
		*/
		
		if (dailyInterestAccrualsMap.values().size() > 0) {
			upsert dailyInterestAccrualsMap.values();
		}
	}
	
}