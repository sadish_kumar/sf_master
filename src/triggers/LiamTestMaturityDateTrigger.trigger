trigger LiamTestMaturityDateTrigger on loan__Loan_Account__c (after insert, after update) {
   Set<Id> contactIds = new Set<Id>();
    List<Contact> contactsMaturing = new List<Contact>();
    Contact tempContact;
    for (loan__Loan_Account__c clContract: Trigger.new){
        
        if (clContract.loan__Contact__c != null
           && (Trigger.isInsert
               || (Trigger.isUpdate && clContract.loan__Last_Installment_Date__c != Trigger.oldMap.get(CLContract.Id).loan__Last_Installment_Date__c))){
    
            if(!contactIds.contains (clContract.loan__Contact__c)){
                contactIds.add(clContract.loan__Contact__c );
                tempContact = New Contact ();
                tempContact.Id = clContract.loan__Contact__c ;
                tempContact.Contract_Maturity_Date__c  = clContract.loan__Last_Installment_Date__c ;
                contactsMaturing.add(tempContact);
            } 
        }
    }
    if (contactsMaturing.size() > 0){
    	update contactsMaturing;
	}
}