public class ConfirmController extends com_loan.BaseController {
public Boolean errorFlag { get; set; }
public String applicationTrackId {get;set;} 
public String personalMonthlyMortgageStr {get;set;} 
public String annualRevenueStr {get;set;} 
public String annualPersonalIncomeStr {get;set;} 
   
    public ConfirmController() {
         if(bdm.businessNumber!=null)
           bdm.businessNumber=bdm.businessNumber+' RT0001';        
      applicationTrackId= ApexPages.currentPage().getParameters().get('id');      
      
      List<genesis__applications__c> application = [select id,com_loan__Annual_Personal_Income__c,com_loan__Annual_Revenues__c,com_loan__Personal_Monthly_Mortgage_or_Rent_Paymen__c from genesis__applications__c where id=:applicationTrackId];
      if(application.size()>0){   
         if(application[0].com_loan__Personal_Monthly_Mortgage_or_Rent_Paymen__c!=null)
           personalMonthlyMortgageStr='$ '+application[0].com_loan__Personal_Monthly_Mortgage_or_Rent_Paymen__c;
        if(application[0].com_loan__Annual_Revenues__c!=null)
            annualRevenueStr='$ '+application[0].com_loan__Annual_Revenues__c;
        if(application[0].com_loan__Annual_Personal_Income__c!=null)
           annualPersonalIncomeStr='$ '+application[0].com_loan__Annual_Personal_Income__c;
       }    
        
    }
    
    public PageReference validate() {
        return validateApplicationSequence();
    }
    
    public PageReference continueAction() {
        try {
            genesis__applications__c application = com_loan.CommunityUtil.getApplicationFromId(applicationId);
            EquifaxIntegration eInts = new EquifaxIntegration();   
            ints__Org_Parameters__c org = ints__Org_Parameters__c.getOrgDefaults();
            Boolean isTest =false;
            if(org != null && org.Equifax_Test__c){
                isTest = true;
            }
            String resp = eInts.equifaxCRApplication1(application.Id,isTest);
            system.debug('Equifax Response'+resp);
            CreditEngine.postLoanApplciation(applicationId);
            PageReference pr = new PageReference('/ApplicationRejected');
            pr.getParameters().put('id', applicationId);
            if(String.isEmpty(resp)){
                application.genesis__Status__c = 'Declined - No Hit';
                update application;                
                //return gotoNext();
                return pr;
            }
            else if(resp.Contains('Error')){
                application.genesis__Status__c = 'Declined - No Hit';
                update application;                
               // return gotoNext();
                return pr;
            }
            
            if([Select Id from ints__EfxReport__c where Id = :resp].size() == 0){
                application.genesis__Status__c = 'Declined - No Hit';
                update application;
                //return gotoNext();
                return pr;
            }
                
            ints__EfxReport__c ecrp = [Select Id,name,
                                            Account__c,
                                            application__c,
                                            ints__FileSinceDate__c,
                                            ints__Hit_Code__c
                                            from ints__EfxReport__c
                                            where Id = :resp];
            return gotoNext();
            } catch(Exception ex) {
                system.debug('Exception while performing equifaxCRAPI:'+ex);
                PageReference errPage = new PageReference('/com_loan__error');
                errPage.getParameters().put('id', applicationId);
                return errPage;
               // return new PageReference('/com_loan__error');
            }
    }
    
    public PageReference abortAction() {
        //do cleanup here
        //com_loan.CommunityUtil.doCleanup(applicationId);
        PageReference pr = new PageReference('http:/lendified.com');
        return pr;
    }
}