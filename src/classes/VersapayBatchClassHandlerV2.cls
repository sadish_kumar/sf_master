public class VersapayBatchClassHandlerV2{
    
    List<genesis__Applications__c> scope;
    DDP_Template__c ddpTemplate;
    
    
    public VersapayBatchClassHandlerV2(List<genesis__Applications__c> scope){
        this.scope = scope;
        ddpTemplate           = new DDP_Template__c();
    }
    
    public void checkVersapayTokenStatus(String sessionId){
        
         system.debug('***sessionId'+sessionId);
        for(genesis__Applications__c appObj : scope){
            System.debug('appObj'+appObj.Id);
            loan_vp.VersapayImpl impl = new loan_vp.VersapayImpl();
            loan_vp.PADResponse res;
            if(!Test.isRunningTest()){
                res =  impl.getPADAgreement(appObj.Versapay_Token__c);
                system.debug('***INLive'+res);
                 System.debug('res.state'+res.state);
            }
            else{
                res = new loan_vp.PADResponse();
                res.state = 'APPROVED';
                system.debug('***INTest'+res);
               
            }
            
            if(res != null)
                appObj.Versapay_Token_Status__c = res.state;
            
            if(appObj.Versapay_Token_Status__c!=null && (appObj.Versapay_Token_Status__c).equalsIgnoreCase('APPROVED')){    
                appObj.Contract_Document_Sent__c = true;
                 System.debug('appObj.genesis__Contact__r.MailingState'+appObj.genesis__Contact__r.MailingState);
                if(appObj.genesis__Contact__c!=null && appObj.genesis__Contact__r.MailingState!=null){
                     
                     if(appObj.genesis__Contact__r.MailingState.equalsIgnoreCase('AB')){
                         System.debug('AB');
                         ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE_AB);
                     
                     }                     
                     else if(appObj.genesis__Contact__r.MailingState.equalsIgnoreCase('QC')){
                         ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE_QC);
                     }
                     else
                         ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
                }
                else{
                ddpTemplate           = DDP_Template__c.getValues(VaultCircleConstants.DDP_TEMPLATE);
                }               
                system.debug('***ddpTemplate'+ddpTemplate.name);
                if(ddpTemplate != null){                
                    // Drawloop.generateDDP(ddpTemplate.Deploy_ID__c , ddpTemplate.Template_Id__c, contractObj.Application__c, sessionId);
                    Map<string, string> variables = new Map<string,string>();
                    variables = new Map<string, string> {
                    'deploy' => ddpTemplate.Deploy_ID__c,
                    'SFAccount' => appObj.genesis__Account__c,
                    'SFContact' => appObj.genesis__Contact__c,
                    'param_name'=> appObj.genesis__Contact__r.name,
                    'param_email'=> appObj.genesis__Contact__r.email
                    };
                    Loop.loopMessage lm = new Loop.loopMessage();
                    lm.sessionId=sessionId;
                    lm.batchNotification = Loop.loopMessage.Notification.BEGIN_AND_COMPLETE;                    
                    lm.requests.add(new Loop.loopMessage.loopMessageRequest(appObj.Id,ddpTemplate.Template_Id__c,variables));
                    String resp=lm.sendAllRequests();
                }
            }
        }
        
        List<Database.SaveResult> updateCLContract =  Database.update(scope);
        
    }
    
}