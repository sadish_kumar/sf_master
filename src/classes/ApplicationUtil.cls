Global Class ApplicationUtil{
    
    Webservice Static String declineApplication(ID appId){
        
        List<genesis__Applications__c> app = [Select Id,name,genesis__Status__c from genesis__Applications__c where Id = : appId];
        
        if(app != null && app.size() > 0){
            if(app[0].genesis__Status__c.equals('APPLICATION - CONVERTED'))
                return 'Error : Converted Application cannot be Declined.';
            app[0].genesis__Status__c = 'Application Declined by Lendified';
        }
        
        try{
            update app;
        }catch(Exception e){
            return 'Error : ' + e.getMessage();
        }
        
        return 'Application Declined.';
    }
    
    Webservice Static String convertApplication(ID appId){
        try{
            //Before converting update Amortization Schedule....
            //Expected Start Date should be Today...
            
            genesis__Applications__c app = [Select id,Name,
                                                genesis__Expected_Start_Date__c,
                                                Number_of_Payments__c,
                                                genesis__Total_Estimated_Interest__c,
                                                genesis__Interest_Only_Period__c,
                                                genesis__Balloon_Payment__c,
                                                genesis__Interest_Calculation_Method__c,
                                                genesis__Days_Convention__c,
                                                genesis__Loan_Amount__c,
                                                genesis__Interest_Rate__c,
                                                genesis__Term__c,
                                                genesis__Expected_First_Payment_Date__c,
                                                genesis__Payment_Frequency__c,
                                                genesis__Status__c 
                                                
                                                from genesis__Applications__c
                                                where id = : appId];
            
            //validation...
            if(app.genesis__Loan_Amount__c == null || app.genesis__Loan_Amount__c == 0)
                return 'Conversion Error : Loan Amount cannot be null or zero.';
            if(app.genesis__Interest_Rate__c == null || app.genesis__Interest_Rate__c ==0)
                return 'Conversion Error : Interest Rate cannot be null or zero.';
            if(app.genesis__Term__c == null || app.genesis__Term__c == 0)
                return 'Conversion Error : Terms cannot be null or zero.';
            if(app.genesis__Expected_First_Payment_Date__c == null)
                return 'Conversion Error : Expected First Payment Date cannot be null.';
            if(app.genesis__Expected_Start_Date__c == null)
                return 'Conversion Error : Expected Start Date cannot be null.';
            if(app.genesis__Payment_Frequency__c == null)
                return 'Conversion Error : Payment Frequency cannot be null.';
            if(app.genesis__Status__c != null && !app.genesis__Status__c.equalsIgnoreCase('Offer Accepted'))
                return 'Conversion Error : Application Status should be "Offer Accepted".';
            
            //set application's start date to CL Loan's date... and first payment date to CL Loan's date + 14...
            loan.GlobalLoanUtilFacade info = new loan.GlobalLoanUtilFacade();
            if(app.genesis__Expected_Start_Date__c != info.getCurrentSystemDate()){
            
                app.genesis__Expected_Start_Date__c = info.getCurrentSystemDate();
                
                app.genesis__Expected_First_Payment_Date__c = app.genesis__Expected_Start_Date__c.addDays(14);
                
                String dayConvention = (app.genesis__Days_Convention__c!=null)?app.genesis__Days_Convention__c:'30/360';
                Decimal interestOnlyPeriod = 0;
                if(app.genesis__Interest_Only_Period__c != 0){
                    interestOnlyPeriod = app.genesis__Interest_Only_Period__c;
                }
                
                Decimal balloonPayment = 0;
                if(app.genesis__Balloon_Payment__c != 0){
                    balloonPayment = app.genesis__Balloon_Payment__c;
                } 
                Decimal totalLoanAmount = app.genesis__Loan_Amount__c;
                Date firstPaymentDate = (Date)app.genesis__Expected_First_Payment_Date__c;
                String paymentFrequencyCode = app.genesis__Payment_Frequency__c;
                String billingMethodCode = app.genesis__Interest_Calculation_Method__c != null ? app.genesis__Interest_Calculation_Method__c
                                                                                                 : 'Declining Balance'; 
                Decimal interestRate = app.genesis__Interest_Rate__c;
                Decimal term = app.Number_of_Payments__c ;
                Date ExpectedStartDate = app.genesis__Expected_Start_Date__c;
                if(app.Number_of_Payments__c > 0)
                    app.genesis__Expected_Close_Date__c = loan.DateUtil.addCycle(app.genesis__Expected_First_Payment_Date__c,
                                                            (Integer)app.genesis__Expected_First_Payment_Date__c.day(),
                                                            app.genesis__Payment_Frequency__c,
                                                            (Integer)(app.Number_of_Payments__c -1) );
                
                
                
                genesis__Lending_Calculator__c calc = new genesis__Lending_Calculator__c(
                                                genesis__Accrual_Base_Method_Code__c = dayConvention,
                                                genesis__Action__c = 'CALCULATION_ALL',
                                                genesis__Additional_Interest_Amount__c = 0,
                                                genesis__Amortization_Calculation_Method_Code__c = 'NONE', 
                                                genesis__APR__c = 0,
                                                genesis__Balance_Amount__c = 0,
                                                genesis__Balloon_Method_Code__c = 'DUMMY',
                                                genesis__Balloon_Payment_Amount__c = balloonPayment,
                                                genesis__Billing_Method_Code__c = billingMethodCode,
                                                genesis__Contract_Date__c = expectedStartDate,
                                                genesis__Final_Payment_Amount__c = 0,
                                                genesis__Financed_Amount__c = 0,
                                                genesis__Financed_Fees__c = 0,
                                                genesis__First_Payment_Date__c = firstPaymentDate, 
                                                genesis__First_Period_Calender_Days__c = 0,
                                                genesis__First_Period_Interest__c = 0,
                                                genesis__Flexible_Repayment_Flag__c = false,
                                                genesis__Installment_Method_Code__c = 'UNDEFINED',
                                                genesis__Interest_Amount__c = 0,
                                                genesis__Interest_Only_Period__c = interestOnlyPeriod,
                                                genesis__Loan_Amount__c = totalLoanAmount,
                                                genesis__Payment_Amount__c = 0,
                                                genesis__Payment_Frequency_Code__c = paymentFrequencyCode,
                                                genesis__Prepaid_Fees__c = 0,
                                                genesis__Principal_Payment_Amount__c = 0,
                                                genesis__Rate__c = interestRate,
                                                genesis__Repayment_Type_Code__c = 'UNDEFINED',
                                                genesis__Term__c = term,
                                                genesis__Total_Finance_Charge__c = 0,
                                                genesis__Total_Financed_Amount__c = 0,
                                                genesis__Total_Of_Payments__c = 0
    
                );  
                //system.debug('***Calc'+calc);
                app.genesis__Total_Estimated_Interest__c = 0;
                List<genesis__Amortization_Schedule__c> emi = genesis.LendingCalculator.calculate(calc, null);
                for(genesis__Amortization_Schedule__c amz : emi){
                    amz.genesis__Application__c = app.Id;
                    app.genesis__Total_Estimated_Interest__c += amz.genesis__Due_Interest__c;
                }
                //delete previous Amortization Schedule...
                List<genesis__Amortization_Schedule__c> amzList = [Select id,name from genesis__Amortization_Schedule__c 
                                                                        where genesis__Application__c =:appId];
                delete amzList;
                
                //insert new Amortization Schedule...
                insert emi;
                
                //update Application...
                update app;
            }
                
            genesis.ConvertApplicationCtrl ctrl = new genesis.ConvertApplicationCtrl();
            String retMsg = ctrl.convertApplication(appId);
            return retMsg;
        }catch(Exception e){
            return 'Error : ' + e.getMessage() + e.getLineNumber();
        }
        
        
        
    }

}