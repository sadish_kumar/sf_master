public class CreateLoanDisbursalTxnHandler{

    List<loan__loan_Account__c> scope;
    private Date iDate;
    private loan__Payment_Mode__c paymentMode;
    private loan_vp.VersapayImpl versaPayImpl = new loan_vp.VersapayImpl();

    
    
    public CreateLoanDisbursalTxnHandler(List<loan__loan_Account__c> scope){
        this.scope = scope;
        loan.GlobalLoanUtilFacade facade = new loan.GlobalLoanUtilFacade();
        paymentMode = [Select id,name from loan__Payment_Mode__c where name = 'ACH'];
        versaPayImpl = new loan_vp.VersapayImpl();
        
         if(iDate == null) {
           this.idate = facade.getCurrentSystemDate(); 
        }

    }

    public void processLoanAccounts(){
        List<loan__Loan_Disbursal_Transaction__c> disbursalTxnList = new List<loan__Loan_Disbursal_Transaction__c>();
        for(loan__loan_Account__c loanAccount : scope){
            String token = loanAccount.loan_vp__Versapay_Token__c;
            if(!String.isEmpty(token)){
               loan_vp.PADResponse padResponse ;
               if(!Test.isRunningTest()){ 
                    padResponse = versaPayImpl.getPADAgreement(loanAccount.loan_vp__Versapay_Token__c); 
               }else{
                   padResponse = new loan_vp.PADResponse();
                   padResponse.state = 'APPROVED';    
               }    
             
               System.debug('padResponse: '+padResponse);
               String padState = padResponse.state;
               if(loanAccount.loan__Loan_Amount__c > 0 && padState.equalsIgnoreCase('APPROVED')){
                    loan__Loan_Disbursal_Transaction__c disbursalTxn = new loan__Loan_Disbursal_Transaction__c();
                    disbursalTxn.loan__Disbursed_Amt__c = loanAccount.loan__Loan_Amount__c;
                    disbursalTxn.loan__Loan_Account__c = loanAccount.Id;
                    disbursalTxn.loan__Disbursal_Date__c = iDate;
                    disbursalTxn.loan__Mode_of_Payment__c = paymentMode.id;
                    disbursalTxn.loan__Cleared__c = true;
                    disbursalTxn.loan__Financed_Amount__c = loanAccount.loan__Disbursal_Amount__c;
                    disbursalTxnList.add(disbursalTxn);
                }
        
                loanAccount.Versapay_Token_Status__c = padState;
            }
          }  
        
            insert disbursalTxnList;
            update scope;
    }

}