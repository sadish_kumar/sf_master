@isTest
public class TestDataFactory {
    public static genesis__applications__c createApplicationTestRecord() {
        genesis__applications__c application = genesis.TestHelper.createLoanApplication();
        Account acc = genesis.TestHelper.createAccount();
        
        Contact con = genesis.TestHelper.createContact();
        con.email   = 'sample@sample.com.sample';
        con.mobilephone     = '123 456 7890';
        update con;

        application.genesis__Account__c = acc.Id;
        application.genesis__Term__c    = 12;
        application.com_loan__Business_Name__c    = 'Sample Business';
        application.com_loan__Loan_Purpose__c  = 'Sample Loan'; 
        application.genesis__Days_Convention__c  = '30/360'; 
        application.genesis__Expected_Close_Date__c  =  Date.newInstance(1960, 2, 17);

        update application;

        genesis__Applications__c app = [Select Id,Name,genesis__Contact__r.Id,genesis__Contact__r.firstname,
                                                genesis__Contact__r.lastname,genesis__Contact__r.mobilephone,
                                                genesis__Contact__r.birthdate,genesis__Contact__r.email,
                                                genesis__Contact__r.mailingstreet,genesis__Loan_Amount__c,genesis__Term__c,
                                                genesis__Contact__r.mailingcity,genesis__Contact__r.mailingpostalcode,
                                                genesis__Contact__r.mailingstate,com_loan__Loan_Purpose__c,
                                                com_loan__Business_Name__c,genesis__Contact__r.mailingcountry,com_loan__Business_Number__c,
                                                com_loan__Street_Address__c,com_loan__City__c,com_loan__State__c,
                                                com_loan__Postal__c,com_loan__Industry__c,Beacon_9_Score__c ,BNI__c,
                                                genesis__Payment_Frequency__c,genesis__Expected_First_Payment_Date__c,
                                                Number_of_Payments__c ,genesis__Expected_Close_Date__c,
                                                genesis__Applications__c.com_loan__Trade_Name_of_Business__c 
                                    from genesis__Applications__c
                                    where Id = :application.Id];
        return app;
    }
}